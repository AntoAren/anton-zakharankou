package com.epam.newsmanagement.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import com.epam.zakharankou.newsmanagement.entity.Tag;
import com.epam.zakharankou.newsmanagement.service.MainNewsManagementService;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

@Controller
@RequestMapping(value = "/tag")
public class TagController {
	
	@Autowired
	private MainNewsManagementService mainNewsManagementService;
	
	@RequestMapping(value = "/addUpdateTags")
	public String addUpdateAuthors(HttpSession session, Model model) {
		try{
			List<Tag> listTags = mainNewsManagementService.readAllTags();	
			model.addAttribute("tags", listTags);
			model.addAttribute("tag", new Tag());
			model.addAttribute("currentPage", "addUpdateTags");
		} catch (ServiceException exception){
			return "error";
		}
		return "addUpdateTags";
	}
	
	@RequestMapping(value = "/saveTag")
	public String saveAuthor(@Valid @ModelAttribute("tag") Tag tag, BindingResult bindingResult, HttpSession session, Model model){
		try{
			if (bindingResult.hasErrors()) {
				List<Tag> listTags = mainNewsManagementService.readAllTags();	
				model.addAttribute("tags", listTags);
				model.addAttribute("currentPage", "addUpdateTags");
				return "addUpdateTags";
			}
			mainNewsManagementService.addTag(tag);
		} catch (ServiceException exception){
			return "error";
		}
		return "redirect:/tag/addUpdateTags";
	}
	
	@RequestMapping(value = "/updateTag")
	public String updateAuthor(@Valid @ModelAttribute("tag") Tag tag, BindingResult bindingResult, HttpSession session, Model model){
		try{
			if (bindingResult.hasErrors()){
				List<Tag> listTags = mainNewsManagementService.readAllTags();	
				model.addAttribute("tags", listTags);
				model.addAttribute("tagIdError", tag.getTagId());
				model.addAttribute("currentPage", "addUpdateTags");
				return "addUpdateTags";
			}
			mainNewsManagementService.updateTag(tag);
		} catch (ServiceException exception){
			return "error";
		}
		return "redirect:/tag/addUpdateTags";
	}
	
	@RequestMapping(value = "/deleteTag")
	public String deleteTag(HttpSession session, Model model, @RequestParam("tagId") Long tagId){
		try{
			mainNewsManagementService.deleteTag(tagId);
		} catch (ServiceException exception){
			return "error";
		}
		return "redirect:/tag/addUpdateTags";
	}
}
