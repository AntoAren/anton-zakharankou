package com.epam.zakharankou.newsmanagement.dao.implEL;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceUnit;

import com.epam.zakharankou.newsmanagement.dao.ICommentDao;
import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.Comment;

/**
 * This class is Eclipse Link Implementation of JPA for #{@link ICommentDao}. 
 * @author Anton_Zakharankou
 *
 */
public class CommentDao implements ICommentDao {
	
	@PersistenceUnit(unitName = "entityManagerFactory")
	private EntityManagerFactory entityManagerFactory;
	
	public Long create(Comment comment) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		try {
			entityManager.persist(comment);
			entityTransaction.commit();
		} finally {
			entityManager.close();
		}
		return comment.getCommentId();
	}

	public Comment readById(Long commentId) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Comment comment = null;
		try {
			comment = entityManager.find(Comment.class, commentId);
		} finally {
			entityManager.close();
		}
		return comment;
	}

	public boolean update(Comment comment) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		boolean flag = true;		
		try {
			entityManager.merge(comment);
			entityTransaction.commit();
		} catch (OptimisticLockException exception){
			flag = false;
			entityTransaction.rollback();
		} finally {	
			entityManager.close();
		}
		return flag;
	}

	public void delete(Long commentId) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		try {
			Comment comment = entityManager.find(Comment.class, commentId);
			entityManager.remove(comment);
			entityTransaction.commit();
		} finally {
			entityManager.close();
		}
	}

}
