package com.epam.zakharankou.newsmanagement.dao.implJDBC;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import com.epam.zakharankou.newsmanagement.dao.implJDBC.NewsDao;
import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.SearchCriteria;

import static org.junit.Assert.*;

@SpringApplicationContext({"configTest.xml"})
@DataSet(value = "DataSetTest.xml", loadStrategy = CleanInsertLoadStrategy.class)
public class NewsDaoTest extends UnitilsJUnit4{
	
	@SpringBean("newsDaoImpl")
	private NewsDao newsDao;
	
	@Test
	public void createTest() throws Exception{
		News expectedNews = new News();
		expectedNews.setTitle("Title100");
		expectedNews.setShortText("ShortText100");
		expectedNews.setFullText("FullText100");
		
		Long actualNewsId = newsDao.create(expectedNews);
		News actualNews = newsDao.readById(actualNewsId);
		
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());
	}
	
	@Test
	public void readTest() throws Exception{
		Long startPosition = new Long(2L);
		Long endPosition = new Long(3L);
		News firstNews = new News();
		News secondNews = new News();
		firstNews.setTitle("Title3");
		firstNews.setShortText("ShortText3");
		firstNews.setFullText("FullText3");
		secondNews.setTitle("Title2");
		secondNews.setShortText("ShortText2");
		secondNews.setFullText("FullText2");
		
		List<News> actualNewsList = newsDao.read(startPosition, endPosition);
		
		assertEquals(firstNews.getTitle(), actualNewsList.get(0).getTitle());
		assertEquals(firstNews.getShortText(), actualNewsList.get(0).getShortText());
		assertEquals(firstNews.getFullText(), actualNewsList.get(0).getFullText());
		assertEquals(secondNews.getTitle(), actualNewsList.get(1).getTitle());
		assertEquals(secondNews.getShortText(), actualNewsList.get(1).getShortText());
		assertEquals(secondNews.getFullText(), actualNewsList.get(1).getFullText());
	}
	
	@Test
	public void readByIdTest() throws Exception{
		Long newsId = new Long(2L);
		News expectedNews = new News();
		expectedNews.setTitle("Title2");
		expectedNews.setShortText("ShortText2");
		expectedNews.setFullText("FullText2");
		
		News actualNews = newsDao.readById(newsId);
		
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());
	}
	
	@Test
	public void updateTest() throws Exception{
		News expectedNews = new News();
		expectedNews.setNewsId(new Long(1L));
		expectedNews.setTitle("Title11111");
		expectedNews.setShortText("ShortText11111");
		expectedNews.setFullText("FullText11111");
		
		newsDao.update(expectedNews);
		News actualNews = newsDao.readById(expectedNews.getNewsId());
		
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());
	}
	
	@Test
	public void deleteTest() throws Exception{
		Long newsId = new Long(3L);
		
		newsDao.delete(newsId);
		News actualNews = newsDao.readById(newsId);
		
		assertNull(actualNews);
	}
	
	@Test
	public void searchNewsTest() throws Exception{
		Long startPosition = new Long(1L);
		Long endPosition = new Long(1L);
		SearchCriteria searchCriteria = new SearchCriteria();
		List<Long> listTagIds = new ArrayList<Long>();
		listTagIds.add(new Long(1L));
		listTagIds.add(new Long(2L));
		listTagIds.add(new Long(3L));
		searchCriteria.setAuthorId(new Long(1L));
		searchCriteria.setTagsId(listTagIds);
		News expectedNews = new News();
		expectedNews.setTitle("Title4");
		expectedNews.setShortText("ShortText4");
		expectedNews.setFullText("FullText4");
		expectedNews.setNewsId(new Long(4L));
		
		
		List<News> actualNewsList = newsDao.searchNews(searchCriteria, startPosition, endPosition);
		
		assertEquals(expectedNews.getTitle(), actualNewsList.get(0).getTitle());
		assertEquals(expectedNews.getShortText(), actualNewsList.get(0).getShortText());
		assertEquals(expectedNews.getFullText(), actualNewsList.get(0).getFullText());
	}
	
	@Test
	public void countAllNewsTest() throws Exception{
		SearchCriteria searchCriteria = new SearchCriteria();
		List<Long> listTagIds = new ArrayList<Long>();
		listTagIds.add(new Long(1L));
		listTagIds.add(new Long(2L));
		listTagIds.add(new Long(3L));
		searchCriteria.setAuthorId(new Long(1L));
		searchCriteria.setTagsId(listTagIds);
		
		Long actualCount = newsDao.countAllNews(searchCriteria);
		
		assertEquals(3L, actualCount.longValue());
	}
	
	@Test
	public void readAllTest() throws Exception{
		News firstNews = new News();
		News secondNews = new News();
		News thirdNews = new News();
		News fourthNews = new News();
		firstNews.setTitle("Title4");
		secondNews.setTitle("Title3");
		thirdNews.setTitle("Title2");
		fourthNews.setTitle("Title1");
		
		List<News> actualNewsList = newsDao.readAll();
		
		assertEquals(firstNews.getTitle(), actualNewsList.get(0).getTitle());
		assertEquals(secondNews.getTitle(), actualNewsList.get(1).getTitle());
		assertEquals(thirdNews.getTitle(), actualNewsList.get(2).getTitle());
		assertEquals(fourthNews.getTitle(), actualNewsList.get(3).getTitle());
	}
}
