package com.epam.zakharankou.newsmanagement.entity;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


/**
 * ComplexNews class provides entity, which contains news entity, author entity, list of tags entities and list of comments entities.
 * @author Anton_Zakharankou
 *
 */
public class ComplexNews {
	
	@NotNull(message = "{error.complexNews.news.notNull}")
	@Valid
	private News news;
		
	private Author author;
	
	private List<Comment> comments;
	
	private List<Tag> tags;	
	
	@NotNull(message = "{error.complexNews.authorId.notNull}")
	@Min(message = "{error.complexNews.authorId.min}", value = 1)
	private Long authorId;
	
	private List<Long> tagIds;
	
	public Long getAuthorId(){
		return authorId;
	}
	
	public void setAuthorId(Long authorId){
		this.authorId = authorId;
	}
	
	public List<Long> getTagIds() {
		return tagIds;
	}	
	public void setTagIds(List<Long> tagIds) {
		this.tagIds = tagIds;
	}
	public News getNews() {
		return news;
	}
	public void setNews(News news) {
		this.news = news;
	}
	public Author getAuthor() {
		return author;
	}
	public void setAuthor(Author author) {
		this.author = author;
	}
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	public List<Tag> getTags() {
		return tags;
	}
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result	+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime * result + ((tagIds == null) ? 0 : tagIds.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComplexNews other = (ComplexNews) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (tagIds == null) {
			if (other.tagIds != null)
				return false;
		} else if (!tagIds.equals(other.tagIds))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ComplexNews [news=" + news + ", author=" + author
				+ ", comments=" + comments + ", tags=" + tags + ", authorId="
				+ authorId + ", tagIds=" + tagIds + "]";
	}
	
	
	
	
	
		
}
