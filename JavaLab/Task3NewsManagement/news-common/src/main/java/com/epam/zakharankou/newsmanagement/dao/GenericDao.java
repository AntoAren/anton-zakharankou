package com.epam.zakharankou.newsmanagement.dao;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;

/**
 * GenericDao is interface which implemented for each entity.
 * @author Anton_Zakharankou
 *
 * @param <T> - type of entity.
 */
public interface GenericDao<T> {
	
	/**
	 * Writes record in database.
	 * @param object - recording entity.
	 * @return id of recorded entity.
	 * @throws DAOException if SQLException was thrown.
	 */
	public Long create(T object) throws DAOException;
	
	/**
	 * Reads record by ID from database.
	 * @return recorded entity.
	 * @throws DAOException if SQLException was thrown.
	 */
	public T readById(Long id) throws DAOException;
	
	/**
	 * Updates record in database.
	 * @param object - new values.
	 * @throws DAOException if SQLException was thrown.
	 */
	public boolean update(T object) throws DAOException;
	
	/**
	 * Deletes record from database.
	 * @param id - id of deleting record.
	 * @throws DAOException
	 */
	public void delete(Long id) throws DAOException;
}
