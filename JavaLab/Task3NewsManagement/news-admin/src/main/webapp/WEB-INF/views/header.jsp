<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div>
<table class="tableHeader">
	<tr>
		<td>					
			<h1><spring:message code="local.newsPortalAdministration"/></h1>
		</td>
		<td align="right">
			<span style="float: right">
				<c:choose>
					<c:when test="${paramString != null}">
   						<a href="?language=en${paramString}">EN</a>
    					<a href="?language=ru${paramString}">РУС</a>
    				</c:when>    				
    				<c:otherwise>
    					<a href="?language=en">EN</a>
    					<a href="?language=ru">РУС</a>
    				</c:otherwise>
    			</c:choose>
			</span>
		</td>
	</tr>
</table>
</div>