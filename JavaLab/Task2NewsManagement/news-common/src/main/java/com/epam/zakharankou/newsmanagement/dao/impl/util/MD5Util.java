package com.epam.zakharankou.newsmanagement.dao.impl.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;

/**
 * This class is used to encode password
 */
public class MD5Util {
	
	private static final String TYPE = "MD5";
	
	public static String codingMD5(String str) throws DAOException{
		
		MessageDigest messageDigest = null;
		byte[] digest = new byte[0];
		try{
			messageDigest = MessageDigest.getInstance(TYPE);
			messageDigest.reset();
			messageDigest.update(str.getBytes());
			digest = messageDigest.digest();
		} catch (NoSuchAlgorithmException e){			
			throw new DAOException("NoSuchAlgorithmException is thrown when trying to encoding password", e);
		}
		BigInteger bigInteger = new BigInteger(1, digest);
		String md5Hex = bigInteger.toString(16);
		while (md5Hex.length() < 32){
			md5Hex = "0" + md5Hex;
		}
		return md5Hex;
	}
}
