package com.epam.zakharankou.newsmanagement.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.dao.ITagDao;
import com.epam.zakharankou.newsmanagement.entity.Tag;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/configTest.xml"})
public class TagServiceImplTest {
	
	@Mock
	private ITagDao tagDao;
	
	@InjectMocks
	private TagServiceImpl tagServiceImpl;
	
	@Before
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void addTagTest() throws DAOException, ServiceException{
		Tag tag = new Tag();
		Long tagId = new Long(1L);
		when(tagDao.create(tag)).thenReturn(tagId);
		assertEquals(tagId, tagServiceImpl.addTag(tag));
		verify(tagDao, times(1)).create(tag);
	}
	
	@Test
	public void deleteTagTest() throws DAOException, ServiceException{
		Long tagId = new Long(1L);
		tagServiceImpl.deleteTag(tagId);
		verify(tagDao, times(1)).delete(tagId);
	}
	
}
