package com.epam.zakharankou.newsmanagement.dao.impl.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;

/**
 * The CloseUnitility class used for close resources.
 * @author Anton_Zakharankou
 *
 */
public class CloseUtility {
	
	/**
	 * Releases Connection and closes Statement, ResultSet.
	 * @throws DAOException if SQLException was thrown.
	 */
	public static void close(Connection connection, Statement statement, ResultSet resultSet, DataSource dataSource) throws DAOException{
		
		DataSourceUtils.releaseConnection(connection, dataSource);
		try{
			if (statement != null){
				if (!statement.isClosed()){
					statement.close();
				}
			}
		} catch (SQLException exception){
			throw new DAOException("Error closing PreparedStatement.", exception);
		}
		
		try{
			if (resultSet != null){
				if (!resultSet.isClosed()){
					resultSet.close();
				}
			}				
		} catch (SQLException exception) {				
			throw new DAOException("Error closing ResultSet.", exception);
		}
	}
	
	/**
	 * Releases Connection and closes Statement
	 * @throws DAOException if SQLException was thrown.
	 */
	public static void close(Connection connection, Statement statement, DataSource dataSource) throws DAOException{
		
		DataSourceUtils.releaseConnection(connection, dataSource);
		
		try{
			if (statement != null){
				if (!statement.isClosed()){
					statement.close();
				}
			}
		} catch (SQLException exception){
			throw new DAOException("Error closing PreparedStatement.", exception);
		}
	}
}
