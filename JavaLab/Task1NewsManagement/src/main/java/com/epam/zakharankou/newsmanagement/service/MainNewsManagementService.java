package com.epam.zakharankou.newsmanagement.service;

import java.util.List;

import com.epam.zakharankou.newsmanagement.entity.Author;
import com.epam.zakharankou.newsmanagement.entity.Comment;
import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.ComplexNews;
import com.epam.zakharankou.newsmanagement.entity.SearchCriteria;
import com.epam.zakharankou.newsmanagement.entity.Tag;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

/**
 * MainNewsManagementService class is main interface of application.
 * @author Anton_Zakharankou
 *
 */
public interface MainNewsManagementService {
	
	/**
	 * Adds {@link com.epam.zakharankou.newsmanagement.entity.News} entity.
	 * @return id of added {@link com.epam.zakharankou.newsmanagement.entity.News} entity.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public Long	addNews(News news) throws ServiceException;
	
	/**
	 * Updates {@link com.epam.zakharankou.newsmanagement.entity.News} entity.
	 * @param {@link com.epam.zakharankou.newsmanagement.entity.News} - new values.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public void updateNews(News news) throws ServiceException;
	
	/**
	 * Deletes {@link com.epam.zakharankou.newsmanagement.entity.News} entity by newsId.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public void	deleteNews(Long newsId) throws ServiceException;
	
	/**
	 * Views list of {@link com.epam.zakharankou.newsmanagement.entity.News} entities sorted by the number of {@link com.epam.zakharankou.newsmanagement.entity.Comment} entities.
	 * @return list of {@link com.epam.zakharankou.newsmanagement.entity.News} entities.
	 * @throws ServiceException
	 */
	public List<News> viewListOfNews(Long startPosition, Long endPosition) throws ServiceException;
	
	/**
	 * Views single {@link com.epam.zakharankou.newsmanagement.entity.ComplexNews} entity.
	 * @return {@link com.epam.zakharankou.newsmanagement.entity.ComplexNews} entity.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public ComplexNews viewSingleNews(Long newsId) throws ServiceException;
	
	/**
	 * Adds {@link com.epam.zakharankou.newsmanagement.entity.Author} entity.
	 * @return id of added {@link com.epam.zakharankou.newsmanagement.entity.Author} entity.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public Long	addAuthor(Author author) throws ServiceException;
	
	/**
	 * Searches {@link com.epam.zakharankou.newsmanagement.entity.News} entities which meets criteria in database.
	 * @return list of {@link com.epam.zakharankou.newsmanagement.entity.News} entities.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public List<News> searchNews(SearchCriteria searchCriteria, Long startPosition, Long endPosition) throws ServiceException;	
	
	/**
	 * Adds {@link com.epam.zakharankou.newsmanagement.entity.Tag} entity.
	 * @return id of added {@link com.epam.zakharankou.newsmanagement.entity.Tag} entity.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public Long	addTag(Tag tag) throws ServiceException;	
	
	/**
	 * Adds {@link com.epam.zakharankou.newsmanagement.entity.Comment} entity.
	 * @return id of added {@link com.epam.zakharankou.newsmanagement.entity.Comment} entity.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public Long addComment(Comment comment) throws ServiceException;
	
	/**
	 * Deletes {@link com.epam.zakharankou.newsmanagement.entity.Comment} entity.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public void deleteComment(Long commentId) throws ServiceException;
	
	/**
	 * Deletes {@link com.epam.zakharankou.newsmanagement.entity.Tag} entity.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public void deleteTag(Long tagId) throws ServiceException;
	
	/**
	 * Adds {@link com.epam.zakharankou.newsmanagement.entity.ComplexNews} entity.
	 * @return id of added {@link com.epam.zakharankou.newsmanagement.entity.News} entity.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public Long addComplexNews(ComplexNews complexNews) throws ServiceException;
	
	/**
	 * Sets {@link com.epam.zakharankou.newsmanagement.entity.Author} entity properties expired.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public void deleteAuthor(Long authorId) throws ServiceException;
	
	/**
	 * Counts the number of records which meets criteria in database.
	 * @return the number of records.
	 * @throws ServiceException if SQLException was thrown.
	 */
	public Long countAllNews(SearchCriteria searchCriteria) throws ServiceException;
}
