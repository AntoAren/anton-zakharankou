package com.epam.zakharankou.newsmanagement.dao.implHB;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.epam.zakharankou.newsmanagement.dao.ICommentDao;
import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.Comment;

public class CommentDao implements ICommentDao{
	
	private SessionFactory sessionFactory;
	
	public Long create(Comment comment) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {	
			session.persist(comment);
			transaction.commit();
		} finally {
			session.close();
		}
		return comment.getCommentId();
	}

	public Comment readById(Long commentId) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Comment comment = null;
		try {
			comment = (Comment)session.createCriteria(Comment.class).add(Restrictions.eq("commentId", commentId)).uniqueResult(); 
		} finally {
			session.close();
		}
		return comment;
	}

	public boolean update(Comment comment) throws DAOException {
		boolean flag = true;
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();		
		try{
			Comment commentItem = (Comment)session.createCriteria(Comment.class).add(Restrictions.eq("commentId", comment.getCommentId())).uniqueResult();
			commentItem.setCommentText(comment.getCommentText());
			transaction.commit();
		} catch (HibernateException exception) {
			flag = false;
			transaction.rollback();
		} finally {
			session.close();
		}
		return flag;
	}

	public void delete(Long commentId) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Comment comment = (Comment)session.createCriteria(Comment.class).add(Restrictions.eq("commentId", commentId)).uniqueResult();
			session.delete(comment);
			transaction.commit();
		} finally {
			session.close();
		}
	}
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
