package com.epam.zakharankou.newsmanagement.service;

import java.util.List;

import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.SearchCriteria;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

/**
 * NewsService class is service which work with {@link com.epam.zakharankou.newsmanagement.entity.News} action.
 * @author Anton_Zakharankou
 *
 */
public interface NewsService {
	
	/**
	 * @see MainNewsManagementService#addNews(News)
	 */
	public Long	addNews(News news) throws ServiceException;
	
	/**
	 * @see MainNewsManagementService#updateNews(News)
	 */
	public void updateNews(News news) throws ServiceException;
	
	/**
	 * @see MainNewsManagementService#deleteNews(Long)
	 */
	public void	deleteNews(Long newsId) throws ServiceException;
	
	/**
	 * @see MainNewsManagementService#viewListOfNews(Long, Long)
	 */
	public List<News> viewListOfNews(Long startPosition, Long endPosition) throws ServiceException;
	
	/**
	 * @see MainNewsManagementService#viewSingleNews(Long)
	 */
	public News	viewSingleNews(Long newsId) throws ServiceException;
	
	/**
	 * @see MainNewsManagementService#searchNews(SearchCriteria, Long, Long)
	 */
	public List<News> searchNews(SearchCriteria searchCriteria, Long startPosition, Long endPosition) throws ServiceException;
	
	/**
	 * @see MainNewsManagementService#countAllNews(SearchCriteria)
	 */
	public Long countAllNews(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * @see MainNewsManagementService#getNextNewsId(SearchCriteria, Long)
	 */
	public Long getNextNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException;
	
	/**
	 * @see MainNewsManagementService#getPrevNewsId(SearchCriteria, Long)
	 */
	public Long getPrevNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException;
	
	/**
	 * @see MainNewsManagementService#getCurrentPageForNews(SearchCriteria, Long)
	 */
	public Long getCurrentPageForNews(SearchCriteria searchCriteria, Long newsId) throws ServiceException;
}
