package com.epam.zakharankou.newsmanagement.dao;

import java.util.List;

import com.epam.zakharankou.newsmanagement.dao.GenericDao;
import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.Tag;

/**
 * ITagDao is interface which works with {@link com.epam.zakharankou.newsmanagement.entity.Tag} entity.
 * @author Anton_Zakharankou
 *
 */
public interface ITagDao extends GenericDao<Tag> {	
		
	/**
	 * Reads all {@link com.epam.zakharankou.newsmanagement.entity.Tag} entities from database.
	 * @return list of {@link com.epam.zakharankou.newsmanagement.entity.Tag} entities.
	 * @throws DAOException if SQLException was thrown.
	 */
	public List<Tag> readAll() throws DAOException;	
}