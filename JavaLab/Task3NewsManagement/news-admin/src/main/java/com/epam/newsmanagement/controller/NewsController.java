package com.epam.newsmanagement.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;





import com.epam.newsmanagement.utils.ConverterUtility;
import com.epam.newsmanagement.utils.DateUtility;
import com.epam.zakharankou.newsmanagement.entity.Author;
import com.epam.zakharankou.newsmanagement.entity.Comment;
import com.epam.zakharankou.newsmanagement.entity.ComplexNews;
import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.SearchCriteria;
import com.epam.zakharankou.newsmanagement.entity.Tag;
import com.epam.zakharankou.newsmanagement.service.MainNewsManagementService;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

@Controller
@RequestMapping(value = "/news")
public class NewsController {
	
	@Autowired
	private MainNewsManagementService mainNewsManagementService; 
		
	@RequestMapping(value = "")
	public String startPage( HttpSession session, Model model){
		return "redirect:/news/pages/1";
	}
	
	@RequestMapping(value = "/pages/{pageNumber}")
	public String showPageNews(@PathVariable Long pageNumber, HttpSession session, Model model){
		try {
			setSearchCriteria(session);
			
			Long startPosition = (pageNumber - 1)*3 + 1;
			Long endPosition = pageNumber*3;
			
			List<Tag> listTags = mainNewsManagementService.readAllTags();
			List<Author> listAuthors = mainNewsManagementService.readAllAuthors();					
			List<News> listNews = mainNewsManagementService.searchNews((SearchCriteria)session.getAttribute("searchCriteria"), startPosition, endPosition);			
			Long countNews = mainNewsManagementService.countAllNews((SearchCriteria)session.getAttribute("searchCriteria"));
			Long countPage = (countNews - 1)/3 + 1;						
			
			model.addAttribute("searchCriteria", new SearchCriteria());
			model.addAttribute("countPage", countPage);
			model.addAttribute("pageNumber", pageNumber);
			model.addAttribute("listNews", listNews);
			model.addAttribute("authors", listAuthors);
			model.addAttribute("tags", listTags);
			model.addAttribute("currentPage", "newsList");
		} catch (ServiceException exception) {
			return "error";
		}
		return "newsList";
	}
	
	@RequestMapping(value = "/delete")
	public String deleteNews(@RequestParam(value = "newsIds", required = false ) List<Long> newsIds, HttpSession session, Model model){
		try{
			if ((newsIds != null) && (newsIds.size() > 0)){
				for (Long newsId: newsIds){
					mainNewsManagementService.deleteNews(newsId);
				}
			} else {
				return "redirect:/news/pages/1";
			}
		} catch (ServiceException exception){
			return "error";
		}
		return "redirect:/news/pages/1";
	}
	
	@RequestMapping(value = "/addNews")
	public String addNews(HttpSession session, Model model){
		try{
			List<Tag> listTags = mainNewsManagementService.readAllTags();
			List<Author> listAuthors = mainNewsManagementService.readAllNotExpiredAuthors();			
			
			model.addAttribute("date", DateUtility.getCurrentDate());
			model.addAttribute("authors", listAuthors);
			model.addAttribute("tags", listTags);
			model.addAttribute("complexNews", new ComplexNews());
			model.addAttribute("currentPage", "addNews");
		} catch (ServiceException exception){
			return "error";
		}		
		return "addNews";
	}
	
	@RequestMapping(value = "/saveNews")
	public String saveNews(@Valid @ModelAttribute("complexNews") ComplexNews complexNews, BindingResult bindingResult, HttpSession session, Model model, @RequestParam(value = "creationDate", required = false) String creationDate){
		Long newsId = null;
		try{
			if (bindingResult.hasErrors()){
				List<Tag> listTags = mainNewsManagementService.readAllTags();
				List<Author> listAuthors = mainNewsManagementService.readAllNotExpiredAuthors();
				System.out.println(complexNews.getAuthorId());	
				model.addAttribute("date", DateUtility.getCurrentDate());
				model.addAttribute("authors", listAuthors);
				model.addAttribute("tags", listTags);				
				model.addAttribute("currentPage", "addNews");
				
				return "addNews";
			}
			newsId = mainNewsManagementService.addNews(complexNews.getNews(), complexNews.getTagIds(), complexNews.getAuthorId());			
		} catch (ServiceException exception){
			return "error";
		}
		return "redirect:/news/showNews/" + newsId;
	}
	
	@RequestMapping(value = "/edit/{newsId}")
	public String editNews(HttpSession session, Model model, @PathVariable Long newsId){
		try{
			List<Tag> listTags = mainNewsManagementService.readAllTags();
			List<Author> listAuthors = mainNewsManagementService.readAllNotExpiredAuthors();
							
			News news = mainNewsManagementService.viewSingleNews(newsId);		
			ComplexNews complexNews = ConverterUtility.convertToComplexNews(news);
						
			model.addAttribute("authors", listAuthors);
			model.addAttribute("tags", listTags);
			model.addAttribute("complexNews", complexNews);
			model.addAttribute("currentPage", "newsList");
		} catch (ServiceException exception){
			return "error";
		}
		return "editNews";
	}
	
	@RequestMapping(value = "/updateNews")
	public String updateNews(@Valid @ModelAttribute("complexNews") ComplexNews complexNews, BindingResult bindingResult, HttpSession session, Model model, @RequestParam(value = "creationDate", required = false) String creationDate){
		try{
			if (bindingResult.hasErrors()){
				List<Tag> listTags = mainNewsManagementService.readAllTags();
				List<Author> listAuthors = mainNewsManagementService.readAllNotExpiredAuthors();
				
				model.addAttribute("authors", listAuthors);
				model.addAttribute("tags", listTags);				
				model.addAttribute("currentPage", "newsList");
				
				return "editNews";
			}
			Date date =	DateUtility.checkDate(creationDate);
			if (date != null){				
				News news = complexNews.getNews();
				news.setCreationDate(date);
				if (mainNewsManagementService.updateNews(news, complexNews.getAuthorId(), complexNews.getTagIds())){
					return "redirect:/news/pages/1";
				} else {
					try{
						List<Tag> listTags = mainNewsManagementService.readAllTags();
						List<Author> listAuthors = mainNewsManagementService.readAllNotExpiredAuthors();
										
						News newsItem = mainNewsManagementService.viewSingleNews(news.getNewsId());		
						ComplexNews complexNewsItem = ConverterUtility.convertToComplexNews(newsItem);
									
						model.addAttribute("authors", listAuthors);
						model.addAttribute("updateError", true);
						model.addAttribute("tags", listTags);
						model.addAttribute("complexNews", complexNewsItem);
						model.addAttribute("currentPage", "newsList");
					} catch (ServiceException exception){
						return "error";
					}
					return "editNews";
				}
			} else {
				Long newsId = complexNews.getNews().getNewsId();
				return "redirect:/news/edit/" + newsId;
			}
		} catch (ServiceException exception){
			return "error";
		}
	}
	
	@RequestMapping(value = "/showNews/{newsId}")
	public String showNews(HttpSession session, Model model, @PathVariable Long newsId){
		try{
			setSearchCriteria(session);
			Long nextNewsId = null;
			Long prevNewsId = null;
			News news = mainNewsManagementService.viewSingleNews(newsId);
			
			if (news != null){
				nextNewsId = mainNewsManagementService.getNextNewsId((SearchCriteria)session.getAttribute("searchCriteria"), newsId);
				prevNewsId = mainNewsManagementService.getPrevNewsId((SearchCriteria)session.getAttribute("searchCriteria"), newsId);
			}
				
			session.setAttribute("newsId", newsId);
			model.addAttribute("comment", new Comment());
			model.addAttribute("nextNewsId", nextNewsId);
			model.addAttribute("prevNewsId", prevNewsId);
			model.addAttribute("news", news);
			model.addAttribute("currentPage", "newsList");
		} catch (ServiceException exception){
			return "error";
		}
		return "singleNews";
	}
		
	@RequestMapping(value = "/postComment")
	public String postComment(@Valid @ModelAttribute("comment") Comment comment, BindingResult result, Model model, HttpSession session){
		setSearchCriteria(session);
		Long newsId = (Long)session.getAttribute("newsId");		
		try{
			
			if (result.hasErrors()){				
				
				News news = mainNewsManagementService.viewSingleNews(newsId);
				System.out.println(news);
				Long nextNewsId = mainNewsManagementService.getNextNewsId((SearchCriteria)session.getAttribute("searchCriteria"), newsId);
				Long prevNewsId = mainNewsManagementService.getPrevNewsId((SearchCriteria)session.getAttribute("searchCriteria"), newsId);
				
				model.addAttribute("nextNewsId", nextNewsId);
				model.addAttribute("prevNewsId", prevNewsId);
				model.addAttribute("news", news);
				model.addAttribute("currentPage", "newsList");
				return "singleNews";
			} else {
				mainNewsManagementService.addComment(comment);
			}
						
			News news = mainNewsManagementService.viewSingleNews(newsId);
			model.addAttribute("complexNews", news);
		} catch (ServiceException exception){
			return "error";
		}
		return "redirect:/news/showNews/" + newsId;
	}
	
	@RequestMapping(value = "/deleteComment")
	public String deleteComment(Comment comment, HttpSession session, Model model){
		try{
			mainNewsManagementService.deleteComment(comment.getCommentId());
		} catch (ServiceException exception){
			return "error";
		}
		return "redirect:/news/showNews/" + comment.getNewsId();
	}
	
	@RequestMapping(value = "/back/{newsId}")
	public String backToListNews(HttpSession session, @PathVariable Long newsId, Model model) {
		setSearchCriteria(session);
		Long currentPage = null;
		try{
			Long position = mainNewsManagementService.getCurrentPageForNews((SearchCriteria)session.getAttribute("searchCriteria"), newsId);
			currentPage = (position - 1)/3 + 1;
		} catch (ServiceException exception){
			return "error";
		}
		return "redirect:/news/pages/" + currentPage;
	}
	
	@RequestMapping(value = "/filter")
	public String filterNews(HttpSession session, SearchCriteria searchCriteria, Model model){
		
		if(searchCriteria.getAuthorId() == 0){
			searchCriteria.setAuthorId(null);
		}
		session.setAttribute("searchCriteria", searchCriteria);
		
		return "redirect:/news/pages/1";
	}
	
	@RequestMapping(value = "/reset")
	public String resetFilter(HttpSession session, Model model){
		
		session.setAttribute("searchCriteria", new SearchCriteria());
		
		return "redirect:/news/pages/1";
	}
	
	private void setSearchCriteria(HttpSession session){
		if (session.getAttribute("searchCriteria") == null){
			session.setAttribute("searchCriteria", new SearchCriteria());
		}
	}
}
