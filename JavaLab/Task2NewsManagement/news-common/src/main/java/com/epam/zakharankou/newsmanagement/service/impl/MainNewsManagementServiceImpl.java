package com.epam.zakharankou.newsmanagement.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.zakharankou.newsmanagement.entity.Author;
import com.epam.zakharankou.newsmanagement.entity.Comment;
import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.ComplexNews;
import com.epam.zakharankou.newsmanagement.entity.SearchCriteria;
import com.epam.zakharankou.newsmanagement.entity.Tag;
import com.epam.zakharankou.newsmanagement.entity.User;
import com.epam.zakharankou.newsmanagement.service.AuthorService;
import com.epam.zakharankou.newsmanagement.service.CommentService;
import com.epam.zakharankou.newsmanagement.service.MainNewsManagementService;
import com.epam.zakharankou.newsmanagement.service.NewsService;
import com.epam.zakharankou.newsmanagement.service.TagService;
import com.epam.zakharankou.newsmanagement.service.UserService;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

/**
 * MainNewsManagementServiceImpl class implements {@link com.epam.zakharankou.newsmanagement.service.MainNewsManagementService}.
 * @author Anton_Zakharankou
 *
 */
@Transactional(rollbackFor = ServiceException.class)
public class MainNewsManagementServiceImpl implements MainNewsManagementService {	
	
	private NewsService newsService;
	private AuthorService authorService;
	private CommentService commentService;
	private TagService tagService;	
	private UserService userService;	

	/**
	 * @see MainNewsManagementService#addNews(News)
	 */
	public Long addNews(News news) throws ServiceException {
		Long newsId = newsService.addNews(news);
		return newsId;
	}

	/**
	 * @see MainNewsManagementService#updateNews(News)
	 */
	public void updateNews(News news, Long authorId, List<Long> tagIds) throws ServiceException {
		authorService.deleteLinkAuthorWithNews(news.getNewsId());
		tagService.deleteLinksTagWithNews(news.getNewsId());
		authorService.createLinkAuthorWithNews(authorId, news.getNewsId());
		tagService.createLinksTagWithNews(tagIds, news.getNewsId());
		newsService.updateNews(news);
	}

	/**
	 * @see MainNewsManagementService#deleteNews(Long)
	 */
	public void deleteNews(Long newsId) throws ServiceException {
		commentService.deleteCommentsByNewsId(newsId);
		tagService.deleteLinksTagWithNews(newsId);
		authorService.deleteLinkAuthorWithNews(newsId);
		newsService.deleteNews(newsId);
	}

	/**
	 * @see MainNewsManagementService#viewListOfNews(Long, Long)
	 */
	public List<News> viewListOfNews(Long startPosition, Long endPosition) throws ServiceException {
		List<News> news = newsService.viewListOfNews(startPosition, endPosition);
		return news;
	}

	/**
	 * @see MainNewsManagementService#viewSingleNews(Long)
	 */
	public ComplexNews viewSingleNews(Long newsId) throws ServiceException {
		ComplexNews complexNews = new ComplexNews();
		News news = newsService.viewSingleNews(newsId);
		Author author = authorService.getAuthorByNewsId(newsId);
		List<Comment> comments = commentService.getCommentsByNewsId(newsId);
		List<Tag> tags = tagService.getTagsByNewsId(newsId);
		complexNews.setNews(news);
		complexNews.setAuthor(author);
		complexNews.setComments(comments);
		complexNews.setTags(tags);
		return complexNews;
	}

	/**
	 * @see MainNewsManagementService#addAuthor(Author)
	 */
	public Long addAuthor(Author author) throws ServiceException {
		Long authorId = authorService.addAuthor(author);
		return authorId;
	}

	/**
	 * @see MainNewsManagementService#searchNews(SearchCriteria, Long, Long)
	 */
	public List<News> searchNews(SearchCriteria searchCriteria, Long startPosition, Long endPosition)	throws ServiceException {
		List<News> news = newsService.searchNews(searchCriteria, startPosition, endPosition);
		return news;
	}

	/**
	 * @see MainNewsManagementService#addTag(Tag)
	 */
	public Long addTag(Tag tag) throws ServiceException {
		Long tagIdLong = tagService.addTag(tag);
		return tagIdLong;
	}

	/**
	 * @see MainNewsManagementService#addComplexNews(ComplexNews)
	 */
	public Long addComplexNews(News news, Long authorId, List<Long> tagIds) throws ServiceException {
		Long newsId = newsService.addNews(news);
		authorService.createLinkAuthorWithNews(authorId, newsId);
		if (tagIds != null){
			tagService.createLinksTagWithNews(tagIds, newsId);
		}
		return newsId;
	}
	
	/**
	 * @see MainNewsManagementService#addComment(Comment) 
	 */
	public Long addComment(Comment comment) throws ServiceException {
		Long commentId = commentService.addComment(comment);
		return commentId;
	}

	/**
	 * @see MainNewsManagementService#deleteComment(Long)
	 */
	public void deleteComment(Long commentId) throws ServiceException {
		commentService.deleteComment(commentId);		
	}

	/**
	 * @see MainNewsManagementService#deleteTag(Long)
	 */
	public void deleteTag(Long tagId) throws ServiceException {
		tagService.deleteLinksByTagId(tagId);
		tagService.deleteTag(tagId);
	}
	
	/**
	 * @see MainNewsManagementService#deleteAuthor(Long)
	 */
	public void deleteAuthor(Long authorId) throws ServiceException {
		authorService.setExpiredAuthor(authorId);		
	}

	/**
	 * @see MainNewsManagementService#countAllNews(SearchCriteria)
	 */
	public Long countAllNews(SearchCriteria searchCriteria)	throws ServiceException {
		Long countAllNews = newsService.countAllNews(searchCriteria);
		return countAllNews;
	}
	
	/**
	 * @see MainNewsManagementService#readAllAuthors()
	 */
	public List<Author> readAllAuthors() throws ServiceException {
		List<Author> listAuthor = authorService.readAllAuthors();  
		return listAuthor;
	}
	
	/**
	 * @see MainNewsManagementService#readAllAuthors()
	 */
	public List<Tag> readAllTags() throws ServiceException {
		List<Tag> listTags = tagService.readAllTags();  
		return listTags;
	}
	
	/**
	 * @see MainNewsManagementService#getNextNewsId(SearchCriteria, Long)
	 */
	public Long getNextNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
		Long nextNewsId = newsService.getNextNewsId(searchCriteria, newsId);
		return nextNewsId;
	}

	/**
	 * @see MainNewsManagementService#getPrevNewsId(SearchCriteria, Long)
	 */
	public Long getPrevNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
		Long prevNewsId = newsService.getPrevNewsId(searchCriteria, newsId);
		return prevNewsId;
	}		
	
	/**
	 * @see MainNewsManagementService#getCurrentPageForNews(SearchCriteria, Long) 
	 */
	public Long getCurrentPageForNews(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
		Long pageNumber = newsService.getCurrentPageForNews(searchCriteria, newsId);
		return pageNumber;
	}
	
	/**
	 * @see MainNewsManagementService#hasUser(User)
	 */
	public User hasUser(User user) throws ServiceException {
		User readUser = userService.hasUser(user);
		return readUser;
	}
	
	/**
	 * @see MainNewsManagementService#updateAuthor(Author)
	 */
	public void updateAuthor(Author author) throws ServiceException{
		authorService.updateAuthor(author);
	}
	
	/**
	 * @see MainNewsManagementService#updateTag(Tag)
	 */
	public void updateTag(Tag tag) throws ServiceException{
		tagService.updateTag(tag);
	}
	
	/**
	 * @see MainNewsManagementService#readAllNotExpiredAuthors()
	 */
	public List<Author> readAllNotExpiredAuthors() throws ServiceException{
		List<Author> authors = authorService.readAllNotExpiredAuthors();
		return authors;
	}
	
	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}
	
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}