package com.epam.zakharankou.newsmanagement.service.impl;

import java.util.Date;

import org.apache.log4j.Logger;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.dao.ICommentDao;
import com.epam.zakharankou.newsmanagement.entity.Comment;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;
import com.epam.zakharankou.newsmanagement.service.CommentService;

/**
 * CommentServiceImpl class implements {@link com.epam.zakharankou.newsmanagement.service.CommentService} interface.
 * @author Anton_Zakharankou
 *
 */
public class CommentServiceImpl implements CommentService {

	private static final Logger logger = Logger.getLogger("RollingFileAppender");
	
	private ICommentDao commentDao;
	
	/**
	 * @see CommentService#addComment(Comment)
	 */
	public Long addComment(Comment comment) throws ServiceException {
		Long commentId = null;
		try{
			comment.setCreationDate(new Date());
			commentId = commentDao.create(comment);
		} catch (DAOException exception){
			logger.error("Error adding comment entity.", exception);
			throw new ServiceException("Error adding comment entity.", exception);
		}
		return commentId;
	}

	/**
	 * @see CommentService#deleteComment(Long)
	 */
	public void deleteComment(Long commentId) throws ServiceException {
		try{
			commentDao.delete(commentId);
		} catch (DAOException exception){
			logger.error("Error deleting comment entity.", exception);
			throw new ServiceException("Error deleting comment entity.", exception);
		}
	}
	
	public void setCommentDao(ICommentDao commentDao) {
		this.commentDao = commentDao;
	}
}
