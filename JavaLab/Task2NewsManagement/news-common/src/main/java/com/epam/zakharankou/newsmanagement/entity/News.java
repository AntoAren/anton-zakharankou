package com.epam.zakharankou.newsmanagement.entity;

import java.sql.Date;
import java.sql.Timestamp;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * News class provides news entity.
 * @author Anton_Zakharankou
 *
 */
public class News{
	private Long newsId;
	
	@NotNull(message = "{error.news.title.notNull}")
	@Size(min = 1, max = 30, message = "{error.news.title.size}")
	private String title;
	
	@NotNull(message = "{error.news.shortText.notNull}")
	@Size(min = 1, max = 100, message = "{error.news.shortText.size}")
	private String shortText;
	
	@NotNull(message = "{error.news.fullText.notNull}")
	@Size(min = 1, max = 2000, message = "{error.news.fullText.size}")
	private String fullText;
	
	private Timestamp creationDate;
	
	private Date modificationDate;
	
	public Long getNewsId() {
		return newsId;
	}
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getShortText() {
		return shortText;
	}
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}
	public String getFullText() {
		return fullText;
	}
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public Date getModificationDate() {
		return modificationDate;
	}
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;		
		result = prime * result	+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result	+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result	+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.intValue());
		result = prime * result	+ ((shortText == null) ? 0 : shortText.hashCode());		
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;		
		if (creationDate == null) {
			if (other.getCreationDate() != null)
				return false;
		} else if (!creationDate.equals(other.getCreationDate()))
			return false;
		if (fullText == null) {
			if (other.getFullText() != null)
				return false;
		} else if (!fullText.equals(other.getFullText()))
			return false;
		if (modificationDate == null) {
			if (other.getModificationDate() != null)
				return false;
		} else if (!modificationDate.equals(other.getModificationDate()))
			return false;
		if (newsId != other.getNewsId())
			return false;
		if (shortText == null) {
			if (other.getShortText() != null)
				return false;
		} else if (!shortText.equals(other.getShortText()))
			return false;		
		if (title == null) {
			if (other.getTitle() != null)
				return false;
		} else if (!title.equals(other.getTitle()))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "News [newsId=" + newsId + ", title=" + title + ", shortText="
				+ shortText + ", fullText=" + fullText + ", creationDate="
				+ creationDate + ", modificationDate=" + modificationDate
				+ "]";
	}
	
		
}
