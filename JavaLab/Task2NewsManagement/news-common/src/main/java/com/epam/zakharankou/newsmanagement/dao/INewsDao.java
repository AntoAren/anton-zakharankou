package com.epam.zakharankou.newsmanagement.dao;

import java.util.List;

import com.epam.zakharankou.newsmanagement.dao.GenericDao;
import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.SearchCriteria;

/**
 * INewsDao is interface which works with {@link com.epam.zakharankou.newsmanagement.entity.News} entity.
 * @author Anton_Zakharankou
 *
 */
public interface INewsDao extends GenericDao<News>{	
	
	/**
	 * Reads a certain number of records {@link com.epam.zakharankou.newsmanagement.entity.News} entities from database.
	 * @return list of {@link com.epam.zakharankou.newsmanagement.entity.News} entities.
	 * @throws DAOException if SQLException was thrown.
	 */
	public List<News> read(Long startPosition, Long endPosition) throws DAOException;
	
	/**
	 * Reads all {@link com.epam.zakharankou.newsmanagement.entity.News} entities from database.
	 * @return list of {@link com.epam.zakharankou.newsmanagement.entity.News} entities.
	 * @throws DAOException if SQLException was thrown.
	 */
	public List<News> readAll() throws DAOException;
	
	/**
	 * Searches {@link com.epam.zakharankou.newsmanagement.entity.News} entities which meets criteria in database.
	 * @return list of {@link com.epam.zakharankou.newsmanagement.entity.News} entities.
	 * @throws DAOException if SQLException was thrown.
	 */
	public List<News> searchNews(SearchCriteria searchCriteria, Long startPosition, Long endPosition) throws DAOException;
	
	/**
	 * Counts the number of records which meets criteria in database.
	 * @return the number of records.
	 * @throws DAOException if SQLException was thrown.
	 */
	public Long countAllNews(SearchCriteria searchCriteria) throws DAOException;
	
	/**
	 * Reads next NEWS_ID for current newsId. 
	 * @return next NEWS_ID.
	 * @throws DAOException if SQLException was thrown.
	 */
	public Long getNextNewsId(SearchCriteria searchCriteria, Long newsId) throws DAOException;
	
	/**
	 * Reads previous NEWS_ID for current newsId. 
	 * @return previous NEWS_ID.
	 * @throws DAOException if SQLException was thrown.
	 */
	public Long getPrevNewsId(SearchCriteria searchCriteria, Long newsId) throws DAOException;
	
	/**
	 * Reads current page for news.
	 * @return current page.
	 * @throws DAOException if SQLException was thrown.
	 */
	public Long getCurrentPageForNews(SearchCriteria searchCriteria, Long newsId) throws DAOException;
}
