<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>

<div class="divNewsList">
	<table class="tableAuthors">
		<c:forEach var="author" items="${authors}">			
			<sf:form commandName="author" action="/news-admin/author/updateAuthor" method="post">
				<tr>								
					<td class="trTableAuthors">
						<b><spring:message code="local.author"/>:</b>
					</td>
					<td class="trTableAuthors">
						<sf:input path="authorName" value="${author.authorName}" readonly="true" cssClass="inputTableAuthors" id="text${author.authorId}"/>												
					</td>					
					<td class="trTableAuthors">						
						<input type="hidden" id="hiddenAuthorId${author.authorId}" value="${author.authorName}">
						<p class="linkText" id="${author.authorId}"  onclick="toggleShow(${author.authorId})"><spring:message code="local.editAuthor"/></p>
						<div style="display: none; width: 250px;" id="hide${author.authorId}" >
							<input class="buttonLikeLink" type="submit" value="<spring:message code="local.updateAuthor"/>">							
							<sf:hidden path="authorId" value="${author.authorId}"/>						
							<a class="linkText" href="/news-admin/author/expireAuthor/${author.authorId}"><spring:message code="local.expireAuthor"/></a> 
							<a class="linkText" onclick="toggleHide(${author.authorId})" ><spring:message code="local.cancelAuthor"/></a>
						</div>
					</td>				
				</tr>
				<c:if test="${author.authorId == authorIdError}">
					<tr>
						<td colspan="3">
							<sf:errors path="authorName" cssClass="error"></sf:errors>
						</td>
					</tr>
				</c:if>
			</sf:form>
		</c:forEach>
		<sf:form commandName="author" action="/news-admin/author/saveAuthor">
			<tr>
				<td>
					<b><spring:message code="local.addAuthor"/></b>
				</td>
				<td>
					<sf:input path="authorName" cssClass="inputTableAuthors"/>					
				</td>
				<td>
					<input type="submit" value="<spring:message code="local.save"/>">
				</td>							
			</tr>
			<c:if test="${authorIdError == null }">
				<tr>
					<td colspan="3">
						<sf:errors path="authorName" cssClass="error"></sf:errors>
					</td>
				</tr>
			</c:if>
		</sf:form>
	</table>
</div>