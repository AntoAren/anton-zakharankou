package com.epam.zakharankou.newsmanagement.dao.implHB;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import com.epam.zakharankou.newsmanagement.entity.Tag;

import static org.junit.Assert.*;

@SpringApplicationContext({"configTest.xml"})
@DataSet(loadStrategy = CleanInsertLoadStrategy.class, value = {"dataSetTest.xml"})
public class TagDaoTest extends UnitilsJUnit4{
	
	@SpringBean("tagDao")
	private TagDao tagDao;
	
	@Test
	public void createTest() throws Exception{
		Tag expectedTag = new Tag();
		expectedTag.setTagName("testName");
		
		Long tagId = tagDao.create(expectedTag);
		Tag actualTag = tagDao.readById(tagId);
		
		assertEquals(expectedTag.getTagName(), actualTag.getTagName());
	}
	
	@Test
	public void readByIdTest() throws Exception{
		Tag expectedTag = new Tag();
		expectedTag.setTagId(1L);
		expectedTag.setTagName("Tag1");
		
		Tag actualTag = tagDao.readById(1L);
		
		assertEquals(expectedTag, actualTag);
	}
	
	@Test
	public void updateTest() throws Exception{
		Tag expectedTag = new Tag();
		expectedTag.setTagId(1L);
		expectedTag.setTagName("newName");
		
		tagDao.update(expectedTag);
		Tag actualTag = tagDao.readById(expectedTag.getTagId());
		
		assertEquals(expectedTag, actualTag);
	}
	
	@Test
	public void deleteTest() throws Exception{
		Tag writtingTag = new Tag();
		writtingTag.setTagName("testName");
				
		Long tagId = tagDao.create(writtingTag);
		
		tagDao.delete(tagId);
		Tag actualTag = tagDao.readById(tagId);
		
		assertNull(actualTag);
	}
	
	@Test
	public void readAllTest() throws Exception{
		Tag firstTag = new Tag();
		Tag secondTag = new Tag();
		Tag thirdTag = new Tag();
		Tag fourthTag = new Tag();
		firstTag.setTagId(new Long(1L));
		firstTag.setTagName("Tag1");
		secondTag.setTagId(new Long(2L));
		secondTag.setTagName("Tag2");
		thirdTag.setTagId(new Long(3L));
		thirdTag.setTagName("Tag3");
		fourthTag.setTagId(new Long(4L));
		fourthTag.setTagName("Tag4");
		List<Tag> expectedTagList = new ArrayList<Tag>();
		expectedTagList.add(firstTag);
		expectedTagList.add(secondTag);
		expectedTagList.add(thirdTag);
		expectedTagList.add(fourthTag);
		
		List<Tag> actualTagList = tagDao.readAll();
		
		assertEquals(expectedTagList, actualTagList);	
	}
}
