package com.epam.newsmanagement.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.zakharankou.newsmanagement.service.MainNewsManagementService;

@Controller
public class LoginController {
	
	@Autowired
	private MainNewsManagementService mainNewsManagementService;
	
	@RequestMapping(value = "/login")
	public String login() {
		
		return "login";
	}

	@RequestMapping(value = "/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "login";
	}
	 
	@RequestMapping(value = "")
	public String startPage(){
		return "redirect:/news/pages/1";
	}
}
