package com.epam.newsmanagement.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.zakharankou.newsmanagement.entity.Author;
import com.epam.zakharankou.newsmanagement.entity.Comment;
import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.SearchCriteria;
import com.epam.zakharankou.newsmanagement.entity.Tag;
import com.epam.zakharankou.newsmanagement.service.MainNewsManagementService;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

@Controller
@RequestMapping("/")
public class HomeController {
	
	@Autowired
	private MainNewsManagementService mainNewsManagementService;
		
	private void setSearchCriteria(HttpSession session){
		if (session.getAttribute("searchCriteria") == null){
			session.setAttribute("searchCriteria", new SearchCriteria());
		}
	}
		
	@RequestMapping("/")
	public String showHomePage(HttpSession session, Model model){	
		return "redirect:/pages/1";
	}
	
	@RequestMapping("/pages/{pageNumber}")
	public String showNewsByPageNumber(HttpSession session, @PathVariable Long pageNumber, Model model){
		try{
			setSearchCriteria(session);
			
			Long startPosition = (pageNumber - 1)*3 + 1;
			Long endPosition = pageNumber*3;
			
			List<Tag> listTags = mainNewsManagementService.readAllTags();
			List<Author> listAuthors = mainNewsManagementService.readAllAuthors();
			
			List<News> listNews = mainNewsManagementService.searchNews((SearchCriteria)session.getAttribute("searchCriteria"),startPosition, endPosition);
						
			Long countNews = mainNewsManagementService.countAllNews((SearchCriteria)session.getAttribute("searchCriteria"));
			Long countPage = (countNews - 1)/3 + 1;			
			
			model.addAttribute("searchCriteria", new SearchCriteria());
			model.addAttribute("tags", listTags);
			model.addAttribute("countPage", countPage);
			model.addAttribute("pageNumber", pageNumber);
			model.addAttribute("listNews", listNews);
			model.addAttribute("authors", listAuthors);
		} catch (ServiceException exception) {
			return "error";
		}
		return "home";
	}
	
	@RequestMapping("/showSingleNews/{newsId}")
	public String showSingleNews(HttpSession session, @PathVariable Long newsId, Model model){
		try{
			Long nextNewsId = null;
			Long prevNewsId = null;
			News news = mainNewsManagementService.viewSingleNews(newsId);
			
			if (news != null){
				nextNewsId = mainNewsManagementService.getNextNewsId((SearchCriteria)session.getAttribute("searchCriteria"), newsId);
				prevNewsId = mainNewsManagementService.getPrevNewsId((SearchCriteria)session.getAttribute("searchCriteria"), newsId);
			}
						
			session.setAttribute("newsId", newsId);
			model.addAttribute("comment", new Comment());
			model.addAttribute("nextNewsId", nextNewsId);
			model.addAttribute("prevNewsId", prevNewsId);
			model.addAttribute("news", news);
		} catch (ServiceException exception){
			return "error";
		}
		return "showSingleNews";
	}
	
	@RequestMapping("/filter")
	public String filterNews(HttpSession session, SearchCriteria searchCriteria, Model model){
				
		if(searchCriteria.getAuthorId() == 0){
			searchCriteria.setAuthorId(null);
		}
		
		session.setAttribute("searchCriteria", searchCriteria);
			
		return "redirect:/pages/1";
	}
	
	@RequestMapping("/reset")
	public String resetFilter(HttpSession session, Model model){
		
		session.setAttribute("searchCriteria", new SearchCriteria());
		
		return "redirect:/pages/1";
	}
	
	@RequestMapping("/postComment")
	public String postComment(@Valid Comment comment, BindingResult bindingResult, HttpSession session, Model model){
		setSearchCriteria(session);
		Long newsId = (Long)session.getAttribute("newsId");
		try{
			if (bindingResult.hasErrors()){
				
				News news = mainNewsManagementService.viewSingleNews(newsId);
				Long nextNewsId = mainNewsManagementService.getNextNewsId((SearchCriteria)session.getAttribute("searchCriteria"), newsId);
				Long prevNewsId = mainNewsManagementService.getPrevNewsId((SearchCriteria)session.getAttribute("searchCriteria"), newsId);
				
				model.addAttribute("nextNewsId", nextNewsId);
				model.addAttribute("prevNewsId", prevNewsId);
				model.addAttribute("news", news);
				
				return "showSingleNews";
			}
			
			mainNewsManagementService.addComment(comment);
			
			News news = mainNewsManagementService.viewSingleNews(newsId);
			model.addAttribute("news", news);
		} catch (ServiceException exception){
			return "error";
		}
		return "redirect:/showSingleNews/" + newsId;
	}
	
	@RequestMapping("/back/{newsId}")
	public String backToListNews(HttpSession session, @PathVariable Long newsId, Model model) {
		setSearchCriteria(session);
		Long currentPage = null;
		try{
			Long position = mainNewsManagementService.getCurrentPageForNews((SearchCriteria)session.getAttribute("searchCriteria"), newsId);
			currentPage = (position - 1)/3 + 1;
		} catch (ServiceException exception){
			return "error";
		}
		return "redirect:/pages/" + currentPage;
	}
}
