package com.epam.zakharankou.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.zakharankou.newsmanagement.entity.Author;
import com.epam.zakharankou.newsmanagement.entity.Comment;
import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.ComplexNews;
import com.epam.zakharankou.newsmanagement.entity.SearchCriteria;
import com.epam.zakharankou.newsmanagement.entity.Tag;
import com.epam.zakharankou.newsmanagement.service.AuthorService;
import com.epam.zakharankou.newsmanagement.service.CommentService;
import com.epam.zakharankou.newsmanagement.service.NewsService;
import com.epam.zakharankou.newsmanagement.service.TagService;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;
import com.epam.zakharankou.newsmanagement.service.impl.MainNewsManagementServiceImpl;

import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/configTest.xml"})
public class MainNewsManagementServiceImplTest {
	
	@Mock 
	private NewsService 	newsService;
	@Mock 
	private AuthorService 	authorService;
	@Mock 
	private CommentService	commentService;
	@Mock 
	private TagService		tagService;
	
	@InjectMocks
	private MainNewsManagementServiceImpl mainNewsManagementServiceImpl;	
	
	@Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);       
    }
	
	@Test
	public void addNewsTest() throws ServiceException{
		News news = new News();
		news.setNewsId(1L);
		when(newsService.addNews(news)).thenReturn(1L);			
		assertEquals(news.getNewsId(), mainNewsManagementServiceImpl.addNews(news));
		verify(newsService, times(1)).addNews(news);
	}
	
	@Test
	public void updateNewsTest() throws ServiceException{
		News news = new News();
		news.setNewsId(1L);
		mainNewsManagementServiceImpl.updateNews(news, new Long(1L), new ArrayList<Long>());		
		verify(newsService, times(1)).updateNews(news);
	}
	
	@Test
	public void deleteNewsTest() throws ServiceException{
		Long newsId = 1L;
		mainNewsManagementServiceImpl.deleteNews(newsId);
		verify(newsService, times(1)).deleteNews(newsId);
		verify(authorService, times(1)).deleteLinkAuthorWithNews(newsId);
		verify(commentService, times(1)).deleteCommentsByNewsId(newsId);
		verify(tagService, times(1)).deleteLinksTagWithNews(newsId);		
	}
	
	@Test
	public void viewListOfNewsTest() throws ServiceException{
		List<News> news = new ArrayList<News>();
		Long startPosition = 1L;
		Long endPosition = 10L;
		when(newsService.viewListOfNews(startPosition, endPosition)).thenReturn(news);
		assertEquals(news, mainNewsManagementServiceImpl.viewListOfNews(startPosition, endPosition));
		verify(newsService, times(1)).viewListOfNews(startPosition, endPosition);
	}
	
	@Test
	public void viewSingleNews() throws ServiceException{
		ComplexNews complexNews = new ComplexNews();
		Long newsId = new Long(1L);
		News news = new News();
		Author author = new Author();
		List<Comment> comments = new ArrayList<Comment>();
		List<Tag> tags = new ArrayList<Tag>();
		complexNews.setAuthor(author);
		complexNews.setComments(comments);
		complexNews.setNews(news);
		complexNews.setTags(tags);
		when(newsService.viewSingleNews(newsId)).thenReturn(news);
		when(authorService.getAuthorByNewsId(newsId)).thenReturn(author);
		when(commentService.getCommentsByNewsId(newsId)).thenReturn(comments);
		when(tagService.getTagsByNewsId(newsId)).thenReturn(tags);
		assertEquals(complexNews, mainNewsManagementServiceImpl.viewSingleNews(newsId));
		verify(newsService, times(1)).viewSingleNews(newsId);
		verify(commentService, times(1)).getCommentsByNewsId(newsId);
		verify(tagService, times(1)).getTagsByNewsId(newsId);
		verify(authorService, times(1)).getAuthorByNewsId(newsId);
	}
	
	@Test
	public void addAuthorTest() throws ServiceException{
		Author author = new Author();
		author.setAuthorId(1L);
		when(authorService.addAuthor(author)).thenReturn(author.getAuthorId());
		assertEquals(author.getAuthorId(), mainNewsManagementServiceImpl.addAuthor(author));
		verify(authorService, times(1)).addAuthor(author);
	}
	
	@Test
	public void searchNewsTest() throws ServiceException{
		SearchCriteria searchCriteria = new SearchCriteria();
		Long startPosition = new Long(1L);
		Long endPosition = new Long(10L);
		List<News> news = new ArrayList<News>();
		when(newsService.searchNews(searchCriteria, startPosition, endPosition)).thenReturn(news);
		assertEquals(news, mainNewsManagementServiceImpl.searchNews(searchCriteria, startPosition, endPosition));
		verify(newsService, times(1)).searchNews(searchCriteria, startPosition, endPosition);
	}
	
	@Test
	public void addTagsTest() throws ServiceException{
		Tag tag = new Tag();
		Long tagId = new Long(1L);
		when(tagService.addTag(tag)).thenReturn(tagId);
		assertEquals(tagId, mainNewsManagementServiceImpl.addTag(tag));		
		verify(tagService, times(1)).addTag(tag);
	}
	
	@Test
	public void addCommentTest() throws ServiceException{
		Comment comment = new Comment();
		Long commentId = new Long(1L);
		comment.setCommentId(commentId);
		when(commentService.addComment(comment)).thenReturn(commentId);
		assertEquals(commentId, mainNewsManagementServiceImpl.addComment(comment));
		verify(commentService, times(1)).addComment(comment);
	}
	
	@Test
	public void deleteCommentTest() throws ServiceException{
		Long commentId = new Long(1L);
		mainNewsManagementServiceImpl.deleteComment(commentId);
		verify(commentService, times(1)).deleteComment(commentId);
	}
	
	@Test
	public void addComplesNewsTest() throws ServiceException{
		Long newsId = new Long(1L);
		Long authorId = new Long(1L);
		ComplexNews complexNews = new ComplexNews();
		News news = new News();
		Author author = new Author();
		List<Comment> comments = new ArrayList<Comment>();
		List<Long> tagIds = new ArrayList<Long>();
		news.setNewsId(newsId);
		author.setAuthorId(authorId);
		complexNews.setComments(comments);
		complexNews.setAuthor(author);
		complexNews.setNews(news);
		
		when(newsService.addNews(news)).thenReturn(newsId);
		assertEquals(newsId, mainNewsManagementServiceImpl.addComplexNews(complexNews.getNews(), authorId, tagIds));
		verify(authorService, times(1)).createLinkAuthorWithNews(author.getAuthorId(), newsId);
		verify(tagService, times(1)).createLinksTagWithNews(tagIds, newsId);
	}
	
	@Test
	public void deleteTagTest() throws ServiceException{
		Long tagId = new Long(1L);
		mainNewsManagementServiceImpl.deleteTag(tagId);
		verify(tagService, times(1)).deleteLinksByTagId(tagId);
		verify(tagService, times(1)).deleteTag(tagId);
	}
	
	@Test(expected = ServiceException.class)
	public void transactionalTest() throws ServiceException{
		doThrow(new ServiceException("Error", new Exception())).when(authorService).getAuthorByNewsId(1L);
		mainNewsManagementServiceImpl.viewSingleNews(1L);
				
		verify(newsService, times(0)).viewSingleNews(1L);
		verify(authorService, times(0)).getAuthorByNewsId(1L);
		verify(commentService, times(0)).getCommentsByNewsId(1L);
		verify(tagService, times(0)).getTagsByNewsId(1L);
	}
}
