<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div>
	<c:choose>
		<c:when test="${complexNews.news != null }">
			<div class="divBack">
				<a href="/news-client/back/${complexNews.news.newsId}"><spring:message code="local.back"/></a>
			</div>
			<table class="tableSingleNews">
				<tr class="trSingleNews">
					<td class="textSingleNewsTitle">
						<b><c:out value="${complexNews.news.shortText}"/></b> (<spring:message code="local.byAuthor"/> <c:out value="${complexNews.author.authorName}"/>)
					</td>
					<td class="textDate">
						<c:out value="${complexNews.news.modificationDate}"/>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="textSingleNewsFullText">
						<c:out value="${complexNews.news.fullText}"/>
					</td>
				</tr>
			</table>
			<table class="tableComments">
				<c:forEach var="comment" items="${complexNews.comments}">
					<tr class="trComment">
						<td class="tdComment">
							<p class="textDateComment"><c:out value="${comment.getDate()}"/>
							<p class="textComment"><c:out value="${comment.commentText}"/> 
						</td>
					</tr>
				</c:forEach>
				<sf:form action="/news-client/postComment" commandName="comment">
					<tr>
						<td>
							<sf:textarea path="commentText" rows="3" cols="44"/>
						</td>			
					</tr>
					<tr>
						<td>
							<sf:errors path="commentText" cssClass="error"></sf:errors>
						</td>
					</tr>
					<tr>
						<td align="right">
							<sf:hidden path="newsId" value="${complexNews.news.newsId}"/>
							<input type="submit" value="<spring:message code="local.postComment"/>"/>
						</td>
					</tr>		
				</sf:form>		
			</table>
			<table class="tablePrevNext">
				<tr>
					<td align="left">
						<c:if test="${prevNewsId != 0}">
							<a href="/news-client/showSingleNews/${prevNewsId}"><spring:message code="local.prev"/></a>					
						</c:if>
					</td>
					<td align="right">
						<c:if test="${nextNewsId != 0}">
							<form action="showSingleNews" method="post">
								<a href="/news-client/showSingleNews/${nextNewsId}"><spring:message code="local.next"/></a>
							</form>
						</c:if>
					</td>
				</tr>
			</table>
		</c:when>
		<c:otherwise>
			<table>
				<tr valign="middle">
					<td align="center">
						<h3><spring:message code="local.noSuchNewsId"/> </h3>
						<a href="/news-client"><spring:message code="local.goToHomePage"/></a>
					</td>
				</tr>
			</table>
		</c:otherwise>
	</c:choose>
</div>