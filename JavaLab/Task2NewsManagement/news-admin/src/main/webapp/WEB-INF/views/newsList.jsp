<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="divNewsList">
	<div class="divFilter">
		<table>
			<tr>						
				<sf:form action="/news-admin/news/filter" commandName="searchCriteria">
					<td>
						<div>
							<sf:select path="authorId">
								<sf:option value="0"><spring:message code="local.pleaseSelectTheAuthor"/></sf:option>
								<c:forEach var="author" items="${authors}">
									<c:choose>
										<c:when test="${author.authorId == sessionScope.searchCriteria.authorId}">
											<option value="${author.authorId}" label="${author.authorName}" selected="selected"/>
										</c:when>
										<c:otherwise>
											<option value="${author.authorId}" label="${author.authorName}"/>
										</c:otherwise>
									</c:choose>								
								</c:forEach>
							</sf:select>
						</div>
					</td>
					<td>
						<div class="multiselect">						
							<div class="selectBox" onclick="showCheckboxes()">
								<select>
									<option><spring:message code="local.pleaseSelectTags"/></option>
								</select>							
							</div>
							<div id="checkboxes">
								<c:forEach var="tag" items="${tags}">
									<c:choose>
										<c:when test="${sessionScope.searchCriteria.tagsId.contains(tag.tagId)}">
											<label for="${tag.tagId}">
												<input type="checkbox" name="tagsId" checked="checked" value="${tag.tagId}"/>${tag.tagName}
											</label>
										</c:when>
										<c:otherwise>
											<label for="${tag.tagId}">
												<input type="checkbox" name="tagsId" value="${tag.tagId}"/>${tag.tagName}
											</label>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</div>
						</div>
					</td>
					<td>
						<input type="submit" value='<spring:message code="local.filter"/>'/>
					</td>				
				</sf:form>				
				<td>
					<sf:form action="/news-admin/news/reset">
						<input type="submit" name="reset" value="<spring:message code="local.reset"/>"/>					
					</sf:form>
				</td>
			</tr>
		</table>
	</div>
	<form action="/news-admin/news/delete" method="post">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />		
		<table class="tableNewsList">
			<c:if test="${complexNews.size() == 0}">
				<tr align="center">
					<td>
						<h3><spring:message code="local.noSuchNews"/></h3>			
					</td>
				</tr>
			</c:if>	
			
			<c:forEach var="complexNews" items="${complexNews}">			
				<tr class="trNewsList">
					<td>
						<table class="tableNews">
							<tr class="trNews">
								<td colspan="3">
									<a class="textTitleLikeLink" href="/news-admin/news/showNews/${complexNews.news.newsId}/"><c:out value="${complexNews.news.title}"/> (<spring:message code="local.byAuthor"/> <c:out value="${complexNews.author.authorName}"/>)</a>
								</td>	
								<td rowspan="2" class="textDate">
									<c:out value="${complexNews.news.modificationDate}"/>
								</td>			
							</tr>	
							<tr class="trNews">
								<td colspan="3" class="textShortText">
									<c:out value="${complexNews.news.shortText}"/>								
								</td>
							</tr>
							<tr class="trNews">
								<td class="tdEmpty">
								</td>
								<td class="textTags">
									<c:forEach var="tag" items="${complexNews.tags}">
										<c:out value="${tag.tagName},"/> 
									</c:forEach>								
								</td>
								<td class="textComments">
									<spring:message code="local.comments"/>(<c:out value="${complexNews.comments.size()}"/>)
								</td>
								<td class="textView">
									<a href="/news-admin/news/edit/${complexNews.news.newsId}"><spring:message code="local.edit"/></a>
									<input type="checkbox" name="newsIds" value="${complexNews.news.newsId}"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</c:forEach>
			<c:if test="${complexNews.size() > 0 }">
				<tr align="right">
					<td>					
						<input type="submit" value='<spring:message code="local.delete"/>'/>
					</td>
				</tr>
			</c:if>
		</table>				
	</form>
	<div class="divPagination" align="center">	
		<table class="tablePagination">
			<tr>	
				<c:forEach var="i" begin="1" end="${countPage}">
					<td>					
						<c:choose>
							<c:when test="${i == pageNumber}">
								<a href="/news-admin/news/pages/${i}" class="textCurrentPage">${i}</a>
							</c:when>
							<c:otherwise>
								<a href="/news-admin/news/pages/${i}">${i}</a>
							</c:otherwise>
						</c:choose>																
					</td>
				</c:forEach>						
			</tr>
		</table>
	</div>
</div>