<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<div class="divNewsList">
	<table class="tableAuthors">
		<c:forEach var="tag" items="${tags}">
			<sf:form action="/news-admin/tag/updateTag" commandName="tag" method="post">
				<tr>				
					<td class="trTableAuthors">
						<b><spring:message code="local.tag"/>:</b>
					</td>
					<td class="trTableAuthors">		
						<sf:input path="tagName" id="text${tag.tagId}" value="${tag.tagName}" readonly="true" class="inputTableAuthors"/>
					</td>
					<td class="trTableAuthors">
						<input type="hidden" id="hiddenAuthorId${tag.tagId}" value="${tag.tagName}">
						<p class="linkText" id="${tag.tagId}"  onclick="toggleShow(${tag.tagId})"><spring:message code="local.editAuthor"/></p>
						<div style="display: none; width: 250px;" id="hide${tag.tagId}" >
							<input class="buttonLikeLink" type="submit" value="<spring:message code="local.updateAuthor"/>">
							<sf:hidden path="tagId" value="${tag.tagId}"/>					
							<a class="linkText" href="/news-admin/tag/deleteTag?tagId=${tag.tagId}"><spring:message code="local.deleteTag"/></a> 
							<a class="linkText" onclick="toggleHide(${tag.tagId})" ><spring:message code="local.cancelAuthor"/></a>
						</div>
					</td>				
				</tr>
				<c:if test="${tag.tagId == tagIdError }">
					<tr>
						<td colspan="3">
							<sf:errors path="tagName" cssClass="error"></sf:errors>
						</td>
					</tr>
				</c:if>
			</sf:form>
		</c:forEach>
		<sf:form action="/news-admin/tag/saveTag" commandName="tag" method="post">
			<tr>
				<td class="trTableSave">
					<b><spring:message code="local.addTag"/></b>
				</td>
				<td>
					<sf:input path="tagName" cssClass="inputTableAuthors"/>					
				</td>
				<td>
					<input type="submit" value="<spring:message code="local.save"/>">
				</td>			
			</tr>
			<c:if test="${tagIdError == null }">
				<tr>
					<td colspan="3" class="trTableAuthorsError">
						<sf:errors path="tagName" cssClass="error"></sf:errors>
					</td>
				</tr>
			</c:if>
		</sf:form>
	</table>
</div>