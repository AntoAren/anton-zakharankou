package com.epam.zakharankou.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.dao.ITagDao;
import com.epam.zakharankou.newsmanagement.entity.Tag;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/configTest.xml"})
public class TagServiceImplTest {
	
	@Mock
	private ITagDao tagDao;
	
	@InjectMocks
	private TagServiceImpl tagServiceImpl;
	
	@Before
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void addTagTest() throws DAOException, ServiceException{
		Tag tag = new Tag();
		Long tagId = new Long(1L);
		when(tagDao.create(tag)).thenReturn(tagId);
		assertEquals(tagId, tagServiceImpl.addTag(tag));
		verify(tagDao, times(1)).create(tag);
	}
	
	@Test
	public void createLinksTagWithNewsTest() throws DAOException, ServiceException{
		List<Long> tagIds = new ArrayList<Long>();
		Long newsId = new Long(1L);
		tagServiceImpl.createLinksTagWithNews(tagIds, newsId);
		verify(tagDao, times(1)).createLinks(tagIds, newsId);
	}
	
	@Test
	public void deleteLinksTagWithNewsTest() throws DAOException, ServiceException{
		Long newsId = new Long(1L);
		tagServiceImpl.deleteLinksTagWithNews(newsId);
		verify(tagDao, times(1)).deleteLinksByNewsId(newsId);
	}
	
	@Test
	public void getTagsByNewsIdTest() throws DAOException, ServiceException{
		List<Tag> tags = new ArrayList<Tag>();
		Long newsId = new Long(1L);
		when(tagDao.readTagsByNewsId(newsId)).thenReturn(tags);
		assertEquals(tags, tagServiceImpl.getTagsByNewsId(newsId));
		verify(tagDao, times(1)).readTagsByNewsId(newsId);
	}
	
	@Test
	public void deleteLinksByTagIdTest() throws DAOException, ServiceException{
		Long tagId = new Long(1L);
		tagServiceImpl.deleteLinksByTagId(tagId);
		verify(tagDao, times(1)).deleteLinksByTagId(tagId);
	}
	
	@Test
	public void deleteTagTest() throws DAOException, ServiceException{
		Long tagId = new Long(1L);
		tagServiceImpl.deleteTag(tagId);
		verify(tagDao, times(1)).delete(tagId);
	}
	
}
