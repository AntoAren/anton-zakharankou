package com.epam.newsmanagement.utils;

import java.util.ArrayList;
import java.util.List;

import com.epam.zakharankou.newsmanagement.entity.ComplexNews;
import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.Tag;

public class ConverterUtility {
	private ConverterUtility(){}
	
	public static ComplexNews convertToComplexNews(News news){
		ComplexNews complexNews = new ComplexNews();
		complexNews.setNews(news);
		complexNews.setAuthor(news.getAuthorList().get(0));
		complexNews.setTagIds(convertToTagIds(news.getTagList()));
		return complexNews;
	}
	
	private static List<Long> convertToTagIds(List<Tag> tags){
		List<Long> tagIds = new ArrayList<Long>();
		Long tagId;
		for(Tag tag: tags){
			tagId = new Long(tag.getTagId());
			tagIds.add(tagId);
		}
		return tagIds;
	}
}
