package com.epam.zakharankou.newsmanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Author class provides author entity.
 * @author Anton_Zakharankou
 *
 */

@Entity
@Table(name = "AUTHORS")
@SequenceGenerator(name="AT_AUTHOR_ID_SEQ", sequenceName="AT_AUTHOR_ID_SEQ",allocationSize=1)
public class Author{
	
	
	@Id	
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AT_AUTHOR_ID_SEQ")
	@Column(name = "AT_AUTHOR_ID", nullable = false, precision = 20)
	private Long authorId;
	
	@Column(name = "AT_AUTHOR_NAME", nullable = false, precision = 30)
	@NotNull(message = "{error.author.authorName.notNull}")
	@Size(min = 1, max = 30, message = "{error.author.authorName.size}")
	private String authorName;
	
	@Column(name = "AT_EXPIRED", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date expired;
	
	public Author(){}	
	
	public Long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public Date getExpired() {
		return expired;
	}
	public void setExpired(Date expired) {
		this.expired = expired;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorId == null) ? 0 : authorId.intValue());
		result = prime * result	+ ((authorName == null) ? 0 : authorName.hashCode());
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		return result;
	}
	
	@Override
	public String toString() {
		return "Author [authorId=" + authorId + ", authorName=" + authorName + ", expired=" + expired + "]";
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (authorId.longValue() != other.getAuthorId().longValue())
			return false;
		if (authorName == null) {
			if (other.getAuthorName() != null)
				return false;
		} else if (!authorName.equals(other.getAuthorName()))
			return false;
		if (expired == null) {
			if (other.getExpired() != null)
				return false;
		} else if (!expired.equals(other.getExpired()))
			return false;
		return true;
	}
	
}
