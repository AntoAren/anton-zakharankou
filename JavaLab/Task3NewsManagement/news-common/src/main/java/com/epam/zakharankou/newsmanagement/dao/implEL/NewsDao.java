package com.epam.zakharankou.newsmanagement.dao.implEL;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.epam.zakharankou.newsmanagement.dao.INewsDao;
import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.SearchCriteria;

/**
 * This class is Eclipse Link Implementation of JPA for #{@link INewsDao}. 
 * @author Anton_Zakharankou
 *
 */
public class NewsDao implements INewsDao {
	
	@PersistenceUnit(unitName = "entityManagerFactory")
	private EntityManagerFactory entityManagerFactory;
	
	public Long create(News news) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		try {
			entityManager.persist(news);
			entityTransaction.commit();
		} finally {
			entityManager.close();
		}
		return news.getNewsId();
	}

	public News readById(Long newsId) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		News news = null;
		try{
			news = entityManager.find(News.class, newsId);
			entityTransaction.commit();
		} finally {
			entityManager.close();
		}
		return news;
	}
	
	public boolean update(News news) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();		
		try {			
			entityManager.merge(news);
			entityTransaction.commit();
		} catch (OptimisticLockException exception){
			entityTransaction.rollback();
			return false;
		} finally {	
			entityManager.close();
		}
		return true;
	}

	public void delete(Long newsId) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		try {
			News news = entityManager.find(News.class, newsId);		
			entityManager.remove(news);		
			entityTransaction.commit();
		} finally {
			entityManager.close();
		}
	}

	public List<News> read(Long startPosition, Long endPosition) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		List<News> newsList = null;
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<News> criteriaQuery = criteriaBuilder.createQuery(News.class);	
			Root<News> news = criteriaQuery.from(News.class);
			criteriaQuery.orderBy(criteriaBuilder.desc(news.get("modificationDate")));
			TypedQuery<News> typedQuery = entityManager.createQuery(criteriaQuery);
			typedQuery.setFirstResult(startPosition.intValue() - 1);
			typedQuery.setMaxResults(endPosition.intValue() - startPosition.intValue() + 1);
			newsList = typedQuery.getResultList();
		} finally {
			entityManager.close();
		}
		return newsList;
	}

	public List<News> readAll() throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		List<News> newsList = null;
		try {	
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<News> criteriaQuery = criteriaBuilder.createQuery(News.class);	
			Root<News> news = criteriaQuery.from(News.class);
			criteriaQuery.orderBy(criteriaBuilder.desc(news.get("modificationDate")));
			TypedQuery<News> typedQuery = entityManager.createQuery(criteriaQuery);		
			newsList = typedQuery.getResultList();
		} finally {
			entityManager.close();
		}
		return newsList;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<News> searchNews(SearchCriteria searchCriteria,	Long startPosition, Long endPosition) throws DAOException {
		List<News> newsList = null;
		if ((searchCriteria == null) || ((searchCriteria.getAuthorId() == null) && (searchCriteria.getTagsId() == null || searchCriteria.getTagsId().size() == 0))){
			newsList = read(startPosition, endPosition);
		} else {		
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			try {
				CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
				CriteriaQuery<News> criteriaQuery = criteriaBuilder.createQuery(News.class);
				Predicate predicate = criteriaBuilder.conjunction();
				Root<News> news = criteriaQuery.from(News.class);
				criteriaQuery.orderBy(criteriaBuilder.desc(news.get("modificationDate")));
				criteriaQuery.distinct(true);
				if (searchCriteria.getAuthorId() != null){
					Join join = news.join("authorList", JoinType.LEFT);
					predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(join.get("authorId"), searchCriteria.getAuthorId()));			
				}
				if ((searchCriteria.getTagsId() != null) && (searchCriteria.getTagsId().size() != 0)){
					Join join = news.join("tagList", JoinType.LEFT);
					predicate = criteriaBuilder.and(predicate, join.get("tagId").in(searchCriteria.getTagsId()));
				}
				criteriaQuery.where(predicate);
				TypedQuery<News> typedQuery = entityManager.createQuery(criteriaQuery);
				typedQuery.setFirstResult(startPosition.intValue() - 1);
				typedQuery.setMaxResults(endPosition.intValue() - startPosition.intValue() + 1);
				newsList = typedQuery.getResultList();
			} finally {
				entityManager.close();
			}
		}
		return newsList;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Long countAllNews(SearchCriteria searchCriteria) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Long count = null;
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();	
			Root news = criteriaQuery.from(News.class);
			Predicate predicate = criteriaBuilder.conjunction();		
			if (searchCriteria.getAuthorId() != null){
				Join join = news.join("authorList", JoinType.LEFT);
				predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(join.get("authorId"), searchCriteria.getAuthorId()));			
			}
			if ((searchCriteria.getTagsId() != null) && (searchCriteria.getTagsId().size() != 0)){
				Join join = news.join("tagList", JoinType.LEFT);
				predicate = criteriaBuilder.and(predicate, criteriaBuilder.and(join.get("tagId").in(searchCriteria.getTagsId())));
			}
			criteriaQuery.where(predicate);
			criteriaQuery.select(criteriaBuilder.countDistinct(news.get("newsId")));			
			Query query = entityManager.createQuery(criteriaQuery);
			count = (Long)query.getSingleResult();
		} finally {
			entityManager.close();
		}		
		return count;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Long getNextNewsId(SearchCriteria searchCriteria, Long newsId) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Long findNewsId = null;
		try {
			CriteriaQuery criteriaQuery = buildCriteriaQuery(searchCriteria, newsId, entityManager);
			Query query = entityManager.createQuery(criteriaQuery);
			Long count = (Long)query.getSingleResult();			
			List<News> newsList = searchNews(searchCriteria, count + 2, count + 2);			
			if ((newsList != null) && (newsList.size() != 0)){
				News itemNews = newsList.get(0);
				if (itemNews != null){
					findNewsId = itemNews.getNewsId();
				}
			}
		} finally {
			entityManager.close();
		}
		return findNewsId;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Long getPrevNewsId(SearchCriteria searchCriteria, Long newsId) throws DAOException {		
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Long findNewsId = null;
		try {		
			CriteriaQuery criteriaQuery = buildCriteriaQuery(searchCriteria, newsId, entityManager);
			Query query = entityManager.createQuery(criteriaQuery);
			Long count = (Long)query.getSingleResult();				
			if (count > 0){
				List<News> newsList = searchNews(searchCriteria, count, count);
				News itemNews = null;
				if ((newsList != null) && (newsList.size() != 0)){
					itemNews = newsList.get(0);
				}
				if (itemNews != null){
					findNewsId = itemNews.getNewsId();
				}
			}
		} finally {
			entityManager.close();
		}
		return findNewsId;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Long getCurrentPageForNews(SearchCriteria searchCriteria, Long newsId) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Long count = null;
		try {
			CriteriaQuery criteriaQuery = buildCriteriaQuery(searchCriteria, newsId, entityManager);			
			Query query = entityManager.createQuery(criteriaQuery);
			count = (Long)query.getSingleResult();		
		} finally {
			entityManager.close();
		}
		return count + 1L;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CriteriaQuery buildCriteriaQuery(SearchCriteria searchCriteria, Long newsId, EntityManager entityManager) throws DAOException{
		News currentNews = readById(newsId);		
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
		Predicate predicate = criteriaBuilder.conjunction();
		Root news = criteriaQuery.from(News.class);
		criteriaQuery.orderBy(criteriaBuilder.desc(news.get("modificationDate")));
		if (searchCriteria.getAuthorId() != null){
			Join join = news.join("authorList", JoinType.LEFT);
			predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(join.get("authorId"), searchCriteria.getAuthorId()));			
		}
		if ((searchCriteria.getTagsId() != null) && (searchCriteria.getTagsId().size() != 0)){
			Join join = news.join("tagList", JoinType.LEFT);
			predicate = criteriaBuilder.and(predicate, join.get("tagId").in(searchCriteria.getTagsId()));			
		}		
		predicate = criteriaBuilder.and(predicate, criteriaBuilder.greaterThan(news.get("modificationDate"), currentNews.getModificationDate()));
		criteriaQuery.where(predicate);
		criteriaQuery.select(criteriaBuilder.countDistinct(news.get("newsId")));
		
		return criteriaQuery;
	}
}
