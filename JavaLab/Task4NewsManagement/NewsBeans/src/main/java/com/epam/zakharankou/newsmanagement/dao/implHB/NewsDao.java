package com.epam.zakharankou.newsmanagement.dao.implHB;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleObjectStateException;
import org.hibernate.Transaction;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import com.epam.zakharankou.newsmanagement.dao.INewsDao;
import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.SearchCriteria;

public class NewsDao implements INewsDao {
	
	private SessionFactory sessionFactory;
	
	public Long create(News news) throws DAOException {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.save(news);
			transaction.commit();
		} finally {
			session.close();
		}
		return news.getNewsId();
	}

	public News readById(Long newsId) throws DAOException {
		Session session = this.sessionFactory.openSession();
		News news = null;
		try {		
			news = (News)session.createCriteria(News.class).add(Restrictions.eq("newsId", newsId)).uniqueResult();
		} finally {
			session.close();
		}
		return news;
	}

	public boolean update(News news) throws DAOException {
		boolean flag = true;
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			session.update(news);
			transaction.commit();
		} catch (StaleObjectStateException exception){
			flag = false;
			transaction.rollback();
		} finally {		
			session.close();
		}
		return flag;
	}

	public void delete(Long newsId) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			News news = (News)session.createCriteria(News.class).add(Restrictions.eq("newsId", newsId)).uniqueResult();		
			session.delete(news);
			transaction.commit();
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<News> read(Long startPosition, Long endPosition) throws DAOException {
		Session session = this.sessionFactory.openSession();
		List<News> newsList = null;
		try {
			 newsList = session.createCriteria(News.class).addOrder(Order.desc("modificationDate")).setFirstResult(startPosition.intValue() - 1).setMaxResults(3).list();
		} finally {
			session.close();
		}
		return newsList;
	}

	@SuppressWarnings("unchecked")
	public List<News> readAll() throws DAOException {
		Session session = this.sessionFactory.openSession();
		List<News> newsList = null;
		try {
			newsList = session.createCriteria(News.class).addOrder(Order.desc("modificationDate")).list();
		} finally {
			session.close();
		} 
		return newsList;
	}

	@SuppressWarnings("unchecked")
	public List<News> searchNews(SearchCriteria searchCriteria,	Long startPosition, Long endPosition) throws DAOException {
		Session session = this.sessionFactory.openSession();
		List<News> newsList = null;
		try {
			DetachedCriteria idsOnlyCriteria = DetachedCriteria.forClass(News.class);			
			Conjunction conjunction = Restrictions.conjunction();			
			if (searchCriteria != null){
				if (searchCriteria.getAuthorId() != null){
					idsOnlyCriteria.createAlias("authorList", "author");
					conjunction.add(Restrictions.eq("author.authorId", searchCriteria.getAuthorId()));
				}
				if ((searchCriteria.getTagsId() != null) && (searchCriteria.getTagsId().size() > 0)){
					idsOnlyCriteria.createAlias("tagList", "tag");
					conjunction.add(Restrictions.in("tag.tagId", searchCriteria.getTagsId()));
				}
			}			
			idsOnlyCriteria.add(conjunction);			
			idsOnlyCriteria.setProjection(Projections.distinct(Projections.id()));				
			Criteria criteria = session.createCriteria(News.class);
			criteria.add(Subqueries.propertyIn("newsId", idsOnlyCriteria));
			criteria.addOrder(Property.forName("modificationDate").desc());
			criteria.setFirstResult(startPosition.intValue() - 1).setMaxResults(endPosition.intValue() - startPosition.intValue() + 1);
			newsList = criteria.list();
		} finally {			
			session.close();
		}
		return newsList;
	}

	public Long countAllNews(SearchCriteria searchCriteria) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Long count = null;
		try {
			DetachedCriteria idsOnlyCriteria = DetachedCriteria.forClass(News.class);
			Conjunction conjunction = Restrictions.conjunction();			
			if (searchCriteria != null){
				if (searchCriteria.getAuthorId() != null){
					idsOnlyCriteria.createAlias("authorList", "author");
					conjunction.add(Restrictions.eq("author.authorId", searchCriteria.getAuthorId()));
				}
				if ((searchCriteria.getTagsId() != null) && (searchCriteria.getTagsId().size() > 0)){
					idsOnlyCriteria.createAlias("tagList", "tag");
					conjunction.add(Restrictions.in("tag.tagId", searchCriteria.getTagsId()));
				}
			}		
			idsOnlyCriteria.add(conjunction);			
			idsOnlyCriteria.setProjection(Projections.distinct(Projections.id()));				
			Criteria criteria = session.createCriteria(News.class);
			criteria.add(Subqueries.propertyIn("newsId", idsOnlyCriteria));
			count = Long.parseLong(criteria.setProjection(Projections.rowCount()).uniqueResult().toString());
		} finally {
			session.close();
		}
		return count;
	}

	public Long getNextNewsId(SearchCriteria searchCriteria, Long newsId) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Long findNewsId = null;
		try {
			Criteria criteria = buildCriteria(searchCriteria, newsId, session);	
			Long count = Long.parseLong(criteria.setProjection(Projections.rowCount()).uniqueResult().toString());
			List<News> newsList = searchNews(searchCriteria, count + 2, count + 2);
			if ((newsList != null) && (newsList.size() != 0)){
				News itemNews = newsList.get(0);
				if (itemNews != null){
					findNewsId = itemNews.getNewsId();
				}
			}
		} finally {
			session.close();
		}
		return findNewsId;
	}

	public Long getPrevNewsId(SearchCriteria searchCriteria, Long newsId) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Long findNewsId = null;
		try {
			Criteria criteria = buildCriteria(searchCriteria, newsId, session);	
			Long count = Long.parseLong(criteria.setProjection(Projections.rowCount()).uniqueResult().toString());							
			if (count > 0){
				List<News> newsList = searchNews(searchCriteria, count, count);
				News itemNews = null;
				if ((newsList != null) && (newsList.size() != 0)){
					itemNews = newsList.get(0);
				}
				if (itemNews != null){
					findNewsId = itemNews.getNewsId();
				}
			}
		} finally {
			session.close();
		}
		return findNewsId;
	}

	public Long getCurrentPageForNews(SearchCriteria searchCriteria, Long newsId) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Long count = null;
		try {		
			Criteria criteria = buildCriteria(searchCriteria, newsId, session);			
			count = Long.parseLong(criteria.setProjection(Projections.rowCount()).uniqueResult().toString());	
		} finally {
			session.close();
		}		
		return count + 1L;
	}
	
	private Criteria buildCriteria(SearchCriteria searchCriteria, Long newsId, Session session) throws DAOException{
		DetachedCriteria idsOnlyCriteria = DetachedCriteria.forClass(News.class);
		Conjunction conjunction = Restrictions.conjunction();		
		News news = readById(newsId);		
		if (searchCriteria != null){
			if (searchCriteria.getAuthorId() != null){
				idsOnlyCriteria.createAlias("authorList", "author");
				conjunction.add(Restrictions.eq("author.authorId", searchCriteria.getAuthorId()));
			}
			if ((searchCriteria.getTagsId() != null) && (searchCriteria.getTagsId().size() > 0)){
				idsOnlyCriteria.createAlias("tagList", "tag");
				conjunction.add(Restrictions.in("tag.tagId", searchCriteria.getTagsId()));
			}
			conjunction.add(Restrictions.gt("modificationDate", news.getModificationDate()));
		}				
		idsOnlyCriteria.add(conjunction);		
		idsOnlyCriteria.setProjection(Projections.distinct(Projections.id()));			
		Criteria criteria = session.createCriteria(News.class);
		criteria.addOrder(Order.desc("modificationDate"));
		criteria.add(Subqueries.propertyIn("newsId", idsOnlyCriteria));			
		return criteria;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
