package com.epam.zakharankou.newsmanagement.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Tag class provides entity tag.
 * @author Anton_Zakharankou
 *
 */
public class Tag{
	private Long tagId;
	
	@NotNull(message = "{error.tag.tagName.notNull}")
	@Size(min = 1, max = 30, message = "{error.tag.tagName.size}")
	private String tagName;
	
	public Long getTagId() {
		return tagId;
	}
	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tagId == null) ? 0 : tagId.intValue());
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (tagId.longValue() != other.getTagId().longValue())
			return false;
		if (tagName == null) {
			if (other.getTagName() != null)
				return false;
		} else if (!tagName.equals(other.getTagName()))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Tag [tagId=" + tagId + ", tagName=" + tagName + "]";
	}
	
}
