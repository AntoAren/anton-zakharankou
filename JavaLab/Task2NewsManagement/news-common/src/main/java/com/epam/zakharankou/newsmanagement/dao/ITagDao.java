package com.epam.zakharankou.newsmanagement.dao;

import java.util.List;

import com.epam.zakharankou.newsmanagement.dao.GenericDao;
import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.Tag;

/**
 * ITagDao is interface which works with {@link com.epam.zakharankou.newsmanagement.entity.Tag} entity.
 * @author Anton_Zakharankou
 *
 */
public interface ITagDao extends GenericDao<Tag> {	
	
	/**
	 * Reads a certain number of records {@link com.epam.zakharankou.newsmanagement.entity.Tag} entities from database.
	 * @return list of {@link com.epam.zakharankou.newsmanagement.entity.Tag} entities.
	 * @throws DAOException if SQLException was thrown.
	 */
	public List<Tag> read(Long startPosition, Long endPosition) throws DAOException;
	
	/**
	 * Reads all {@link com.epam.zakharankou.newsmanagement.entity.Tag} entities from database.
	 * @return list of {@link com.epam.zakharankou.newsmanagement.entity.Tag} entities.
	 * @throws DAOException if SQLException was thrown.
	 */
	public List<Tag> readAll() throws DAOException;
	
	/**
	 * Writes record in NEWS_TAGS table in database. Creates link between {@link com.epam.zakharankou.newsmanagement.entity.Tag} entity and 
	 * {@link com.epam.zakharankou.newsmanagement.entity.News} entity.
	 * @throws DAOException if SQLException was thrown.
	 */
	public void createLink(Long newsId, Long tagId) throws DAOException;
	
	/**
	 * Writes records in NEWS_TAGS table in database. Creates links between {@link com.epam.zakharankou.newsmanagement.entity.Tag} entities and 
	 * {@link com.epam.zakharankou.newsmanagement.entity.News} entity.
	 * @param tags - list of tags.
	 * @throws DAOException if SQLException was thrown. 
	 */
	public void createLinks(List<Long> tagIds, Long newsId) throws DAOException;
	
	/**
	 * Deletes record from NEWS_TAGS table from database. Deletes link between {@link com.epam.zakharankou.newsmanagement.entity.Tag} entity and 
	 * {@link com.epam.zakharankou.newsmanagement.entity.News} entity.
	 * @throws DAOException if SQLException was thrown.
	 */
	public void deleteLink(Long newsId, Long tagId) throws DAOException;
	
	/**
	 * Deletes records from NEWS_TAGS table from database. Deletes links between {@link com.epam.zakharankou.newsmanagement.entity.Tag} entities and 
	 * {@link com.epam.zakharankou.newsmanagement.entity.News} entity by newsId.
	 * @throws DAOException if SQLException was thrown.
	 */
	public void deleteLinksByNewsId(Long newsId) throws DAOException;
	
	/**
	 * Reads tags entities by newsId.
	 * @return list of tags.
	 * @throws DAOException
	 */
	public List<Tag> readTagsByNewsId(Long newsId) throws DAOException;
	
	/**
	 * Deletes records from NEWS_TAGS table from database. Deletes links between {@link com.epam.zakharankou.newsmanagement.entity.Tag} entities and 
	 * {@link com.epam.zakharankou.newsmanagement.entity.News} entity by tagId.
	 * @throws DAOException if SQLException was thrown.
	 */
	public void deleteLinksByTagId(Long tagId) throws DAOException;
}
