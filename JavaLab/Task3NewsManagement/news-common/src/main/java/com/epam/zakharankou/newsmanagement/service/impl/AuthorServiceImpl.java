package com.epam.zakharankou.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.dao.IAuthorDao;
import com.epam.zakharankou.newsmanagement.entity.Author;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;
import com.epam.zakharankou.newsmanagement.service.AuthorService;

/**
 * AuthorServiceImpl class implements {@link com.epam.zakharankou.newsmanagement.service.AuthorService} interface.
 * @author Anton_Zakharankou
 *
 */
public class AuthorServiceImpl implements AuthorService{

	private static final Logger logger = Logger.getLogger("RollingFileAppender");
			
	private IAuthorDao authorDao;
	
	/**
	 * @see AuthorService#addAuthor(Author)
	 */
	public Long addAuthor(Author author) throws ServiceException {
		Long authorId = null;		
		try{
			authorId = authorDao.create(author);			
		} catch (DAOException exception){
			logger.error("Error adding author entity.", exception);
			throw new ServiceException("Error adding author entity.", exception);
		}
		return authorId;
	}	

	/**
	 * @see AuthorService#setExpiredAuthor(Long)
	 */
	public void setExpiredAuthor(Long authorId) throws ServiceException {
		try{
			authorDao.delete(authorId);
		} catch (DAOException exception){
			logger.error("Error setting expired author status.", exception);
			throw new ServiceException("Error setting expired author status.", exception);
		}
	}	
	
	/**
	 * @see AuthorService#readAllAuthors()
	 */
	public List<Author> readAllAuthors() throws ServiceException {
		List<Author> listAuthors = null;
		try{
			listAuthors = authorDao.readAll();
		} catch (DAOException exception){
			logger.error("Error reading all author entities.", exception);
			throw new ServiceException("Error reading all author entities.", exception);
		}
		return listAuthors;
	}
	
	/**
	 * @see AuthorService#updateAuthor(Author)
	 */
	public void updateAuthor(Author author) throws ServiceException{
		try{
			authorDao.update(author);
		} catch (DAOException exception){
			logger.error("Error updating author entity.", exception);
			throw new ServiceException("Error updating author entity.", exception);
		}
	}
	
	public List<Author> readAllNotExpiredAuthors() throws ServiceException{
		List<Author> listAuthors = null;
		try{
			listAuthors = authorDao.readAllNotExpired();
		} catch (DAOException exception){
			logger.error("Error reading not expired author entities.", exception);
			throw new ServiceException("Error reading not expired author entities.", exception);			
		}
		return listAuthors;
	}
	
	public void setAuthorDao(IAuthorDao authorDao) {
		this.authorDao = authorDao;
	}	
}
