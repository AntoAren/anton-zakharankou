package com.epam.zakharankou.newsmanagement.dao.implJDBC;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import com.epam.zakharankou.newsmanagement.dao.implJDBC.AuthorDao;
import com.epam.zakharankou.newsmanagement.entity.Author;

import static org.junit.Assert.*;


@SpringApplicationContext({"configTest.xml"})
@DataSet(value = {"dataSetTest.xml"} ,loadStrategy = CleanInsertLoadStrategy.class)
public class AuthorDaoTest extends UnitilsJUnit4{
	
	@SpringBean("authorDaoImpl")
	private AuthorDao authorDao;
	
	
	@Test
	public void createTest() throws Exception{
		Author expectedAuthor = new Author();		
		expectedAuthor.setAuthorName("Anton");
		
		Long authorId = authorDao.create(expectedAuthor);
		Author actualAuthor = authorDao.readById(authorId);
		
		assertEquals(expectedAuthor.getAuthorName(), actualAuthor.getAuthorName());
	}
	
	@Test
	public void updateTest() throws Exception{
		
		Author expectedAuthor = new Author();
		expectedAuthor.setAuthorName("Anton");
		expectedAuthor.setAuthorId(new Long(2L));
		
		authorDao.update(expectedAuthor);
		Author actualAuthor = authorDao.readById(expectedAuthor.getAuthorId());
		
		assertEquals(expectedAuthor.getAuthorName(), actualAuthor.getAuthorName());
	}
	
	/*@Test
	public void deleteTest() throws Exception{
		Long authorId = new Long(2L);
		
		authorDao.delete(authorId);		
		Author actualAuthor = authorDao.readById(authorId);
		
		assertNotNull(actualAuthor.getExpired());
	}*/
	
	@Test
	public void readAllTest() throws Exception{
		List<Author> expectedListAuthors = new ArrayList<Author>();
		Author firstAuthor = new Author();
		Author secondAuthor = new Author();
		Author thirdAuthor = new Author();
		Author fourthAuthor = new Author();
		firstAuthor.setAuthorId(new Long(1L));
		firstAuthor.setAuthorName("Dmitriy");
		secondAuthor.setAuthorId(new Long(2L));
		secondAuthor.setAuthorName("Ivan");
		thirdAuthor.setAuthorId(new Long(3L));
		thirdAuthor.setAuthorName("Oleg");
		fourthAuthor.setAuthorId(new Long(4L));
		fourthAuthor.setAuthorName("Roma");
		expectedListAuthors.add(firstAuthor);
		expectedListAuthors.add(secondAuthor);
		expectedListAuthors.add(thirdAuthor);
		expectedListAuthors.add(fourthAuthor);
		
		List<Author> actualListAuthors = authorDao.readAll();
		
		assertEquals(expectedListAuthors, actualListAuthors);		
	}
	
	@Test
	public void readTest() throws Exception{
		Long startPosition = new Long(2L);
		Long endPosition = new Long(3L);
		Author secondAuthor = new Author();
		Author thirdAuthor = new Author();
		secondAuthor.setAuthorId(new Long(2L));
		secondAuthor.setAuthorName("Ivan");
		thirdAuthor.setAuthorId(new Long(3L));
		thirdAuthor.setAuthorName("Oleg");
		List<Author> expectedListAuthors = new ArrayList<Author>();
		expectedListAuthors.add(secondAuthor);
		expectedListAuthors.add(thirdAuthor);
		
		List<Author> actualListAuthors = authorDao.read(startPosition, endPosition);
		
		assertEquals(expectedListAuthors, actualListAuthors);
	}
	
	@Test
	public void readByIdTest() throws Exception{
		Long authorId = new Long(2L);
		Author expectedAuthor = new Author();
		expectedAuthor.setAuthorId(2L);
		expectedAuthor.setAuthorName("Ivan");
		
		Author actualAuthor = authorDao.readById(authorId);
		
		assertEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void createLinkTest() throws Exception{
		Long authorId = new Long(2L);
		Long newsId = new Long(3L);
		Author expectedAuthor = new Author();
		expectedAuthor.setAuthorId(2L);
		expectedAuthor.setAuthorName("Ivan");
		
		authorDao.createLink(authorId, newsId);
		Author actualAuthor = authorDao.readAuthorByNewsId(newsId);
		
		assertEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void deleteLinkTest() throws Exception{
		Long newsId = new Long(2L);
		
		authorDao.deleteLink(newsId);
		Author actualAuthor = authorDao.readAuthorByNewsId(newsId);
		
		assertNull(actualAuthor);
	}
	
	@Test
	public void readAuthorByNewsIdTest() throws Exception{
		Long newsId = new Long(4L);
		Author expectedAuthor = new Author();
		expectedAuthor.setAuthorId(1L);
		expectedAuthor.setAuthorName("Dmitriy");
		
		Author actualAuthor = authorDao.readAuthorByNewsId(newsId);
		
		assertEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void readAllNotExpiredTest() throws Exception{
		List<Author> expectedListAuthors = new ArrayList<Author>();
		Author firstAuthor = new Author();
		Author secondAuthor = new Author();
		Author thirdAuthor = new Author();
		firstAuthor.setAuthorId(new Long(1L));
		firstAuthor.setAuthorName("Dmitriy");
		secondAuthor.setAuthorId(new Long(2L));
		secondAuthor.setAuthorName("Ivan");
		thirdAuthor.setAuthorId(new Long(3L));
		thirdAuthor.setAuthorName("Oleg");		
		expectedListAuthors.add(firstAuthor);
		expectedListAuthors.add(secondAuthor);
		expectedListAuthors.add(thirdAuthor);
		
		authorDao.delete(4L);
		List<Author> actualListAuthors = authorDao.readAllNotExpired();
		
		assertEquals(expectedListAuthors, actualListAuthors);
	}
}
