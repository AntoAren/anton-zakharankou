package com.epam.zakharankou.newsmanagement.service;

import com.epam.zakharankou.newsmanagement.entity.Author;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

/**
 * AuthorService class is service which work with {@link com.epam.zakharankou.newsmanagement.entity.Author} action.
 * @author Anton_Zakharankou
 *
 */
public interface AuthorService {
	
	/**
	 * @see MainNewsManagementService#addAuthor(Author)
	 * @throws ServiceException if DAOException was thrown.
	 */
	public Long addAuthor(Author author) throws ServiceException;
	
	/**
	 * Creates link between {@link com.epam.zakharankou.newsmanagement.entity.Author} entity and 
	 * {@link com.epam.zakharankou.newsmanagement.entity.News} entity. Writes in NEWS_AUTHORS table in database.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public void createLinkAuthorWithNews(Author author, Long newsId) throws ServiceException;
	
	/**
	 * Deletes link between {@link com.epam.zakharankou.newsmanagement.entity.Author} entity and 
	 * {@link com.epam.zakharankou.newsmanagement.entity.News} entity. Deletes from NEWS_AUTHORS table in database.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public void deleteLinkAuthorWithNews(Long newsId) throws ServiceException;
	
	/**
	 * Sets expired properties {@link com.epam.zakharankou.newsmanagement.entity.Author} entity.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public void setExpiredAuthor(Long authorId) throws ServiceException;
	
	/**
	 * Reads {@link com.epam.zakharankou.newsmanagement.entity.Author} entity by newsId
	 * @return {@link com.epam.zakharankou.newsmanagement.entity.Author} entity.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public Author getAuthorByNewsId(Long newsId) throws ServiceException;
}
