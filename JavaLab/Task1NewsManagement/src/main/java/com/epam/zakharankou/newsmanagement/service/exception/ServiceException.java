package com.epam.zakharankou.newsmanagement.service.exception;

/**
 * The class exception. It throws Service layer.
 * @author Anton_Zakharankou
 *
 */
public class ServiceException extends Exception{
	private static final long serialVersionUID = 1L;
	
	public ServiceException(String message, Exception exception){
		super(message, exception);		
	}
}
