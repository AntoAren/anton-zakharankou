package com.epam.zakharankou.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import com.epam.zakharankou.newsmanagement.entity.Comment;

import static org.junit.Assert.*;

@SpringApplicationContext({"configTest.xml"})
@DataSet(value = "DataSetTest.xml", loadStrategy = CleanInsertLoadStrategy.class)
public class CommentDaoTest extends UnitilsJUnit4{
	
	@SpringBean("commentDao")
	private CommentDao commentDao;
		
	@Test
	public void createTest() throws Exception{
		Comment expectedComment = new Comment();
		expectedComment.setNewsId(new Long(3L));
		expectedComment.setCommentText("Comment333");
		
		Long actualCommentId = commentDao.create(expectedComment);
		Comment actualComment = commentDao.readById(actualCommentId);
		
		assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
		assertEquals(expectedComment.getNewsId(), actualComment.getNewsId());
	}
	
	@Test
	public void updateTest() throws Exception{
		Comment expectedComment = new Comment();
		expectedComment.setCommentId(new Long(3L));
		expectedComment.setNewsId(new Long(2L));
		expectedComment.setCommentText("Comment333");
		
		commentDao.update(expectedComment);
		Comment actualComment = commentDao.readById(expectedComment.getCommentId());
		
		assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
		assertEquals(expectedComment.getNewsId(), actualComment.getNewsId());
	}
	
	@Test
	public void deleteTest() throws Exception{
		Long commentId = new Long(6L);
		
		commentDao.delete(commentId);
		Comment actualComment = commentDao.readById(commentId);
		
		assertNull(actualComment);
	}
	
	@Test
	public void readByIdTest() throws Exception{
		Long commentId = new Long(8L);
		Comment expectedComment = new Comment();
		expectedComment.setCommentText("Comment8");
		expectedComment.setNewsId(new Long(4L));
		
		Comment actualComment = commentDao.readById(commentId);
		
		assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
		assertEquals(expectedComment.getNewsId(), actualComment.getNewsId());
	}
	
	@Test
	public void deleteCommentsByNewsIdTest() throws Exception{
		Long newsId = new Long(1L);
		List<Comment> expectedCommentList = new ArrayList<Comment>();
		
		commentDao.deleteCommentsByNewsId(newsId);
		List<Comment> actualCommentList = commentDao.readCommentsByNewsId(newsId);
		
		assertEquals(expectedCommentList, actualCommentList);
	}
	
	@Test
	public void readCommentsByNewsIdTest() throws Exception{
		Long newsId = new Long(2L);
		Comment firstComment = new Comment();
		Comment secondComment = new Comment();
		firstComment.setCommentId(new Long(4L));
		firstComment.setCommentText("Comment4");
		secondComment.setCommentId(new Long(5L));
		secondComment.setCommentText("Comment5");
		
		List<Comment> actualCommentList = commentDao.readCommentsByNewsId(newsId);
		
		assertEquals(2, actualCommentList.size());
		assertEquals(firstComment.getCommentId(), actualCommentList.get(0).getCommentId());
		assertEquals(secondComment.getCommentId(), actualCommentList.get(1).getCommentId());
		assertEquals(firstComment.getCommentText(), actualCommentList.get(0).getCommentText());
		assertEquals(secondComment.getCommentText(), actualCommentList.get(1).getCommentText());
	}
}
