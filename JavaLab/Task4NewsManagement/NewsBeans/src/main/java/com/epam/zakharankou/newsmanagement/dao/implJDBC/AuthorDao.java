package com.epam.zakharankou.newsmanagement.dao.implJDBC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.dao.GenericDao;
import com.epam.zakharankou.newsmanagement.dao.IAuthorDao;
import com.epam.zakharankou.newsmanagement.dao.implJDBC.util.CloseUtility;
import com.epam.zakharankou.newsmanagement.entity.Author;

/**
 * AuthorDao class implements {@link com.epam.zakharankou.newsmanagement.dao.IAuthorDao}.
 * @author Anton_Zakharankou
 *
 */

public class AuthorDao implements IAuthorDao {

	private DataSource dataSource;
	
	private static final String AUTHOR_CREATE = "INSERT INTO AUTHORS (AUTHOR_ID, AUTHOR_NAME) VALUES (AT_AUTHOR_ID_SEQ.nextval, ? ) ";
	private static final String AUTHOR_READ = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM (SELECT AUTHORS.AUTHOR_ID, AUTHORS.AUTHOR_NAME, AUTHORS.EXPIRED, ROW_NUMBER() over (ORDER BY AUTHORS.AUTHOR_ID) R FROM AUTHORS) WHERE R BETWEEN ? and ? ";
	private static final String AUTHOR_READ_ALL = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHORS";
	private static final String AUTHOR_READ_BY_ID = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHORS WHERE (AUTHOR_ID = ?)";
	private static final String AUTHOR_READ_BY_NEWS_ID = "SELECT AUTHORS.AUTHOR_ID, AUTHORS.AUTHOR_NAME, AUTHORS.EXPIRED FROM AUTHORS INNER JOIN NEWS_AUTHORS ON (NEWS_AUTHORS.AUTHOR_ID = AUTHORS.AUTHOR_ID) WHERE (NEWS_AUTHORS.NEWS_ID = ? )";
	private static final String AUTHOR_UPDATE = "UPDATE AUTHORS SET AUTHOR_NAME = ? WHERE (AUTHOR_ID = ?)";
	private static final String AUTHOR_DELETE = "UPDATE AUTHORS SET EXPIRED = sysdate WHERE (AUTHOR_ID = ?)";
	private static final String AUTHOR_CREATE_LINK = "INSERT INTO NEWS_AUTHORS (NEWS_AUTHORS.NEWS_ID, NEWS_AUTHORS.AUTHOR_ID) VALUES (?, ?)";
	private static final String AUTHOR_DELETE_LINK = "DELETE FROM NEWS_AUTHORS WHERE (NEWS_ID = ?)";
	
	private static final String AT_AUTHOR_ID = "AUTHOR_ID";
	private static final String AT_AUTHOR_NAME = "AUTHOR_NAME";
	private static final String AT_EXPIRED = "EXPIRED";
	
	/**
	 * @see GenericDao#create(Object)
	 */
	public Long create(Author author) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Long authorId = null;
		String[] column = {"AUTHOR_ID"};
		try{			
			connection = DataSourceUtils.getConnection(dataSource);			
			preparedStatement = connection.prepareStatement(AUTHOR_CREATE, column);
			preparedStatement.setString(1, author.getAuthorName());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if ((resultSet != null) && (resultSet.next())){
				authorId = resultSet.getLong(1);
			}
		} catch (SQLException exception){
			throw new DAOException("Error creating author entity.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return authorId;
	}
	
	/**
	 * @see GenericDao#update(Object)
	 */
	public boolean update(Author author) throws DAOException{
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(AUTHOR_UPDATE);
			preparedStatement.setString(1, author.getAuthorName());
			preparedStatement.setLong(2, author.getAuthorId());
			preparedStatement.executeUpdate();			
		} catch (SQLException exception){
			throw new DAOException("Error updating author entity.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, dataSource);
		}
		return true;
	}

	/**
	 * @see GenericDao#delete(Long)
	 */
	public void delete(Long authorId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(AUTHOR_DELETE);
			preparedStatement.setLong(1, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException exception){
			throw new DAOException("Error deleting author entity (setting expired properties).", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, dataSource);
		}	
	}
	
	/**
	 * @see IAuthorDao#readAll()
	 */
	public List<Author> readAll() throws DAOException{
		List<Author> listAuthors = null;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try{
			Author author;
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(AUTHOR_READ_ALL);
			listAuthors = new ArrayList<Author>();
			while (resultSet.next()){
				author = parseResultSet(resultSet);
				listAuthors.add(author);
			}		
		} catch (SQLException exception){
			throw new DAOException("Error reading all authors entities.", exception);
		} finally {
			CloseUtility.close(connection, statement, resultSet, dataSource);
		}
		return listAuthors;
	}
	
	/**
	 * @see IAuthorDao#read(Long, Long)
	 */
	public List<Author> read(Long startPosition, Long endPosition)	throws DAOException {
		List<Author> listAuthors = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			Author author;
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(AUTHOR_READ);
			preparedStatement.setLong(1, startPosition);
			preparedStatement.setLong(2, endPosition);
			resultSet = preparedStatement.executeQuery();
			listAuthors = new ArrayList<Author>();
			while (resultSet.next()){
				author = parseResultSet(resultSet);
				listAuthors.add(author);
			}		
		} catch (SQLException exception){
			throw new DAOException(String.format("Error reading authors entities between %d and %d positions.", startPosition, endPosition), exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return listAuthors;
	}
	
	/**
	 * @see GenericDao#readById(Long)
	 */
	public Author readById(Long authorId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Author author = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(AUTHOR_READ_BY_ID);
			preparedStatement.setLong(1, authorId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()){
				author = parseResultSet(resultSet);
			}
		} catch (SQLException exception){
			throw new DAOException("Error reading author entity by AUTHOR_ID.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return author;
	}
	
	/**
	 * @see IAuthorDao#createLink(Long, Long)
	 */
	public void createLink(Long authorId, Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(AUTHOR_CREATE_LINK);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException exception){
			throw new DAOException("Error creating author-news link.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, dataSource);
		}
	}		

	/**
	 * @see IAuthorDao#deleteLink(Long)
	 */
	public void deleteLink(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(AUTHOR_DELETE_LINK);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException exception){
			throw new DAOException("Error deleting author-news link.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, dataSource);
		}
	}
	
	/**
	 * @see IAuthorDao#readAuthorByNewsId(Long)
	 */
	public Author readAuthorByNewsId(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Author author = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(AUTHOR_READ_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				author = parseResultSet(resultSet);
			}
		} catch (SQLException exception){
			throw new DAOException("Error reading author entity by NEWS_ID.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return author;
	}
	
	/**
	 * @see IAuthorDao#readAllNotExpired()
	 */
	public List<Author> readAllNotExpired() throws DAOException{
		List<Author> listAuthors = null;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try{
			Author author;
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(AUTHOR_READ_ALL);
			listAuthors = new ArrayList<Author>();
			while (resultSet.next()){
				author = parseResultSet(resultSet);
				if (author.getExpired() == null){
					listAuthors.add(author);
				}
			}		
		} catch (SQLException exception){
			throw new DAOException("Error reading all authors entities.", exception);
		} finally {
			CloseUtility.close(connection, statement, resultSet, dataSource);
		}
		return listAuthors;
	}
	
	private Author parseResultSet(ResultSet resultSet) throws SQLException{
		Author author = new Author();
		author.setAuthorId(resultSet.getLong(AT_AUTHOR_ID));
		author.setAuthorName(resultSet.getString(AT_AUTHOR_NAME));
		author.setExpired(resultSet.getTimestamp(AT_EXPIRED));
		return author;
	}
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
