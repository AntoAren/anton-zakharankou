package com.epam.zakharankou.newsmanagement.service;

import java.util.List;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.Tag;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

/**
 * TagService class is service which work with {@link com.epam.zakharankou.newsmanagement.entity.Tag} action.
 * @author Anton_Zakharankou
 *
 */
public interface TagService {
	
	/**
	 * @see MainNewsManagementService#addTag(Tag)
	 */
	public Long	addTag(Tag tag) throws ServiceException;	
	
	/**
	 * @see MainNewsManagementService#deleteTag(Long)
	 */
	public void deleteTag(Long tagId) throws ServiceException;
	
	/**
	 * @see MainNewsManagementService#readAllTags()
	 */
	public List<Tag> readAllTags() throws ServiceException;
	
	/**
	 * Updates tag entity.
	 * @param tag - new parameters.
	 * @throws ServiceException if {@link DAOException} was thrown.
	 */
	public void updateTag(Tag tag) throws ServiceException;
}