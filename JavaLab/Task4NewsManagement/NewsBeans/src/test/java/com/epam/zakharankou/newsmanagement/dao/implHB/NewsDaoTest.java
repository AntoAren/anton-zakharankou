package com.epam.zakharankou.newsmanagement.dao.implHB;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import com.epam.zakharankou.newsmanagement.dao.implHB.NewsDao;
import com.epam.zakharankou.newsmanagement.entity.Author;
import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.SearchCriteria;
import com.epam.zakharankou.newsmanagement.entity.Tag;

@SpringApplicationContext({"configTest.xml"})
@DataSet(value = "DataSetTest.xml", loadStrategy = CleanInsertLoadStrategy.class)
public class NewsDaoTest extends UnitilsJUnit4{
	
	@SpringBean("newsDao")
	private NewsDao newsDao;	
	
	@Test
	public void createTest() throws Exception{
		News expectedNews = new News();
		expectedNews.setTitle("Title100");
		expectedNews.setShortText("ShortText100");
		expectedNews.setFullText("FullText100");
		expectedNews.setCreationDate(new Date());
		expectedNews.setModificationDate(new Date());
		
		Author expectedAuthor = new Author();
		expectedAuthor.setAuthorId(1L);
		expectedAuthor.setAuthorName("Dmitriy");
		expectedAuthor.setExpired(null);
		expectedNews.addAuthor(expectedAuthor);
		
		Tag firstTag = new Tag();
		Tag secondTag = new Tag();
		firstTag.setTagId(new Long(1L));
		firstTag.setTagName("Tag1");
		secondTag.setTagId(new Long(2L));
		secondTag.setTagName("Tag2");
		
		expectedNews.addTag(firstTag);
		expectedNews.addTag(secondTag);
		
		Long actualNewsId = newsDao.create(expectedNews);
		News actualNews = newsDao.readById(actualNewsId);
		
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());
	}
	
	@Test
	public void readByIdTest() throws Exception{
		Long newsId = new Long(2L);
		News expectedNews = new News();
		expectedNews.setTitle("Title2");
		expectedNews.setShortText("ShortText2");
		expectedNews.setFullText("FullText2");
		
		News actualNews = newsDao.readById(newsId);
		
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());
	}
	
	@Test
	public void updateTest() throws Exception{
		News expectedNews = new News();
		expectedNews.setNewsId(new Long(1L));
		expectedNews.setTitle("Title11111");
		expectedNews.setShortText("ShortText11111");
		expectedNews.setFullText("FullText11111");
		expectedNews.setCreationDate(new Date());
		expectedNews.setModificationDate(new Date());
		
		Author expectedAuthor = new Author();
		expectedAuthor.setAuthorId(2L);
		expectedAuthor.setAuthorName("Ivan");
		expectedAuthor.setExpired(null);
		expectedNews.addAuthor(expectedAuthor);
		
		Tag firstTag = new Tag();
		Tag secondTag = new Tag();
		firstTag.setTagId(new Long(3L));
		firstTag.setTagName("Tag3");
		secondTag.setTagId(new Long(4L));
		secondTag.setTagName("Tag4");
		
		expectedNews.addTag(firstTag);
		expectedNews.addTag(secondTag);
				
		newsDao.update(expectedNews);
		News actualNews = newsDao.readById(expectedNews.getNewsId());		
		
		assertEquals(expectedNews.getTitle(), actualNews.getTitle());
		assertEquals(expectedNews.getShortText(), actualNews.getShortText());
		assertEquals(expectedNews.getFullText(), actualNews.getFullText());
	}
	
	@Test
	public void deleteTest() throws Exception{
		Long newsId = new Long(3L);
		
		newsDao.delete(newsId);		
		News actualNews = newsDao.readById(newsId);
		
		assertNull(actualNews);
	}
	
	@Test
	public void readTest() throws Exception{
		Long startPosition = new Long(2L);
		Long endPosition = new Long(3L);
		News firstNews = new News();
		News secondNews = new News();
		firstNews.setTitle("Title3");
		firstNews.setShortText("ShortText3");
		firstNews.setFullText("FullText3");
		secondNews.setTitle("Title2");
		secondNews.setShortText("ShortText2");
		secondNews.setFullText("FullText2");
		
		List<News> actualNewsList = newsDao.read(startPosition, endPosition);		
		
		assertEquals(firstNews.getTitle(), actualNewsList.get(0).getTitle());
		assertEquals(firstNews.getShortText(), actualNewsList.get(0).getShortText());
		assertEquals(firstNews.getFullText(), actualNewsList.get(0).getFullText());
		assertEquals(secondNews.getTitle(), actualNewsList.get(1).getTitle());
		assertEquals(secondNews.getShortText(), actualNewsList.get(1).getShortText());
		assertEquals(secondNews.getFullText(), actualNewsList.get(1).getFullText());
	}
	
	@Test
	public void readAllTest() throws Exception{
		News firstNews = new News();
		News secondNews = new News();
		News thirdNews = new News();
		News fourthNews = new News();
		firstNews.setTitle("Title4");
		secondNews.setTitle("Title3");
		thirdNews.setTitle("Title2");
		fourthNews.setTitle("Title1");
		
		List<News> actualNewsList = newsDao.readAll();
		
		assertEquals(firstNews.getTitle(), actualNewsList.get(0).getTitle());
		assertEquals(secondNews.getTitle(), actualNewsList.get(1).getTitle());
		assertEquals(thirdNews.getTitle(), actualNewsList.get(2).getTitle());
		assertEquals(fourthNews.getTitle(), actualNewsList.get(3).getTitle());
	}
	
	@Test
	public void searchNewsTest() throws Exception{
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(1L);
		List<Long> tagIds = new ArrayList<Long>();
		tagIds.add(1L);
		tagIds.add(4L);
		searchCriteria.setTagsId(tagIds);
		News firstNews = new News();
		News secondNews = new News();
		firstNews.setNewsId(2L);
		secondNews.setNewsId(1L);
		
		
		List<News> actualNews = newsDao.searchNews(searchCriteria, 1L, 2L);
		
		assertEquals(firstNews.getNewsId(), actualNews.get(0).getNewsId());
		assertEquals(secondNews.getNewsId(), actualNews.get(1).getNewsId());
	}
	
	@Test
	public void countAllNewsTest() throws Exception{
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(1L);
		List<Long> tagIds = new ArrayList<Long>();
		tagIds.add(1L);
		tagIds.add(4L);
		searchCriteria.setTagsId(tagIds);
		Long expectedNewsCount = 2L;
		
		Long actualNewsCount = newsDao.countAllNews(searchCriteria);
		
		assertEquals(expectedNewsCount, actualNewsCount);
	}
	
	@Test
	public void getNextNewsIdTest() throws Exception{
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(1L);
		List<Long> tagIds = new ArrayList<Long>();
		tagIds.add(1L);
		tagIds.add(4L);
		searchCriteria.setTagsId(tagIds);
		Long newsId = 1L;
		Long expectedNewsId = null;
		
		Long actualNewsId = newsDao.getNextNewsId(searchCriteria, newsId);
		
		assertEquals(expectedNewsId, actualNewsId);		
	}
	
	@Test
	public void getPrevNewsIdTest() throws Exception{
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(1L);
		List<Long> tagIds = new ArrayList<Long>();
		tagIds.add(1L);
		tagIds.add(4L);
		searchCriteria.setTagsId(tagIds);
		Long newsId = 1L;
		Long expectedNewsId = 2L;
		
		Long actualNewsId = newsDao.getPrevNewsId(searchCriteria, newsId);
		
		assertEquals(expectedNewsId, actualNewsId);		
	}
	
	@Test
	public void getCurrentPageForNewsTest() throws Exception{
		SearchCriteria searchCriteria = new SearchCriteria();
		Long expectedPosition = 2L;
		Long newsId = 3L;
		
		Long actualPosition = newsDao.getCurrentPageForNews(searchCriteria, newsId);
		
		assertEquals(expectedPosition, actualPosition);
	}
}
