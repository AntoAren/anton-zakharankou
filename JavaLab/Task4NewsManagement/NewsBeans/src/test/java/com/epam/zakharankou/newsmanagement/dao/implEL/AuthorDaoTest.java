package com.epam.zakharankou.newsmanagement.dao.implEL;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import com.epam.zakharankou.newsmanagement.entity.Author;

@SpringApplicationContext({"configTest.xml"})
@DataSet(value = {"dataSetTest.xml"} ,loadStrategy = CleanInsertLoadStrategy.class)
public class AuthorDaoTest extends UnitilsJUnit4{
	
	@SpringBean("authorDao")
	private AuthorDao authorDao;
	
	@Test
	public void createTest() throws Exception{
		Author expectedAuthor = new Author();
		expectedAuthor.setAuthorName("testName");
		
		Long authorId = authorDao.create(expectedAuthor);
		Author actualAuthor = authorDao.readById(authorId);
		
		assertEquals(expectedAuthor.getAuthorName(), actualAuthor.getAuthorName());
	}
	
	@Test
	public void readByIdTest() throws Exception{
		Author expectedAuthor = new Author();
		expectedAuthor.setAuthorId(3L);
		expectedAuthor.setAuthorName("Sanja");
		
		Author actualAuthor = authorDao.readById(3L);
		
		assertEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void updateTest() throws Exception{
		Author expectedAuthor = new Author();
		expectedAuthor.setAuthorId(3L);
		expectedAuthor.setAuthorName("Sanja");
		
		authorDao.update(expectedAuthor);
		Author actualAuthor = authorDao.readById(3L);
		
		assertEquals(expectedAuthor, actualAuthor);		
	}
	
	@Test
	public void deleteTest() throws Exception{
		Long authorId = new Long(3L);
		
		authorDao.delete(authorId);
		Author actualAuthor = authorDao.readById(authorId);
		
		assertNotNull(actualAuthor.getExpired());
	}
	
	@Test
	public void readAllTest() throws Exception{
		List<Author> expectedListAuthors = new ArrayList<Author>();
		Author firstAuthor = new Author();
		Author secondAuthor = new Author();
		Author thirdAuthor = new Author();
		Author fourthAuthor = new Author();
		firstAuthor.setAuthorId(new Long(1L));
		firstAuthor.setAuthorName("Dmitriy");
		secondAuthor.setAuthorId(new Long(2L));
		secondAuthor.setAuthorName("Ivan");
		thirdAuthor.setAuthorId(new Long(3L));
		thirdAuthor.setAuthorName("Oleg");
		fourthAuthor.setAuthorId(new Long(4L));
		fourthAuthor.setAuthorName("Roma");
		expectedListAuthors.add(firstAuthor);
		expectedListAuthors.add(secondAuthor);
		expectedListAuthors.add(thirdAuthor);
		expectedListAuthors.add(fourthAuthor);
		
		List<Author> actualListAuthors = authorDao.readAll();
		
		assertEquals(expectedListAuthors, actualListAuthors);
	}
	
	@Test
	public void readAllNotExpiredTest() throws Exception{
		List<Author> expectedListAuthors = new ArrayList<Author>();
		Author firstAuthor = new Author();
		Author secondAuthor = new Author();
		Author thirdAuthor = new Author();
		firstAuthor.setAuthorId(new Long(1L));
		firstAuthor.setAuthorName("Dmitriy");
		secondAuthor.setAuthorId(new Long(2L));
		secondAuthor.setAuthorName("Ivan");
		thirdAuthor.setAuthorId(new Long(3L));
		thirdAuthor.setAuthorName("Sanja");		
		expectedListAuthors.add(firstAuthor);
		expectedListAuthors.add(secondAuthor);
		expectedListAuthors.add(thirdAuthor);
		
		authorDao.delete(4L);
		
		List<Author> actualListAuthors = authorDao.readAllNotExpired();
		
		assertEquals(expectedListAuthors, actualListAuthors);
	}	
}
