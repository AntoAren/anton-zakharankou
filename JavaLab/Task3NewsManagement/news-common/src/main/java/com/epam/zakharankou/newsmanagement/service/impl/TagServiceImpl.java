package com.epam.zakharankou.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.dao.ITagDao;
import com.epam.zakharankou.newsmanagement.entity.Tag;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;
import com.epam.zakharankou.newsmanagement.service.TagService;

/**
 * TagServiceImpl class implements {@link com.epam.zakharankou.newsmanagement.service.TagService} interface.
 * @author Anton_Zakharankou
 *
 */
public class TagServiceImpl implements TagService{
	
	private static final Logger logger = Logger.getLogger("RollingFileAppender");
	
	private ITagDao tagDao;
	
	/**
	 * @see TagService#addTag(Tag)
	 */
	public Long addTag(Tag tag) throws ServiceException {
		Long tagId = null;
		try{
			tagId = tagDao.create(tag);
		} catch (DAOException exception){
			logger.error("Error adding tags entities.", exception);
			throw new ServiceException("Error adding tags entities.", exception);
		}
		return tagId;
	}	

	/**
	 * @see TagService#deleteTag(Long)
	 */
	public void deleteTag(Long tagId) throws ServiceException {
		try{
			tagDao.delete(tagId);
		} catch (DAOException exception){
			logger.error("Error deleting tag entity.", exception);
			throw new ServiceException("Error deleting tag entity.", exception);
		}		
	}
	
	/**
	 * @see TagService#readAllTags()
	 */
	public List<Tag> readAllTags() throws ServiceException{
		List<Tag> listTags = null;
		try{
			listTags = tagDao.readAll();
		} catch (DAOException exception){
			logger.error("Error reading all tags entity.", exception);
			throw new ServiceException("Error reading all tags entity.", exception);
		}
		return listTags;
	}
	
	/**
	 * @see TagService#updateTag(Tag)
	 */
	public void updateTag(Tag tag) throws ServiceException{
		try{
			tagDao.update(tag);
		} catch (DAOException exception){
			logger.error("Error updating tag entity.", exception);
			throw new ServiceException("Error updating tag entity.", exception);
		}
	}
	
	public void setTagDao(ITagDao tagDao) {
		this.tagDao = tagDao;
	}
}
