package com.epam.zakharankou.newsmanagement.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.dao.IAuthorDao;
import com.epam.zakharankou.newsmanagement.entity.Author;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/configTest.xml"})
public class AuthorServiceImplTest {
	
	@Mock
	private IAuthorDao authorDao;
	
	@InjectMocks
	private AuthorServiceImpl authorServiceImpl;
	
	@Before
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void addAuthorTest() throws DAOException, ServiceException{
		Author author = new Author();
		Long authorId = new Long(1L);
		when(authorDao.create(author)).thenReturn(authorId);
		assertEquals(authorId, authorServiceImpl.addAuthor(author));
		verify(authorDao, times(1)).create(author);
	}	
	
	@Test
	public void setExpiredAuthorTest() throws DAOException, ServiceException{
		Long authorId = new Long(1L);
		authorServiceImpl.setExpiredAuthor(authorId);
		verify(authorDao, times(1)).delete(authorId);
	}	
}
