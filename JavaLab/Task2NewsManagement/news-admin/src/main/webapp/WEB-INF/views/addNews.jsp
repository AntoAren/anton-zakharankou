<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="divNewsList">
	<table class="tableAddNews">
		<sf:form commandName="complexNews" action="/news-admin/news/saveNews">
			<tr>
				<td class="textAddNews">
					<spring:message code="local.title"/>:
				</td>
				<td class="textAddNews">
					<sf:input path="news.title" class="inputTitleAddNews"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<sf:errors path="news.title" cssClass="error"></sf:errors>
					<sf:errors path="news" cssClass="error"></sf:errors>
				</td>
			</tr>
			<tr>
				<td class="textAddNews">
					<spring:message code="local.date"/>:
				</td>
				<td class="textAddNews">
					<input type="text" value="${date}" readonly="readonly"/>
				</td>
			</tr>			
			<tr>
				<td class="textAddNews">
					<spring:message code="local.brief"/>:
				</td>
				<td class="textAddNews">
					<sf:textarea path="news.shortText" rows="3" cols="70"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<sf:errors path="news.shortText" cssClass="error"></sf:errors>
				</td>
			</tr>
			<tr>
				<td class="textAddNews">
					<spring:message code="local.content"/>:
				</td>
				<td class="textAddNews">
					<sf:textarea path="news.fullText" rows="7" cols="70"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<sf:errors path="news.fullText" cssClass="error"></sf:errors>
				</td>
			</tr>
			<tr align="center">
				<td colspan="2" class="textAddNews">
					<div>
						<table>
							<tr>								
								<td>
									<div>
										<sf:select path="authorId" >
											<sf:option value="0"><spring:message code="local.pleaseSelectTheAuthor"/></sf:option>
											<c:forEach var="author" items="${authors}">
												<option value="${author.authorId}" label="${author.authorName}"/>																				
											</c:forEach>
										</sf:select>
									</div>
								</td>
								<td>
									<div class="multiselect">						
										<div class="selectBox" onclick="showCheckboxes()">
											<select>
												<option><spring:message code="local.pleaseSelectTags"/></option>
											</select>							
										</div>
										<div id="checkboxes">
											<c:forEach var="tag" items="${tags}">
												<label for="${tag.tagId}">
													<sf:checkbox path="tagIds" value="${tag.tagId}" label="${tag.tagName}"/>
												</label>												
											</c:forEach>
										</div>
									</div>
								</td>								
							</tr>
							<tr>
								<td colspan="2">
									<sf:errors path="authorId" cssClass="error"></sf:errors>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="right"  class="textAddNews">
					<input type="submit" value="<spring:message code="local.save"/>">
				</td>
			</tr>
		</sf:form>
	</table>
</div>