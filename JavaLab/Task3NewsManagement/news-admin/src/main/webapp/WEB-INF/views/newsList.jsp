<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setBundle basename="local" var="bundle" />
<fmt:message bundle="${bundle}" key="local.date.format" var="pattern" />

<div class="divNewsList">
<script type="text/javascript">
$(function() {
    $('#light-theme').pagination({
    	pages: '${countPage}',
        itemsOnPage: 3,
        cssStyle: 'light-theme',
       	hrefTextPrefix: '',
       	currentPage: '${pageNumber}'
    });
});

</script>
	<div class="divFilter">
		<table>
			<tr>						
				<sf:form action="/news-admin/news/filter" commandName="searchCriteria">
					<td>
						<div>
							<sf:select path="authorId">
								<sf:option value="0"><spring:message code="local.pleaseSelectTheAuthor"/></sf:option>
								<c:forEach var="author" items="${authors}">
									<c:choose>
										<c:when test="${author.authorId == sessionScope.searchCriteria.authorId}">
											<option value="${author.authorId}" label="${author.authorName}" selected="selected"/>
										</c:when>
										<c:otherwise>
											<option value="${author.authorId}" label="${author.authorName}"/>
										</c:otherwise>
									</c:choose>								
								</c:forEach>
							</sf:select>
						</div>
					</td>
					<td>
						<div class="multiselect">						
							<div class="selectBox" onclick="showCheckboxes()">
								<select>
									<option><spring:message code="local.pleaseSelectTags"/></option>
								</select>							
							</div>
							<div id="checkboxes">
								<c:forEach var="tag" items="${tags}">
									<c:choose>
										<c:when test="${sessionScope.searchCriteria.tagsId.contains(tag.tagId)}">
											<label for="${tag.tagId}">
												<input type="checkbox" name="tagsId" checked="checked" value="${tag.tagId}"/><c:out value="${tag.tagName}"/>
											</label>
										</c:when>
										<c:otherwise>
											<label for="${tag.tagId}">
												<input type="checkbox" name="tagsId" value="${tag.tagId}"/><c:out value="${tag.tagName}"/>
											</label>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</div>
						</div>
					</td>
					<td>
						<input type="submit" value='<spring:message code="local.filter"/>'/>
					</td>				
				</sf:form>				
				<td>
					<sf:form action="/news-admin/news/reset">
						<input type="submit" name="reset" value="<spring:message code="local.reset"/>"/>					
					</sf:form>
				</td>
			</tr>
		</table>
	</div>
	<form action="/news-admin/news/delete" method="post">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />		
		<table class="tableNewsList">
			<c:if test="${listNews.size() == 0}">
				<tr align="center">
					<td>
						<h3><spring:message code="local.noSuchNews"/></h3>			
					</td>
				</tr>
			</c:if>	
			
			<c:forEach var="news" items="${listNews}">			
				<tr class="trNewsList">
					<td>
						<table class="tableNews">
							<tr class="trNews">
								<td colspan="3">
									<a class="textTitleLikeLink" href="/news-admin/news/showNews/${news.newsId}/"><c:out value="${news.title}"/> (<spring:message code="local.byAuthor"/> <c:out value="${news.authorList.get(0).authorName}"/>)</a>
								</td>	
								<td rowspan="2" class="textDate">
									<fmt:formatDate value="${news.modificationDate}" pattern="${pattern}" var="formattedDate" />
									<c:out value="${formattedDate}"/>
								</td>			
							</tr>	
							<tr class="trNews">
								<td colspan="3" class="textShortText">
									<c:out value="${news.shortText}"/>								
								</td>
							</tr>
							<tr class="trNews">
								<td class="tdEmpty">
								</td>
								<td class="textTags">
									<c:forEach var="tag" items="${news.tagList}">
										<c:out value="${tag.tagName},"/> 
									</c:forEach>								
								</td>
								<td class="textComments">
									<spring:message code="local.comments"/>(<c:out value="${news.commentList.size()}"/>)
								</td>
								<td class="textView">
									<a href="/news-admin/news/edit/${news.newsId}"><spring:message code="local.edit"/></a>
									<input type="checkbox" name="newsIds" value="${news.newsId}"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</c:forEach>
			<c:if test="${listNews.size() > 0 }">
				<tr align="right">
					<td>					
						<input type="submit" value='<spring:message code="local.delete"/>'/>
					</td>
				</tr>
			</c:if>
		</table>				
	</form>
	<div class="divPagination" align="center">	
		<div id="light-theme">
		
		</div>
	</div>
</div>