package com.epam.zakharankou.newsmanagement.dao;

import com.epam.zakharankou.newsmanagement.dao.GenericDao;
import com.epam.zakharankou.newsmanagement.entity.Comment;

/**
 * ICommentDao is interface which works with {@link com.epam.zakharankou.newsmanagement.entity.Comment} entity.
 * @author Anton_Zakharankou
 *
 */
public interface ICommentDao extends GenericDao<Comment> {
		
}