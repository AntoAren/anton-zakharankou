package com.epam.zakharankou.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.dao.ICommentDao;
import com.epam.zakharankou.newsmanagement.entity.Comment;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/configTest.xml"})
public class CommentServiceImplTest {
	
	@Mock
	private ICommentDao commentDao;
	
	@InjectMocks
	private CommentServiceImpl commentServiceImpl;
	
	@Before
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void addCommentTest() throws DAOException, ServiceException{
		Comment comment = new Comment();
		commentServiceImpl.addComment(comment);
		verify(commentDao, times(1)).create(comment);
	}
	
	@Test
	public void deleteCommentTest() throws DAOException, ServiceException{
		Long commentId = new Long(1L);
		commentServiceImpl.deleteComment(commentId);
		verify(commentDao, times(1)).delete(commentId);
	}
	
	@Test
	public void deleteCommentsByNewsIdTest() throws DAOException, ServiceException{
		Long newsId = new Long(1L);
		commentServiceImpl.deleteCommentsByNewsId(newsId);
		verify(commentDao, times(1)).deleteCommentsByNewsId(newsId);
	}
	
	@Test
	public void getCommentsByNewsIdTest() throws DAOException, ServiceException{
		List<Comment> comments = new ArrayList<Comment>();
		Long newsId = new Long(1L);
		when(commentDao.readCommentsByNewsId(newsId)).thenReturn(comments);
		assertEquals(comments, commentServiceImpl.getCommentsByNewsId(newsId));
		verify(commentDao, times(1)).readCommentsByNewsId(newsId);
	}
}
