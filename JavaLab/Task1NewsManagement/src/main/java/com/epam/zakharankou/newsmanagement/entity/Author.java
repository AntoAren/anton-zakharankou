package com.epam.zakharankou.newsmanagement.entity;

import java.sql.Timestamp;

/**
 * Author class provides author entity.
 * @author Anton_Zakharankou
 *
 */
public class Author{
	private Long authorId;
	private String authorName;
	private Timestamp expired;
	
	public Long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public Timestamp getExpired() {
		return expired;
	}
	public void setExpired(Timestamp expired) {
		this.expired = expired;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + authorId.intValue();
		result = prime * result
				+ ((authorName == null) ? 0 : authorName.hashCode());
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		return result;
	}
	
	@Override
	public String toString() {
		return "Author [authorId=" + authorId + ", authorName=" + authorName
				+ ", expired=" + expired + "]";
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (authorId.longValue() != other.getAuthorId().longValue())
			return false;
		if (authorName == null) {
			if (other.getAuthorName() != null)
				return false;
		} else if (!authorName.equals(other.getAuthorName()))
			return false;
		if (expired == null) {
			if (other.getExpired() != null)
				return false;
		} else if (!expired.equals(other.getExpired()))
			return false;
		return true;
	}
	
}
