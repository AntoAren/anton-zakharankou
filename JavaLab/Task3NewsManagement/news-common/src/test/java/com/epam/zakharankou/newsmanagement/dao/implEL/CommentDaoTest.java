package com.epam.zakharankou.newsmanagement.dao.implEL;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Date;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import com.epam.zakharankou.newsmanagement.entity.Comment;

@SpringApplicationContext({"configTest.xml"})
@DataSet(value = {"dataSetTest.xml"} ,loadStrategy = CleanInsertLoadStrategy.class)
public class CommentDaoTest extends UnitilsJUnit4 {
	
	@SpringBean("commentDao")
	private CommentDao commentDao;
	
	@Test
	public void createTest() throws Exception{
		Comment expectedComment = new Comment();
		expectedComment.setNewsId(new Long(3L));
		expectedComment.setCommentText("Comment333");
		expectedComment.setCreationDate(new Date());
				
		Long actualCommentId = commentDao.create(expectedComment);
		Comment actualComment = commentDao.readById(actualCommentId);
		
		assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
		assertEquals(expectedComment.getNewsId(), actualComment.getNewsId());
	}
	
	@Test
	public void readByIdTest() throws Exception{
		Long commentId = new Long(8L);
		Comment expectedComment = new Comment();
		expectedComment.setCommentText("Comment8");
		expectedComment.setNewsId(new Long(4L));
		
		Comment actualComment = commentDao.readById(commentId);
		
		assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
		assertEquals(expectedComment.getNewsId(), actualComment.getNewsId());
	}
	
	@Test
	public void updateTest() throws Exception{
		Comment expectedComment = new Comment();
		expectedComment.setCommentId(new Long(3L));
		expectedComment.setCommentText("Comment333");
		expectedComment.setNewsId(new Long(3L));
		expectedComment.setCreationDate(new Date());
		
		commentDao.update(expectedComment);
		Comment actualComment = commentDao.readById(expectedComment.getCommentId());
		
		assertEquals(expectedComment.getCommentText(), actualComment.getCommentText());
	}
	
	@Test
	public void deleteTest() throws Exception{
		Long commentId = new Long(6L);
		
		commentDao.delete(commentId);
		Comment actualComment = commentDao.readById(commentId);
		
		assertNull(actualComment);
	}	
}
