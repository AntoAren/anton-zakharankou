package com.epam.zakharankou.newsmanagement.dao.implHB;

import java.util.Date;
import java.util.List;




import javax.persistence.OptimisticLockException;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.epam.zakharankou.newsmanagement.dao.IAuthorDao;
import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.Author;

public class AuthorDao implements IAuthorDao{

	private SessionFactory sessionFactory;
	
	public Long create(Author author) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.persist(author);
			transaction.commit();
		} finally {
			session.close();
		}
		return author.getAuthorId();
	}	
	
	public Author readById(Long authorId) throws DAOException {		
		Session session = this.sessionFactory.openSession();
		Author author = null;
		try {
			Criteria criteria = session.createCriteria(Author.class).add(Restrictions.eq("authorId", authorId));
			author = (Author)criteria.uniqueResult();
		} finally {		
			session.close();
		}
		return author;
	}

	public boolean update(Author author) throws DAOException {
		boolean flag = true;
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();		
		try {
			session.update(author);
			transaction.commit();
		} catch (OptimisticLockException exception){
			flag = false;
			transaction.rollback();
		} finally {		
			session.close();
		}
		return flag;
	}

	public void delete(Long authorId) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Author author = (Author)session.createCriteria(Author.class).add(Restrictions.eq("authorId", authorId)).uniqueResult();
			author.setExpired(new Date());		
			transaction.commit();
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Author> readAll() throws DAOException {
		Session session = this.sessionFactory.openSession();
		List<Author> authorList = null;
		try {
			authorList = (List<Author>)session.createCriteria(Author.class).list();
		} finally {
			session.close();
		}
		return authorList;
	}

	@SuppressWarnings("unchecked")
	public List<Author> readAllNotExpired() throws DAOException {
		Session session = this.sessionFactory.openSession();
		List<Author> authorList = null;
		try {
			authorList = (List<Author>)session.createCriteria(Author.class).add(Restrictions.isNull("expired")).list(); 
		} finally {
			session.close();
		}
		return authorList;
	}		
	
	public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
