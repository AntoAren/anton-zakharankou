package com.epam.zakharankou.newsmanagement.service.impl;

import org.apache.log4j.Logger;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.dao.IAuthorDao;
import com.epam.zakharankou.newsmanagement.entity.Author;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;
import com.epam.zakharankou.newsmanagement.service.AuthorService;

/**
 * AuthorServiceImpl class implements {@link com.epam.zakharankou.newsmanagement.service.AuthorService} interface.
 * @author Anton_Zakharankou
 *
 */
public class AuthorServiceImpl implements AuthorService{

	private static final Logger logger = Logger.getLogger("RollingFileAppender");
			
	private IAuthorDao authorDao;
	
	/**
	 * @see AuthorService#addAuthor(Author)
	 */
	public Long addAuthor(Author author) throws ServiceException {
		Long authorId = null;
		try{
			authorId = authorDao.create(author);			
		} catch (DAOException exception){
			logger.error("Error adding author entity.", exception);
			throw new ServiceException("Error adding author entity.", exception);
		}
		return authorId;
	}

	/**
	 * @see AuthorService#createLinkAuthorWithNews(Author, Long)
	 */
	public void createLinkAuthorWithNews(Author author, Long newsId) throws ServiceException {
		try{
			authorDao.createLink(author.getAuthorId(), newsId);
		} catch (DAOException exception){
			logger.error("Error creating link between author entity and news entity.", exception);
			throw new ServiceException("Error creating link between author entity and news entity.", exception);
		}
	}

	/**
	 * @see AuthorService#deleteLinkAuthorWithNews(Long)
	 */
	public void deleteLinkAuthorWithNews(Long newsId) throws ServiceException {
		try{
			authorDao.deleteLink(newsId);
		} catch (DAOException exception){
			logger.error("Error deleting link between author entity and news entity.", exception);
			throw new ServiceException("Error deleting link between author entity and news entity.", exception);
		}
	}

	/**
	 * @see AuthorService#setExpiredAuthor(Long)
	 */
	public void setExpiredAuthor(Long authorId) throws ServiceException {
		try{
			authorDao.delete(authorId);
		} catch (DAOException exception){
			logger.error("Error setting expired author status.", exception);
			throw new ServiceException("Error setting expired author status.", exception);
		}
	}

	/**
	 * @see AuthorService#getAuthorByNewsId(Long)
	 */
	public Author getAuthorByNewsId(Long newsId) throws ServiceException {
		Author author = null;
		try{
			author = authorDao.readAuthorByNewsId(newsId);
		} catch (DAOException exception){
			logger.error("Error reading author entity by newsId.", exception);
			throw new ServiceException("Error reading author entity by newsId.", exception);
		}
		return author;
	}
	
	public void setAuthorDao(IAuthorDao authorDao) {
		this.authorDao = authorDao;
	}
}
