package com.epam.zakharankou.newsmanagement.dao;

import java.util.List;

import com.epam.zakharankou.newsmanagement.dao.GenericDao;
import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.Comment;

/**
 * ICommentDao is interface which works with {@link com.epam.zakharankou.newsmanagement.entity.Comment} entity.
 * @author Anton_Zakharankou
 *
 */
public interface ICommentDao extends GenericDao<Comment> {
	
	/**
	 * Deletes all {@link com.epam.zakharankou.newsmanagement.entity.Comment} entities by newsId from database.
	 * @throws DAOException if SQLException was thrown.
	 */
	public void deleteCommentsByNewsId(Long newsId) throws DAOException;
	
	/**
	 * Reads all {@link com.epam.zakharankou.newsmanagement.entity.Comment} entities by newsId from database.
	 * @return list of {@link com.epam.zakharankou.newsmanagement.entity.Comment} entity.
	 * @throws DAOException if SQLException was thrown.
	 */
	public List<Comment> readCommentsByNewsId(Long newsId) throws DAOException;
}