<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
	<head>		
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link href="<c:url value="/resources/css/adminStyle.css" />" rel="stylesheet"/>
		<script type="text/javascript" src="<c:url value="/resources/js/checkboxes.js" />" > </script>
		<script type="text/javascript" src="<c:url value="/resources/js/showHide.js" />" > </script>
		<title><tiles:getAsString name="title"/></title> 
	</head>
	<body>		
		<table class="tableMainTemplate">
      		<tr>
        		<td colspan="2" class="tdHeaderMainTemplate">
          			<tiles:insertAttribute name="header" />
        		</td>
      		</tr>
      		<tr>
      			<td class="tdMenuMainTemplate">
      				<tiles:insertAttribute name="menu"/>
      			</td>
        		<td class="tdBodyMainTemplate">
          			<tiles:insertAttribute name="body" />
        		</td>
      		</tr>
      		<tr align="center" >
        		<td colspan="2" class="tdFooterMainTemplate">
          			<tiles:insertAttribute name="footer" />
        		</td>
      		</tr>
    	</table>
	</body>
</html>