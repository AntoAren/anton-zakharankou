package com.epam.zakharankou.newsmanagement.entity;

import java.sql.Timestamp;

/**
 * Comment class provides comment entity.
 * @author Anton_Zakharankou
 *
 */
public class Comment{
	private Long commentId;	
	private Long newsId;
	private String commentText;
	private Timestamp creationDate;
	
	public Long getCommentId() {
		return commentId;
	}
	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}
	public Long getNewsId() {
		return newsId;
	}	
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + commentId.intValue();
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + newsId.intValue();
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentId != other.getCommentId())
			return false;
		if (commentText == null) {
			if (other.getCommentText() != null)
				return false;
		} else if (!commentText.equals(other.getCommentText()))
			return false;
		if (creationDate == null) {
			if (other.getCreationDate() != null)
				return false;
		} else if (!creationDate.equals(other.getCreationDate()))
			return false;
		if (newsId != other.getNewsId())
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Comment [commentId=" + commentId + ", newsId=" + newsId
				+ ", commentText=" + commentText + ", creationDate="
				+ creationDate + "]";
	}	
}
