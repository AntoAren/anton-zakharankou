package com.epam.zakharankou.newsmanagement.service;

import java.util.List;

import com.epam.zakharankou.newsmanagement.entity.Comment;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

/**
 * CommentService class is service which work with {@link com.epam.zakharankou.newsmanagement.entity.Comment} action.
 * @author Anton_Zakharankou
 *
 */
public interface CommentService {	
	
	/**
	 * Deletes {@link com.epam.zakharankou.newsmanagement.entity.Comment} entities by newsId.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public void deleteCommentsByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * Reads {@link com.epam.zakharankou.newsmanagement.entity.Comment} entities by newsId.
	 * @return list of {@link com.epam.zakharankou.newsmanagement.entity.Comment} entities
	 * @throws ServiceException if DAOException was thrown.
	 */
	public List<Comment> getCommentsByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * @see MainNewsManagementService#addComment(Comment)
	 */
	public Long addComment(Comment comment) throws ServiceException;
	
	/**
	 * @see MainNewsManagementService#deleteComment(Long)
	 */
	public void deleteComment(Long commentId) throws ServiceException;
}
