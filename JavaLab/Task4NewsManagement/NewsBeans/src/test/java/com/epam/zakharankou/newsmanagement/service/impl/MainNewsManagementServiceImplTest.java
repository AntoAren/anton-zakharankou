package com.epam.zakharankou.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.zakharankou.newsmanagement.entity.Author;
import com.epam.zakharankou.newsmanagement.entity.Comment;
import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.ComplexNews;
import com.epam.zakharankou.newsmanagement.entity.SearchCriteria;
import com.epam.zakharankou.newsmanagement.entity.Tag;
import com.epam.zakharankou.newsmanagement.service.AuthorService;
import com.epam.zakharankou.newsmanagement.service.CommentService;
import com.epam.zakharankou.newsmanagement.service.NewsService;
import com.epam.zakharankou.newsmanagement.service.TagService;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;
import com.epam.zakharankou.newsmanagement.service.impl.MainNewsManagementServiceImpl;

import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/configTest.xml"})
public class MainNewsManagementServiceImplTest {
	
	@Mock 
	private NewsService 	newsService;
	@Mock 
	private AuthorService 	authorService;
	@Mock 
	private CommentService	commentService;
	@Mock 
	private TagService		tagService;
	
	@InjectMocks
	private MainNewsManagementServiceImpl mainNewsManagementServiceImpl;	
	
	@Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);       
    }	
	
	@Test
	public void updateNewsTest() throws ServiceException{
		News news = new News();
		news.setNewsId(1L);
		mainNewsManagementServiceImpl.updateNews(news, new Long(1L), new ArrayList<Long>());		
		verify(newsService, times(1)).updateNews(news, new Long(1L), new ArrayList<Long>());
	}
	
	@Test
	public void deleteNewsTest() throws ServiceException{
		Long newsId = 1L;
		mainNewsManagementServiceImpl.deleteNews(newsId);
		verify(newsService, times(1)).deleteNews(newsId);		
	}	
	
	@Test
	public void viewSingleNews() throws ServiceException{
		News news = new News();
		Long newsId = new Long(1L);		
		when(newsService.viewSingleNews(newsId)).thenReturn(news);
		assertEquals(news, mainNewsManagementServiceImpl.viewSingleNews(newsId));
		verify(newsService, times(1)).viewSingleNews(newsId);
	}
	
	@Test
	public void addAuthorTest() throws ServiceException{
		Author author = new Author();
		author.setAuthorId(1L);
		when(authorService.addAuthor(author)).thenReturn(author.getAuthorId());
		assertEquals(author.getAuthorId(), mainNewsManagementServiceImpl.addAuthor(author));
		verify(authorService, times(1)).addAuthor(author);
	}
	
	@Test
	public void searchNewsTest() throws ServiceException{
		SearchCriteria searchCriteria = new SearchCriteria();
		Long startPosition = new Long(1L);
		Long endPosition = new Long(10L);
		List<News> news = new ArrayList<News>();
		when(newsService.searchNews(searchCriteria, startPosition, endPosition)).thenReturn(news);
		assertEquals(news, mainNewsManagementServiceImpl.searchNews(searchCriteria, startPosition, endPosition));
		verify(newsService, times(1)).searchNews(searchCriteria, startPosition, endPosition);
	}
	
	@Test
	public void addTagsTest() throws ServiceException{
		Tag tag = new Tag();
		Long tagId = new Long(1L);
		when(tagService.addTag(tag)).thenReturn(tagId);
		assertEquals(tagId, mainNewsManagementServiceImpl.addTag(tag));		
		verify(tagService, times(1)).addTag(tag);
	}
	
	@Test
	public void addCommentTest() throws ServiceException{
		Comment comment = new Comment();
		Long commentId = new Long(1L);
		comment.setCommentId(commentId);
		when(commentService.addComment(comment)).thenReturn(commentId);
		assertEquals(commentId, mainNewsManagementServiceImpl.addComment(comment));
		verify(commentService, times(1)).addComment(comment);
	}
	
	@Test
	public void deleteCommentTest() throws ServiceException{
		Long commentId = new Long(1L);
		mainNewsManagementServiceImpl.deleteComment(commentId);
		verify(commentService, times(1)).deleteComment(commentId);
	}
	
	@Test
	public void addNewsTest() throws ServiceException{
		Long newsId = new Long(1L);
		Long authorId = new Long(1L);
		ComplexNews complexNews = new ComplexNews();
		News news = new News();
		Author author = new Author();
		List<Comment> comments = new ArrayList<Comment>();
		List<Long> tagIds = new ArrayList<Long>();
		news.setNewsId(newsId);
		author.setAuthorId(authorId);
		complexNews.setComments(comments);
		complexNews.setAuthor(author);
		complexNews.setNews(news);
		
		when(newsService.addNews(news, tagIds, authorId)).thenReturn(newsId);
		assertEquals(newsId, mainNewsManagementServiceImpl.addNews(complexNews.getNews(), tagIds, authorId));
	}
	
	@Test
	public void deleteTagTest() throws ServiceException{
		Long tagId = new Long(1L);
		mainNewsManagementServiceImpl.deleteTag(tagId);
		verify(tagService, times(1)).deleteTag(tagId);
	}
}
