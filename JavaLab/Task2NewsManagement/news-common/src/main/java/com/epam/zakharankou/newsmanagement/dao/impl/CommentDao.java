package com.epam.zakharankou.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.dao.GenericDao;
import com.epam.zakharankou.newsmanagement.dao.ICommentDao;
import com.epam.zakharankou.newsmanagement.dao.impl.util.CloseUtility;
import com.epam.zakharankou.newsmanagement.entity.Comment;

/**
 * CommentDao class implements {@link com.epam.zakharankou.newsmanagement.dao.ICommentDao}.
 * @author Anton_Zakharankou
 *
 */

public class CommentDao implements ICommentDao {
	
	private DataSource dataSource;	
	
	private static final String COMMENT_CREATE = "INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES (CM_COMMENT_ID_SEQ.nextval, ?, ?, sysdate)";	
	private static final String COMMENT_READ_BY_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE (COMMENT_ID = ?)";
	private static final String COMMENT_READ_BY_NEWS_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE (NEWS_ID = ?)";
	private static final String COMMENT_UPDATE = "UPDATE COMMENTS SET NEWS_ID = ?, COMMENT_TEXT = ? WHERE (COMMENT_ID = ?)";
	private static final String COMMENT_DELETE = "DELETE FROM COMMENTS WHERE (COMMENT_ID = ?)";	
	private static final String COMMENT_DELETE_BY_NEWS_ID = "DELETE FROM COMMENTS WHERE (NEWS_ID = ?)";
	
	private static final String CM_COMMENT_ID = "COMMENT_ID";
	private static final String CM_NEWS_ID = "NEWS_ID";
	private static final String CM_COMMENT_TEXT = "COMMENT_TEXT";
	private static final String CM_CREATION_DATE = "CREATION_DATE";
	
	/**
	 * @see GenericDao#create(Object)
	 */
	public Long create(Comment comment) throws DAOException{
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Long commentId = null;
		String[] column = {"COMMENT_ID"};
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(COMMENT_CREATE, column);
			preparedStatement.setLong(1, comment.getNewsId());
			preparedStatement.setString(2, comment.getCommentText());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if ((resultSet != null) && (resultSet.next())){
				commentId = resultSet.getLong(1);
			}
		} catch (SQLException exception){
			throw new DAOException("Error creating comment entity.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return commentId;
	}
	
	/**
	 * @see GenericDao#update(Object)
	 */
	public void update(Comment comment) throws DAOException{
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(COMMENT_UPDATE);
			preparedStatement.setLong(1, comment.getNewsId());
			preparedStatement.setString(2, comment.getCommentText());
			preparedStatement.setLong(3, comment.getCommentId());
			preparedStatement.executeUpdate();
		} catch (SQLException exception){
			throw new DAOException("Error updating comment entity.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, dataSource);	
		}
	}
	
	/**
	 * @see GenericDao#delete(Long)
	 */
	public void delete(Long commentId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(COMMENT_DELETE);
			preparedStatement.setLong(1, commentId);
			preparedStatement.executeUpdate();
		} catch (SQLException exception){
			throw new DAOException("Error deleting comment entity.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, dataSource);
		}
	}	

	/**
	 * @see ICommentDao#readById(Long)
	 */
	public Comment readById(Long commentId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Comment comment = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(COMMENT_READ_BY_ID);
			preparedStatement.setLong(1, commentId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()){
				comment = parseResultSet(resultSet);				
			}
		} catch (SQLException exception){
			throw new DAOException("Error reading comment entity by COMMENT_ID.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return comment;
	}		

	/**
	 * @see ICommentDao#deleteCommentsByNewsId(Long)
	 */
	public void deleteCommentsByNewsId(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(COMMENT_DELETE_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException exception){
			throw new DAOException("Error deleting comments entities by NEWS_ID.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, dataSource);
		}
	}

	/**
	 * @see ICommentDao#readCommentsByNewsId(Long)
	 */
	public List<Comment> readCommentsByNewsId(Long newsId) throws DAOException {
		List<Comment> comments = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(COMMENT_READ_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			Comment comment;
			comments = new ArrayList<Comment>();
			while (resultSet.next()){
				comment = parseResultSet(resultSet);
				comments.add(comment);
			}
		} catch (SQLException exception){
			throw new DAOException("Error reading comments entities by NEWS_ID.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return comments;
	}	
	
	private Comment parseResultSet(ResultSet resultSet) throws SQLException{
		Comment comment = new Comment();
		comment.setCommentId(resultSet.getLong(CM_COMMENT_ID));
		comment.setCommentText(resultSet.getString(CM_COMMENT_TEXT));
		comment.setCreationDate(resultSet.getTimestamp(CM_CREATION_DATE));
		comment.setNewsId(resultSet.getLong(CM_NEWS_ID));
		return comment;
	}
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
