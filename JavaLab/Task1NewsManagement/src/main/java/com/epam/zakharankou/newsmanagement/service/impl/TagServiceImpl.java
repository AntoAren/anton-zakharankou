package com.epam.zakharankou.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.dao.ITagDao;
import com.epam.zakharankou.newsmanagement.entity.Tag;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;
import com.epam.zakharankou.newsmanagement.service.TagService;

/**
 * TagServiceImpl class implements {@link com.epam.zakharankou.newsmanagement.service.TagService} interface.
 * @author Anton_Zakharankou
 *
 */
public class TagServiceImpl implements TagService{
	
	private static final Logger logger = Logger.getLogger("RollingFileAppender");
	
	private ITagDao tagDao;
	
	public Long addTag(Tag tag) throws ServiceException {
		Long tagId = null;
		try{
			tagId = tagDao.create(tag);
		} catch (DAOException exception){
			logger.error("Error adding tags entities.", exception);
			throw new ServiceException("Error adding tags entities.", exception);
		}
		return tagId;
	}

	/**
	 * @see TagService#createLinksTagWithNews(List, Long)
	 */
	public void createLinksTagWithNews(List<Tag> tags, Long newsId) throws ServiceException {
		try{
			tagDao.createLinks(tags, newsId);
		} catch (DAOException exception){
			logger.error("Error creating links between tags entities and news entity.", exception);
			throw new ServiceException("Error creating links between tags entities and news entity.", exception);
		}
	}

	/**
	 * @see TagService#deleteLinksTagWithNews(Long)
	 */
	public void deleteLinksTagWithNews(Long newsId) throws ServiceException {
		try{
			tagDao.deleteLinksByNewsId(newsId);
		} catch (DAOException exception){
			logger.error("Error deleting links between tags entities and news entity.", exception);
			throw new ServiceException("Error deleting links between tags entities and news entity.", exception);
		}
	}

	/**
	 * @see TagService#getTagsByNewsId(Long)
	 */
	public List<Tag> getTagsByNewsId(Long newsId) throws ServiceException {
		List<Tag> tags = null;
		try{
			tags = tagDao.readTagsByNewsId(newsId);
		} catch (DAOException exception){
			logger.error("Error reading tags entities.", exception);
			throw new ServiceException("Error reading tags entities.", exception);
		}
		return tags;
	}

	/**
	 * @see TagService#deleteLinksByTagId(Long)
	 */
	public void deleteLinksByTagId(Long tagId) throws ServiceException {
		try{
			tagDao.deleteLinksByTagId(tagId);
		} catch (DAOException exception){
			logger.error("Error deleting links by tagId.", exception);
			throw new ServiceException("Error deleting links by tagId.", exception);
		}
	}

	/**
	 * @see TagService#deleteTag(Long)
	 */
	public void deleteTag(Long tagId) throws ServiceException {
		try{
			tagDao.delete(tagId);
		} catch (DAOException exception){
			logger.error("Error deleting tag entity.", exception);
			throw new ServiceException("Error deleting tag entity.", exception);
		}		
	}
	
	public void setTagDao(ITagDao tagDao) {
		this.tagDao = tagDao;
	}
}
