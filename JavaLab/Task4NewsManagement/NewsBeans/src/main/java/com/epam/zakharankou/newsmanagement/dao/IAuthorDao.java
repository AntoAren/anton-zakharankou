package com.epam.zakharankou.newsmanagement.dao;

import java.util.List;

import com.epam.zakharankou.newsmanagement.dao.GenericDao;
import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.Author;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

/**
 * IAuthorDao is interface which works with {@link com.epam.zakharankou.newsmanagement.entity.Author} entity.
 * @author Anton_Zakharankou
 *
 */
public interface IAuthorDao extends GenericDao<Author> {	
		
	/**
	 * Reads all {@link com.epam.zakharankou.newsmanagement.entity.Author} entities from database.
	 * @return list of {@link com.epam.zakharankou.newsmanagement.entity.Author} entities.
	 * @throws DAOException if SQLException was thrown.
	 */
	public List<Author> readAll() throws DAOException;
		
	/**
	 * Reads all not expired authors.
	 * @return list of authors.
	 * @throws ServiceException if SQLException was thrown.
	 */
	public List<Author> readAllNotExpired() throws DAOException;
}