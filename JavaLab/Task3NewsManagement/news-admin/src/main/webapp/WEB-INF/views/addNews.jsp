<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setBundle basename="local" var="bundle" />
<fmt:message bundle="${bundle}" key="local.date.format" var="pattern" />
<fmt:formatDate value="${date}" pattern="${pattern}" var="formattedDate" />
<div class="divNewsList">
	<table class="tableAddNews">		
		<sf:form commandName="complexNews" action="/news-admin/news/saveNews">
			<tr align="center">
				<td colspan="2" class="textAddNews">
					<div>
						<table>
							<tr>								
								<td>									
									<div>
										<sf:select path="authorId" >
											<sf:option value="0"><spring:message code="local.pleaseSelectTheAuthor"/></sf:option>											
											<c:forEach var="author" items="${authors}">
												<%-- <sf:option value="${author.authorId}" label="${author.authorName}"></sf:option> --%>
												<c:choose>
													<c:when test="${author.authorId == complexNews.authorId}">
														<option value="${author.authorId}" label="${author.authorName}" selected="selected"/>
													</c:when>
													<c:otherwise>
														<option value="${author.authorId}" label="${author.authorName}"/>
													</c:otherwise>
												</c:choose>																														
											</c:forEach>
										</sf:select>
									</div>
								</td>
								<td>
									<div class="multiselect">						
										<div class="selectBox" onclick="showCheckboxes()">
											<select>
												<option><spring:message code="local.pleaseSelectTags"/></option>
											</select>							
										</div>
										<div id="checkboxes">
											<c:forEach var="tag" items="${tags}">												
												<c:choose>
													<c:when test="${complexNews.tagIds.contains(tag.tagId)}">
														<label for="${tag.tagId}">
															<input type="checkbox" name="tagIds" value="${tag.tagId}" checked="checked"/><c:out value="${tag.tagName}"/>																														
														</label>
													</c:when>
													<c:otherwise>
														<label for="${tag.tagId}">
															<input type="checkbox" name="tagIds" value="${tag.tagId}"/><c:out value="${tag.tagName}"/>
														</label>
													</c:otherwise>
												</c:choose>												
											</c:forEach>
										</div>
									</div>
								</td>								
							</tr>
							<tr>
								<td colspan="2">
									<sf:errors path="authorId" cssClass="error"></sf:errors>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td class="textAddNews">
					<spring:message code="local.title"/>:
				</td>
				<td class="textAddNews">
					<sf:input path="news.title" class="inputTitleAddNews"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<sf:errors path="news.title" cssClass="error"></sf:errors>
					<sf:errors path="news" cssClass="error"></sf:errors>
				</td>
			</tr>
			<tr>
				<td class="textAddNews">
					<spring:message code="local.date"/>:
				</td>
				<td class="textAddNews">
					<input type="text" value="${formattedDate}" readonly="readonly"/>
				</td>
			</tr>			
			<tr>
				<td class="textAddNews">
					<spring:message code="local.brief"/>:
				</td>
				<td class="textAddNews">
					<sf:textarea path="news.shortText" rows="3" cols="70"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<sf:errors path="news.shortText" cssClass="error"></sf:errors>
				</td>
			</tr>
			<tr>
				<td class="textAddNews">
					<spring:message code="local.content"/>:
				</td>
				<td class="textAddNews">
					<sf:textarea path="news.fullText" rows="7" cols="70"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<sf:errors path="news.fullText" cssClass="error"></sf:errors>
				</td>
			</tr>			
			<tr>
				<td colspan="2" align="right"  class="textAddNews">
					<input type="submit" value="<spring:message code="local.save"/>">
				</td>
			</tr>
		</sf:form>
	</table>
</div>