package com.epam.zakharankou.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.dao.INewsDao;
import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.SearchCriteria;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;
import com.epam.zakharankou.newsmanagement.service.NewsService;

/**
 * NewsServiceImpl class implements {@link com.epam.zakharankou.newsmanagement.NewsService} interface.
 * @author Anton_Zakharankou
 *
 */
public class NewsServiceImpl implements NewsService{
	
	private static final Logger logger = Logger.getLogger("RollingFileAppender");
	
	private INewsDao newsDao;	

	/**
	 * @see NewsService#addNews(News)
	 */
	public Long addNews(News news) throws ServiceException {
		Long newsId = null;
		try {
			newsId = newsDao.create(news);
		} catch (DAOException exception) {
			logger.error("Error adding news entity.", exception);
			throw new ServiceException("Error adding news entity.", exception);
		}
		return newsId;
	}

	/**
	 * @see NewsService#updateNews(News)
	 */
	public void updateNews(News news) throws ServiceException {		
		try{
			newsDao.update(news);
		} catch (DAOException exception) {
			logger.error("Error editing news entity.", exception);
			throw new ServiceException("Error editing news entity.", exception);
		}
	}

	/**
	 * see NewsService#deleteNews(Long)
	 */
	public void deleteNews(Long newsId) throws ServiceException {
		try{
			newsDao.delete(newsId);
		} catch (DAOException exception) {
			logger.error("Error deleting news entity.", exception);
			throw new ServiceException("Error deleting news entity.", exception);
		}
	}

	/**
	 * @see NewsService#viewListOfNews(Long, Long)
	 */
	public List<News> viewListOfNews(Long startPosition, Long endPosition) throws ServiceException {
		List<News> news = null;
		try{
			news = newsDao.read(startPosition, endPosition);
		} catch (DAOException exception){
			logger.error("Error reading news entity.", exception);
			throw new ServiceException("Error reading news entity.", exception);
		}
		return news;
	}

	/**
	 * @see NewsService#viewSingleNews(Long)
	 */
	public News viewSingleNews(Long newsId) throws ServiceException {
		News news = null;
		try{
			news = newsDao.readById(newsId);
		} catch (DAOException exception){
			logger.error("Error reading news entity.", exception);
			throw new ServiceException("Error reading news entity.", exception);
		}
		return news;
	}

	/**
	 * @see NewsService#searchNews(SearchCriteria, Long, Long)
	 */
	public List<News> searchNews(SearchCriteria searchCriteria, Long startPosition, Long endPosition)	throws ServiceException {
		List<News> news = null;
		try{
			news = newsDao.searchNews(searchCriteria, startPosition, endPosition);
		} catch (DAOException exception){
			logger.error("Error searching news entity", exception);
			throw new ServiceException("Error searching news entity", exception);
		}
		return news;
	}	

	/**
	 * @see NewsService#countAllNews(SearchCriteria)
	 */
	public Long countAllNews(SearchCriteria searchCriteria)	throws ServiceException {
		Long countAllNews = null;
		try{
			countAllNews = newsDao.countAllNews(searchCriteria);
		} catch (DAOException exception){
			logger.error("Error counting all news entities.", exception);
			throw new ServiceException("Error counting all news entities.", exception);
		}
		return countAllNews;
	}
	
	/**
	 * @see NewsService#getNextNewsId(SearchCriteria, Long)
	 */
	public Long getNextNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
		Long nextNewsId = null;
		try{
			nextNewsId = newsDao.getNextNewsId(searchCriteria, newsId);
		} catch (DAOException exception){
			logger.error("Error reading next newsId.", exception);
			throw new ServiceException("Error reading next newsId.", exception);
		}
		return nextNewsId;
	}

	public Long getPrevNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
		Long prevNewsId = null;
		try{
			prevNewsId = newsDao.getPrevNewsId(searchCriteria, newsId);
		} catch(DAOException exception){
			logger.error("Error reading previous newsId.", exception);
			throw new ServiceException("Error reading previous newsId.", exception);
		}
		return prevNewsId;
	}
	
	public void setNewsDao(INewsDao newsDao) {
		this.newsDao = newsDao;
	}

	public Long getCurrentPageForNews(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
		Long pageNumber = null;
		try{
			pageNumber = newsDao.getCurrentPageForNews(searchCriteria, newsId);
		} catch (DAOException exception){
			logger.error("Error reading current page for news.", exception);
			throw new ServiceException("Error reading current page for news.", exception);
		}
		return pageNumber;
	}	
}
