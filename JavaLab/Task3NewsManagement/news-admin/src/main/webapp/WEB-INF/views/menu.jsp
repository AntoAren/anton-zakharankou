<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="divMenu">	
	<table class="tableMenu">		
		<tr>
			<td class="tdMenu">
				<c:choose>
					<c:when test="${currentPage == 'newsList'}">
						<b><a href="/news-admin/news/pages/1" class="linkText"><spring:message code="local.menuPointNewsList"/></a></b>
					</c:when>
					<c:otherwise>
						<a href="/news-admin/news/pages/1" class="linkText"><spring:message code="local.menuPointNewsList"/></a>
					</c:otherwise>
				</c:choose>				
			</td>
		</tr>
		<tr>
			<td class="tdMenu">				
				<c:choose>
					<c:when test="${currentPage == 'addNews'}">
						<b><a href="/news-admin/news/addNews" class="linkText"><spring:message code="local.menuPointAddNews"/></a></b>
					</c:when>
					<c:otherwise>
						<a href="/news-admin/news/addNews" class="linkText"><spring:message code="local.menuPointAddNews"/></a>
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
		<tr>
			<td class="tdMenu">
				<c:choose>
					<c:when test="${currentPage == 'addUpdateAuthors'}">
						<b><a href="/news-admin/author/addUpdateAuthors" class="linkText"><spring:message code="local.menuPointAddUpdateAuthors"/></a></b>
					</c:when>
					<c:otherwise>
						<a href="/news-admin/author/addUpdateAuthors" class="linkText"><spring:message code="local.menuPointAddUpdateAuthors"/></a>
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
		<tr>
			<td class="tdMenu">
				<c:choose>
					<c:when test="${currentPage == 'addUpdateTags'}">
						<b><a href="/news-admin/tag/addUpdateTags" class="linkText"><spring:message code="local.menuPointAddUpdateTags"/></a></b>
					</c:when>
					<c:otherwise>
						<a href="/news-admin/tag/addUpdateTags" class="linkText"><spring:message code="local.menuPointAddUpdateTags"/></a>
					</c:otherwise>
				</c:choose>
			</td>
		</tr>	
	</table>
</div>