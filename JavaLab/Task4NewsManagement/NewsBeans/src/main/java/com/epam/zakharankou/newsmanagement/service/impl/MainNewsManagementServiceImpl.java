package com.epam.zakharankou.newsmanagement.service.impl;

import java.util.List;


import com.epam.zakharankou.newsmanagement.entity.Author;
import com.epam.zakharankou.newsmanagement.entity.Comment;
import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.ComplexNews;
import com.epam.zakharankou.newsmanagement.entity.SearchCriteria;
import com.epam.zakharankou.newsmanagement.entity.Tag;
import com.epam.zakharankou.newsmanagement.service.AuthorService;
import com.epam.zakharankou.newsmanagement.service.CommentService;
import com.epam.zakharankou.newsmanagement.service.MainNewsManagementService;
import com.epam.zakharankou.newsmanagement.service.NewsService;
import com.epam.zakharankou.newsmanagement.service.TagService;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

/**
 * MainNewsManagementServiceImpl class implements {@link com.epam.zakharankou.newsmanagement.service.MainNewsManagementService}.
 * @author Anton_Zakharankou
 *
 */
public class MainNewsManagementServiceImpl implements MainNewsManagementService {	
	
	private NewsService newsService;
	private AuthorService authorService;
	private CommentService commentService;
	private TagService tagService;		
	

	/**
	 * @see MainNewsManagementService#updateNews(News)
	 */
	public boolean updateNews(News news, Long authorId, List<Long> tagIds) throws ServiceException {		
		return newsService.updateNews(news, authorId, tagIds);
	}

	/**
	 * @see MainNewsManagementService#deleteNews(Long)
	 */
	public void deleteNews(Long newsId) throws ServiceException {
		newsService.deleteNews(newsId);
	}	

	/**
	 * @see MainNewsManagementService#viewSingleNews(Long)
	 */
	public News viewSingleNews(Long newsId) throws ServiceException {
		return newsService.viewSingleNews(newsId);
	}

	/**
	 * @see MainNewsManagementService#addAuthor(Author)
	 */
	public Long addAuthor(Author author) throws ServiceException {
		return authorService.addAuthor(author);
	}

	/**
	 * @see MainNewsManagementService#searchNews(SearchCriteria, Long, Long)
	 */
	public List<News> searchNews(SearchCriteria searchCriteria, Long startPosition, Long endPosition)	throws ServiceException {
		return newsService.searchNews(searchCriteria, startPosition, endPosition);
	}

	/**
	 * @see MainNewsManagementService#addTag(Tag)
	 */
	public Long addTag(Tag tag) throws ServiceException {
		return tagService.addTag(tag);
	}

	/**
	 * @see MainNewsManagementService#addComplexNews(ComplexNews)
	 */
	public Long addNews(News news, List<Long> tagIds, Long authorId) throws ServiceException {
		return newsService.addNews(news, tagIds, authorId);
	}
	
	/**
	 * @see MainNewsManagementService#addComment(Comment) 
	 */
	public Long addComment(Comment comment) throws ServiceException {
		return commentService.addComment(comment);
	}

	/**
	 * @see MainNewsManagementService#deleteComment(Long)
	 */
	public void deleteComment(Long commentId) throws ServiceException {
		commentService.deleteComment(commentId);		
	}

	/**
	 * @see MainNewsManagementService#deleteTag(Long)
	 */
	public void deleteTag(Long tagId) throws ServiceException {
		tagService.deleteTag(tagId);
	}
	
	/**
	 * @see MainNewsManagementService#deleteAuthor(Long)
	 */
	public void deleteAuthor(Long authorId) throws ServiceException {
		authorService.setExpiredAuthor(authorId);		
	}

	/**
	 * @see MainNewsManagementService#countAllNews(SearchCriteria)
	 */
	public Long countAllNews(SearchCriteria searchCriteria)	throws ServiceException {
		return newsService.countAllNews(searchCriteria);
	}
	
	/**
	 * @see MainNewsManagementService#readAllAuthors()
	 */
	public List<Author> readAllAuthors() throws ServiceException {
		return authorService.readAllAuthors();
	}
	
	/**
	 * @see MainNewsManagementService#readAllAuthors()
	 */
	public List<Tag> readAllTags() throws ServiceException {
		return tagService.readAllTags();
	}
	
	/**
	 * @see MainNewsManagementService#getNextNewsId(SearchCriteria, Long)
	 */
	public Long getNextNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
		return newsService.getNextNewsId(searchCriteria, newsId);
	}

	/**
	 * @see MainNewsManagementService#getPrevNewsId(SearchCriteria, Long)
	 */
	public Long getPrevNewsId(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
		return newsService.getPrevNewsId(searchCriteria, newsId);
	}		
	
	/**
	 * @see MainNewsManagementService#getCurrentPageForNews(SearchCriteria, Long) 
	 */
	public Long getCurrentPageForNews(SearchCriteria searchCriteria, Long newsId) throws ServiceException {
		return newsService.getCurrentPageForNews(searchCriteria, newsId);
	}
		
	
	/**
	 * @see MainNewsManagementService#updateAuthor(Author)
	 */
	public void updateAuthor(Author author) throws ServiceException{
		authorService.updateAuthor(author);
	}
	
	/**
	 * @see MainNewsManagementService#updateTag(Tag)
	 */
	public void updateTag(Tag tag) throws ServiceException{
		tagService.updateTag(tag);
	}
	
	/**
	 * @see MainNewsManagementService#readAllNotExpiredAuthors()
	 */
	public List<Author> readAllNotExpiredAuthors() throws ServiceException{
		return authorService.readAllNotExpiredAuthors();
	}
	
	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}	
}