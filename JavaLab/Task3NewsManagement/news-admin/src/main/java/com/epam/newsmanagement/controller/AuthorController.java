package com.epam.newsmanagement.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.zakharankou.newsmanagement.entity.Author;
import com.epam.zakharankou.newsmanagement.service.MainNewsManagementService;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

@Controller
@RequestMapping(value = "/author")
public class AuthorController {
		
	@Autowired
	private MainNewsManagementService mainNewsManagementService;
		
	@RequestMapping(value = "/addUpdateAuthors")
	public String addUpdateAuthors(HttpSession session, Model model) {		
		try{
			List<Author> listAuthors = mainNewsManagementService.readAllNotExpiredAuthors();	
			model.addAttribute("authors", listAuthors);
			model.addAttribute("author", new Author());
			model.addAttribute("currentPage", "addUpdateAuthors");
		} catch (ServiceException exception){
			return "error";
		}
		return "addUpdateAuthors";
	}
	
	@RequestMapping(value = "/saveAuthor")
	public String saveAuthor(@Valid @ModelAttribute("author") Author author, BindingResult bindingResult, HttpSession session, Model model){
		try{						
	    	if (bindingResult.hasErrors()){
	    		List<Author> listAuthors = mainNewsManagementService.readAllNotExpiredAuthors();	
				model.addAttribute("authors", listAuthors);
				model.addAttribute("currentPage", "addUpdateAuthors");
				return "addUpdateAuthors";
	    	}
			mainNewsManagementService.addAuthor(author);
		} catch (ServiceException exception){
			return "error";
		}
		return "redirect:/author/addUpdateAuthors";
	}
	
	@RequestMapping(value = "/updateAuthor")
	public String updateAuthor(@Valid @ModelAttribute("author") Author author, BindingResult bindingResult, HttpSession session, Model model){
		try{
			if (bindingResult.hasErrors()){
				
				List<Author> listAuthors = mainNewsManagementService.readAllNotExpiredAuthors();	
				model.addAttribute("authors", listAuthors);
				model.addAttribute("authorIdError", author.getAuthorId());
				model.addAttribute("currentPage", "addUpdateAuthors");
				return "addUpdateAuthors";
			}						
			mainNewsManagementService.updateAuthor(author);
		} catch (ServiceException exception){
			return "error";
		}
		return "redirect:/author/addUpdateAuthors";
	}
	
	@RequestMapping(value = "/expireAuthor/{authorId}")
	public String expireAuthor(HttpSession session, Model model, @PathVariable Long authorId){
		try{
			mainNewsManagementService.deleteAuthor(authorId);
		} catch (ServiceException exception){
			return "error";
		}
		return "redirect:/author/addUpdateAuthors";
	}
}
