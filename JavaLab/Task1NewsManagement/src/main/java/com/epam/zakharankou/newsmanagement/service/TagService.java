package com.epam.zakharankou.newsmanagement.service;

import java.util.List;

import com.epam.zakharankou.newsmanagement.entity.Tag;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

/**
 * TagService class is service which work with {@link com.epam.zakharankou.newsmanagement.entity.Tag} action.
 * @author Anton_Zakharankou
 *
 */
public interface TagService {
	
	/**
	 * @see MainNewsManagementService#addTag(Tag)
	 */
	public Long	addTag(Tag tag) throws ServiceException;
	
	/**
	 * Creates link between {@link com.epam.zakharankou.newsmanagement.entity.Tag} entity and 
	 * {@link com.epam.zakharankou.newsmanagement.entity.News} entity. Writes in NEWS_TAGS table in database.
	 * @throws ServiceException if DAOException was thrown
	 */
	public void createLinksTagWithNews(List<Tag> tags, Long newsId) throws ServiceException;
	
	/**
	 * Deletes link between {@link com.epam.zakharankou.newsmanagement.entity.Tag} entity and 
	 * {@link com.epam.zakharankou.newsmanagement.entity.News} entity by newsId. Deletes from NEWS_TAGS table in database.
	 * @throws ServiceException if DAOException was thrown
	 */
	public void deleteLinksTagWithNews(Long newsId) throws ServiceException;
	
	/**
	 * Reads {@link com.epam.zakharankou.newsmanagement.entity.Tag} entities by newsId
	 * @return list of {@link com.epam.zakharankou.newsmanagement.entity.Tag} entities.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public List<Tag> getTagsByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * Deletes link between {@link com.epam.zakharankou.newsmanagement.entity.Tag} entity and 
	 * {@link com.epam.zakharankou.newsmanagement.entity.News} entity by tagId. Deletes from NEWS_TAGS table in database.
	 * @throws ServiceException if DAOException was thrown
	 */
	public void deleteLinksByTagId(Long tagId) throws ServiceException;
	
	/**
	 * @see MainNewsManagementService#deleteTag(Long)
	 */
	public void deleteTag(Long tagId) throws ServiceException;
}