<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<div>
<table class="tableHeader">
	<tr>
		<td rowspan="2">					
			<h1><spring:message code="local.newsPortalAdministration"/></h1>
		</td>			
		<td align="center">
			<spring:message code="local.hello"/>, <security:authentication property="principal.username" />
		</td>
		<td align="right">	
			<form action="/news-admin/logout" method="post">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				<input type="submit" value="<spring:message code="local.logout"/>"/>
			</form>
		</td>		
	</tr>
	<tr>	
		<td align="right" colspan="2">
			<span style="float: right">
				<c:choose>
					<c:when test="${paramString != null}">
   						<a href="?language=en${paramString}">EN</a>
    					<a href="?language=ru${paramString}">РУС</a>
    				</c:when>    				
    				<c:otherwise>
    					<a href="?language=en">EN</a>
    					<a href="?language=ru">РУС</a>
    				</c:otherwise>
    			</c:choose>
			</span>
		</td>
	</tr>
</table>
</div>