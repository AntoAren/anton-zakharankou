package com.epam.zakharankou.newsmanagement.dao.implHB;

import java.util.List;

import javax.persistence.OptimisticLockException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.epam.zakharankou.newsmanagement.dao.ITagDao;
import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.Tag;

public class TagDao implements ITagDao {
	
	private SessionFactory sessionFactory;
	
	public Long create(Tag tag) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			session.persist(tag);
			transaction.commit();
		} finally {
			session.close();
		}
		return tag.getTagId();
	}

	public Tag readById(Long tagId) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Tag tag = null;
		try {
			tag = (Tag)session.createCriteria(Tag.class).add(Restrictions.eq("tagId", tagId)).uniqueResult();
		} finally {
			session.close();
		}
		return tag;
	}

	public boolean update(Tag tag) throws DAOException {
		boolean flag = true;
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try{
			session.update(tag);
			transaction.commit();
		} catch(OptimisticLockException exception){
			flag = false;
			transaction.rollback();
		} finally {	
			session.close();
		}
		return flag;
	}

	public void delete(Long tagId) throws DAOException {
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			Tag tag = (Tag)session.createCriteria(Tag.class).add(Restrictions.eq("tagId", tagId)).uniqueResult();
			session.delete(tag);
			transaction.commit();
		} finally {
			session.close();
		}
	}	

	@SuppressWarnings("unchecked")
	public List<Tag> readAll() throws DAOException {
		Session session = this.sessionFactory.openSession();
		List<Tag> tagList = null;
		try {
			 tagList = (List<Tag>)session.createCriteria(Tag.class).list();
		} finally {
			session.close();
		}
		return tagList;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
