package com.epam.zakharankou.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.zakharankou.newsmanagement.dao.IUserDao;
import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.dao.impl.util.CloseUtility;
import com.epam.zakharankou.newsmanagement.dao.impl.util.MD5Util;
import com.epam.zakharankou.newsmanagement.entity.User;

/**
 * UserDao class implements {@link com.epam.zakharankou.newsmanagement.dao.IUserDao}.
 * @author Anton_Zakharankou
 *
 */
public class UserDao implements IUserDao {
	
	private DataSource dataSource;
	
	private static final String USER_HAS_USER = "SELECT USERS.USER_ID, USERS.USER_NAME, USERS.LOGIN, ROLES.ROLE_NAME FROM USERS INNER JOIN ROLES ON (USERS.USER_ID = ROLES.USER_ID) WHERE (LOGIN = ? ) AND (PASSWORD = ? )";
	
	private static final String UR_USER_NAME = "USER_NAME";
	private static final String UR_USER_ID = "USER_ID";
	private static final String UR_LOGIN = "LOGIN";
	private static final String RL_ROLE_NAME = "ROLE_NAME";
	
	/**
	 * @see IUserDao#hasUser(User)
	 */
	public User hasUser(User user) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		User readUser = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(USER_HAS_USER);
			preparedStatement.setString(1, user.getLogin());
			preparedStatement.setString(2, MD5Util.codingMD5(user.getPassword()));
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				readUser = new User();
				readUser.setUserId(resultSet.getLong(UR_USER_ID));
				readUser.setUserName(resultSet.getString(UR_USER_NAME));
				readUser.setLogin(resultSet.getString(UR_LOGIN));	
				readUser.setRoleName(resultSet.getString(RL_ROLE_NAME));
			}
		} catch (SQLException exception) {
			throw new DAOException("Error reading user entity.", exception);
		} finally{
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return readUser;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
