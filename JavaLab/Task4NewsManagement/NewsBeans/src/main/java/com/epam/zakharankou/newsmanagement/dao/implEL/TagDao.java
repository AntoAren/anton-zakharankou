package com.epam.zakharankou.newsmanagement.dao.implEL;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import com.epam.zakharankou.newsmanagement.dao.ITagDao;
import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.Tag;

/**
 * This class is Eclipse Link implementation of JPA for #{@link ITagDao}. 
 * @author Anton_Zakharankou
 *
 */
public class TagDao implements ITagDao {

	@PersistenceUnit(unitName = "entityManagerFactory")
	private EntityManagerFactory entityManagerFactory;
	
	public Long create(Tag tag) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		try {
			entityManager.persist(tag);
			entityTransaction.commit();
		} finally {
			entityManager.close();
		}
		return tag.getTagId();
	}

	public Tag readById(Long tagId) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Tag tag = null;
		try {
			tag = entityManager.find(Tag.class, tagId);
		} finally {
			entityManager.close();
		}
		return tag;
	}

	public boolean update(Tag tag) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		boolean flag = true;		
		try {
			entityManager.merge(tag);
			entityTransaction.commit();
		} catch (OptimisticLockException exception){
			flag = false;
			entityTransaction.rollback();
		} finally {
			entityManager.close();
		}
		return flag;
	}

	public void delete(Long tagId) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		Tag tag = null;
		try {
			tag = entityManager.find(Tag.class, tagId);
			entityManager.remove(tag);
			entityTransaction.commit();
		} finally {
			entityManager.close();
		}
	}

	public List<Tag> readAll() throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		List<Tag> tagList = null;
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<Tag> criteriaQuery = criteriaBuilder.createQuery(Tag.class);
			TypedQuery<Tag> typedQuery = entityManager.createQuery(criteriaQuery);
			tagList = typedQuery.getResultList();
		} finally {
			entityManager.close();
		}
		return tagList;
	}

}
