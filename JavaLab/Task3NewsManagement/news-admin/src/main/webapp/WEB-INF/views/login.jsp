<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div>
	<form action="/news-admin/login" method="post">
		<table class="tableLogin">
			<tr>
				<td>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				</td>
			</tr>
			<tr>
				<td>
					<spring:message code="local.loginText"/>:
				</td>
				<td>
					<input type="text" name="username" class="inputTextLoginTable" />
				</td>
			</tr>
			<tr>
				<td>
					<spring:message code="local.password"/>:
				</td>
				<td>
					<input type="password" name="password" class="inputTextLoginTable"/>
				</td>
			</tr>
			<c:if test="${login_error == true}">
				<tr>
					<td colspan="2">
						<spring:message code="local.wrongPasswordOrAndEmail"/>
					</td>
				</tr>
			</c:if>
			<tr>
				<td colspan="2" align="right">
					<input type="submit" value='<spring:message code="local.loginButton"/>'>
				</td>
			</tr>		
		</table>
	</form>
</div>