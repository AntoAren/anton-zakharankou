<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="divNewsList">
	<table class="tableAddNews">
		<sf:form commandName="complexNews" action="/news-admin/news/updateNews">		
			<tr>
				<td class="textAddNews">
					<spring:message code="local.title"/>:
				</td>
				<td class="textAddNews">					
					<sf:input path="news.title" value="${complexNews.news.title}" class="inputTitleAddNews"/>					
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<sf:errors path="news.title" cssClass="error"></sf:errors>
					<sf:errors path="news" cssClass="error"></sf:errors>
				</td>
			</tr>
			<tr>
				<td class="textAddNews">
					<spring:message code="local.date"/>:
				</td>
				<td class="textAddNews">
					<input value="${date}" type="text" readonly="readonly"/>
				</td>
			</tr>
			<tr>
				<td class="textAddNews">
					<spring:message code="local.brief"/>:
				</td>
				<td class="textAddNews">
					<sf:textarea path="news.shortText" rows="3" cols="70" value="${complexNews.news.shortText}"/>					
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<sf:errors path="news.shortText" cssClass="error"></sf:errors>
				</td>
			</tr>
			<tr>
				<td class="textAddNews">
					<spring:message code="local.content"/>:
				</td>
				<td class="textAddNews">
					<sf:textarea path="news.fullText" rows="7" cols="70" value="${complexNews.news.fullText}"/>					
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<sf:errors path="news.fullText" cssClass="error"></sf:errors>
				</td>
			</tr>
			<tr align="center">
				<td colspan="2" class="textAddNews">
					<div>
						<table>
							<tr>								
								<td>
									<div>
										<sf:select path="authorId">
											<sf:option value="0"><spring:message code="local.pleaseSelectTheAuthor"/></sf:option>
											<c:forEach var="author" items="${authors}">
												<c:choose>
													<c:when test="${author.authorId == complexNews.author.authorId}">
														<option value="${author.authorId}" label="${author.authorName}" selected="selected"/>
													</c:when>
													<c:otherwise>
														<option value="${author.authorId}" label="${author.authorName}"/>
													</c:otherwise>
												</c:choose>								
											</c:forEach>
										</sf:select>
									</div>
								</td>
								<td>
									<div class="multiselect">						
										<div class="selectBox" onclick="showCheckboxes()">
											<select>
												<option><spring:message code="local.pleaseSelectTags"/></option>
											</select>							
										</div>
										<div id="checkboxes">
											<c:forEach var="tag" items="${tags}">
												<c:choose>
													<c:when test="${complexNews.tagIds.contains(tag.tagId)}">
														<label for="${tag.tagId}">
															<sf:checkbox path="tagIds" value="${tag.tagId}" label="${tag.tagName}" checked="true"/>															
														</label>
													</c:when>
													<c:otherwise>
														<label for="${tag.tagId}">
															<sf:checkbox path="tagIds" value="${tag.tagId}" label="${tag.tagName}"/>	
														</label>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</div>
									</div>
								</td>								
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="right"  class="textAddNews">
					<sf:hidden path="news.newsId" value="${complexNews.news.newsId}"/>															
					<input type="submit" value="<spring:message code="local.update"/>">					
				</td>
			</tr>
		</sf:form>
	</table>
</div>