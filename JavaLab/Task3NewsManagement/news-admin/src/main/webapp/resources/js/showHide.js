function toggleShow(el) {
	var e = document.getElementById(el);
	var q = document.getElementById('hide'+el);
	var text = document.getElementById('text'+el);
	var value = document.getElementById('hiddenAuthorId'+el);
	var arr = document.getElementsByName('authors');
	
	if (e.style.display == 'block' || e.style.display=='')
    {
        e.style.display = 'none';
        q.style.display = 'block';
        text.readOnly = '';        
    }
    else 
    {
        e.style.display = 'block';
        q.style.display = 'none';
        text.readOnly = 'readonly';
    }
}

function toggleHide(el) {
	var e = document.getElementById(el);
	var q = document.getElementById('hide'+el);
	var text = document.getElementById('text'+el);
	var value = document.getElementById('hiddenAuthorId'+el);
	
	if (q.style.display == 'block' || q.style.display=='')
    {
        q.style.display = 'none';
        e.style.display = 'block';
        text.readOnly = 'readonly';
        text.value = value.value;
    }
    else 
    {
        q.style.display = 'block';
        e.style.display = 'none';
        text.readOnly = '';
        text.value = value.value;
    }
}