package com.epam.newsmanagement.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;

public class DateUtility {
	private DateUtility(){}
	
	public static Date checkDate(String stringDate){
		Date date = null;
		Locale locale = LocaleContextHolder.getLocale();
		try{	
			if ("ru".equals(locale.toString())){
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
				dateFormat.setLenient(false);
				date = dateFormat.parse(stringDate);
			}	
			if ("en".equals(locale.toString())){
				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				dateFormat.setLenient(false);
				date = dateFormat.parse(stringDate);
			}
		} catch (Exception exception){
			date = null;
		}
		return date;
	}
	
	public static Date getCurrentDate(){
		return new Date();
	}
}
