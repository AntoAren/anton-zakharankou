package com.epam.zakharankou.newsmanagement.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * News class provides news entity.
 * @author Anton_Zakharankou
 *
 */
@Entity
@Table(name = "NEWS")
@SequenceGenerator(name = "NS_NEWS_ID_SEQ", sequenceName = "NS_NEWS_ID_SEQ", allocationSize = 1)
public class News{
		
	@Id	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NS_NEWS_ID_SEQ")
	@Column(name = "NW_NEWS_ID", nullable = false, precision = 20)
	private Long newsId;
	
	@Column(name = "NW_TITLE", nullable = false, precision = 30)
	@NotNull(message = "{error.news.title.notNull}")
	@Size(min = 1, max = 30, message = "{error.news.title.size}")
	private String title;
	
	@Column(name = "NW_SHORT_TEXT", nullable = false, precision = 100)
	@NotNull(message = "{error.news.shortText.notNull}")
	@Size(min = 1, max = 100, message = "{error.news.shortText.size}")
	private String shortText;
	
	@Column(name = "NW_FULL_TEXT", nullable = false, precision = 2000)
	@NotNull(message = "{error.news.fullText.notNull}")
	@Size(min = 1, max = 2000, message = "{error.news.fullText.size}")
	private String fullText;
	
	@Column(name = "NW_CREATION_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	
	@Column(name = "NW_MODIFICATION_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;
	
	@Version
	@Column(name = "NW_VERSION", nullable = false)
	private Long version;
	
	@LazyCollection(value = LazyCollectionOption.FALSE)
	@ManyToMany
	@JoinTable(name = "NEWS_AUTHORS", joinColumns = { @JoinColumn(name = "NA_NEWS_ID",
	referencedColumnName = "NW_NEWS_ID") }, inverseJoinColumns = { @JoinColumn(name = "NA_AUTHOR_ID",
	referencedColumnName = "AT_AUTHOR_ID") })
	private List<Author> authorList;
	
	@LazyCollection(value = LazyCollectionOption.FALSE)
	@ManyToMany
	@JoinTable(name = "NEWS_TAGS", joinColumns = {@JoinColumn(name = "NT_NEWS_ID", 
	referencedColumnName = "NW_NEWS_ID")}, inverseJoinColumns = {@JoinColumn(name = "NT_TAG_ID",
	referencedColumnName = "TG_TAG_ID")})
	private List<Tag> tagList;
	
	@LazyCollection(value = LazyCollectionOption.FALSE)
	@OneToMany(targetEntity = Comment.class, orphanRemoval = true)
	@JoinColumn(name = "CM_NEWS_ID", referencedColumnName = "NW_NEWS_ID", updatable = false)
	private List<Comment> commentList;
	
	public News(){}
	
	public void addAuthor(Author author){
		if (authorList == null){
			authorList = new ArrayList<Author>();
		}
		authorList.add(author);
		
	}
	
	public void addTag(Tag tag){
		if (tagList == null){
			tagList = new ArrayList<Tag>();
		}
		tagList.add(tag);
	}
	
	public void addComment(Comment comment){
		if (commentList == null){
			commentList = new ArrayList<Comment>();
		}
		commentList.add(comment);
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public List<Author> getAuthorList() {
		return authorList;
	}

	public void setAuthorList(List<Author> authorList) {
		this.authorList = authorList;
	}

	public List<Tag> getTagList() {
		return tagList;
	}

	public void setTagList(List<Tag> tagList) {
		this.tagList = tagList;
	}

	public List<Comment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result	+ ((authorList == null) ? 0 : authorList.hashCode());
		result = prime * result	+ ((commentList == null) ? 0 : commentList.hashCode());
		result = prime * result	+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result	+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result	+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		result = prime * result	+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((tagList == null) ? 0 : tagList.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (authorList == null) {
			if (other.authorList != null)
				return false;
		} else if (!authorList.equals(other.authorList))
			return false;
		if (commentList == null) {
			if (other.commentList != null)
				return false;
		} else if (!commentList.equals(other.commentList))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (tagList == null) {
			if (other.tagList != null)
				return false;
		} else if (!tagList.equals(other.tagList))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "News [newsId=" + newsId + ", title=" + title + ", shortText="
				+ shortText + ", fullText=" + fullText + ", creationDate="
				+ creationDate + ", modificationDate=" + modificationDate
				+ ", version=" + version + ", authorList=" + authorList
				+ ", tagList=" + tagList + ", commentList=" + commentList + "]";
	}
}
