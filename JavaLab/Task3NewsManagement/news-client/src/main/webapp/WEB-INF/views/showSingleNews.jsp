<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmt:setBundle basename="local" var="bundle" />
<fmt:message bundle="${bundle}" key="local.date.format" var="pattern" />
<div>
	<c:choose>
		<c:when test="${news != null }">
			<div class="divBack">
				<a href="/news-client/back/${news.newsId}"><spring:message code="local.back"/></a>
			</div>
			<table class="tableSingleNews">
				<tr class="trSingleNews">
					<td class="textSingleNewsTitle">
						<b><c:out value="${news.shortText}"/></b> (<spring:message code="local.byAuthor"/> <c:out value="${news.authorList.get(0).authorName}"/>)
					</td>
					<td class="textDate">
						<fmt:formatDate value="${news.modificationDate}" pattern="${pattern}" var="formattedDate" />
						<c:out value="${formattedDate}"/>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="textSingleNewsFullText">
						<c:out value="${news.fullText}"/>
					</td>
				</tr>
			</table>
			<table class="tableComments">
				<c:forEach var="comment" items="${news.commentList}">
					<tr class="trComment">
						<td class="tdComment">
							<fmt:formatDate value="${comment.creationDate}" pattern="${pattern}" var="formattedDate" />
							<p class="textDateComment"><c:out value="${formattedDate}"/>
							<p class="textComment"><c:out value="${comment.commentText}"/> 
						</td>
					</tr>
				</c:forEach>
				<sf:form action="/news-client/postComment" commandName="comment">
					<tr>
						<td>
							<sf:textarea path="commentText" rows="3" cols="44"/>
						</td>			
					</tr>
					<tr>
						<td>
							<sf:errors path="commentText" cssClass="error"></sf:errors>
						</td>
					</tr>
					<tr>
						<td align="right">
							<sf:hidden path="newsId" value="${news.newsId}"/>
							<input type="submit" value="<spring:message code="local.postComment"/>"/>
						</td>
					</tr>		
				</sf:form>		
			</table>
			<table class="tablePrevNext">
				<tr>
					<td align="left">
						<c:if test="${prevNewsId != null}">
							<a href="/news-client/showSingleNews/${prevNewsId}"><spring:message code="local.prev"/></a>					
						</c:if>
					</td>
					<td align="right">
						<c:if test="${nextNewsId != null}">
							<form action="showSingleNews" method="post">
								<a href="/news-client/showSingleNews/${nextNewsId}"><spring:message code="local.next"/></a>
							</form>
						</c:if>
					</td>
				</tr>
			</table>
		</c:when>
		<c:otherwise>
			<table>
				<tr valign="middle">
					<td align="center">
						<h3><spring:message code="local.noSuchNewsId"/> </h3>
						<a href="/news-client"><spring:message code="local.goToHomePage"/></a>
					</td>
				</tr>
			</table>
		</c:otherwise>
	</c:choose>
</div>