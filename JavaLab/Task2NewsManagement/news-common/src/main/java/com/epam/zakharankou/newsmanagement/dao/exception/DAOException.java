package com.epam.zakharankou.newsmanagement.dao.exception;

/**
 * The class exception. It throws DAO layer.
 * @author Anton_Zakharankou
 *
 */
public class DAOException extends Exception{
	private static final long serialVersionUID = 1L;
	
	public DAOException(String message, Exception exception){
		super(message, exception);		
	}
}