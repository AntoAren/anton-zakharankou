package com.epam.zakharankou.newsmanagement.service;

import java.util.List;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.Author;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

/**
 * AuthorService class is service which work with {@link com.epam.zakharankou.newsmanagement.entity.Author} action.
 * @author Anton_Zakharankou
 *
 */
public interface AuthorService {
	
	/**
	 * @see MainNewsManagementService#addAuthor(Author)
	 * @throws ServiceException if DAOException was thrown.
	 */
	public Long addAuthor(Author author) throws ServiceException;
	
	/**
	 * Creates link between {@link com.epam.zakharankou.newsmanagement.entity.Author} entity and 
	 * {@link com.epam.zakharankou.newsmanagement.entity.News} entity. Writes in NEWS_AUTHORS table in database.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public void createLinkAuthorWithNews(Long authorId, Long newsId) throws ServiceException;
	
	/**
	 * Deletes link between {@link com.epam.zakharankou.newsmanagement.entity.Author} entity and 
	 * {@link com.epam.zakharankou.newsmanagement.entity.News} entity. Deletes from NEWS_AUTHORS table in database.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public void deleteLinkAuthorWithNews(Long newsId) throws ServiceException;
	
	/**
	 * Sets expired properties {@link com.epam.zakharankou.newsmanagement.entity.Author} entity.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public void setExpiredAuthor(Long authorId) throws ServiceException;
	
	/**
	 * Reads {@link com.epam.zakharankou.newsmanagement.entity.Author} entity by newsId
	 * @return {@link com.epam.zakharankou.newsmanagement.entity.Author} entity.
	 * @throws ServiceException if DAOException was thrown.
	 */
	public Author getAuthorByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * Reads all Authors
	 * @return list of Authors
	 * @throws ServiceException if DAOException was thrown.
	 */
	public List<Author> readAllAuthors() throws ServiceException;
	
	/**
	 * Updates author entity.
	 * @param author - new parameters.
	 * @throws ServiceException if {@link DAOException} was thrown.
	 */
	public void updateAuthor(Author author) throws ServiceException;
	
	/**
	 * Reads all not expired authors.
	 * @return list of authors.
	 * @throws ServiceException if SQLException was thrown.
	 */
	public List<Author> readAllNotExpiredAuthors() throws ServiceException;
}
