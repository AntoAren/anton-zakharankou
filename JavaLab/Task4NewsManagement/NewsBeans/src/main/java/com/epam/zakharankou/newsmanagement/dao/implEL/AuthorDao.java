package com.epam.zakharankou.newsmanagement.dao.implEL;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.epam.zakharankou.newsmanagement.dao.IAuthorDao;
import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.Author;

/**
 * This class is Eclipse Link Implementation of JPA for #{@link IAuthorDao}. 
 * @author Anton_Zakharankou
 *
 */
public class AuthorDao implements IAuthorDao {
	
	@PersistenceUnit(unitName = "entityManagerFactory")
	private EntityManagerFactory entityManagerFactory;
		
	public Long create(Author author){
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		try{
			entityManager.persist(author);
			entityTransaction.commit();
		} finally {
			entityManager.close();		
		}		
		return author.getAuthorId();				
	}

	public Author readById(Long authorId){
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Author author = null;
		try{
			author = entityManager.find(Author.class, authorId);
		} finally {
			entityManager.close();
		}
		return author;
	}

	public boolean update(Author author) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		boolean flag = true;
		
		try{
			entityManager.merge(author);
			entityTransaction.commit();
		} catch (OptimisticLockException exception){
			flag = false;
			entityTransaction.rollback();
		} finally {
			entityManager.close();
		}		
		return flag;
	}
		
	public void delete(Long authorId) throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		
		try {
			Author author = entityManager.find(Author.class, authorId);
			author.setExpired(new Date());		
			entityTransaction.commit();
		} finally{
			entityManager.close();
		}
	}
	
	public List<Author> readAll() throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		List<Author> authorList = null;
		try{
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<Author> criteriaQuery = criteriaBuilder.createQuery(Author.class);
			TypedQuery<Author> typedQuery = entityManager.createQuery(criteriaQuery);
			authorList = typedQuery.getResultList();
		} finally {
			entityManager.close();
		}
		return authorList;
	}

	public List<Author> readAllNotExpired() throws DAOException {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		List<Author> authorList = null;		
		try{
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<Author> criteriaQuery = criteriaBuilder.createQuery(Author.class);
			Root<Author> author = criteriaQuery.from(Author.class);
			criteriaQuery.where(criteriaBuilder.isNull(author.get("expired")));
			TypedQuery<Author> typedQuery = entityManager.createQuery(criteriaQuery);
			authorList = typedQuery.getResultList();
		} finally {
			entityManager.close();
		}
		return authorList;
	}	
}
