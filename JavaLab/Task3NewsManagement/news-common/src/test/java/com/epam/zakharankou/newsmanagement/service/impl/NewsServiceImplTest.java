package com.epam.zakharankou.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.dao.INewsDao;
import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.SearchCriteria;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:src/test/resources/configTest.xml"})
public class NewsServiceImplTest {
	
	@Mock
	private INewsDao newsDao;
	
	@InjectMocks
	private NewsServiceImpl newsServiceImpl;
	
	@Before
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void addNewsTest() throws DAOException, ServiceException{
		News news = new News();
		Long newsId = new Long(1L);
		
		List<Long> tagList = new ArrayList<Long>();
		when(newsDao.create(news)).thenReturn(newsId);
		assertEquals(newsId, newsServiceImpl.addNews(news, tagList, 1L));
		verify(newsDao, times(1)).create(news);
	}
	
	@Test
	public void updateNewsTest() throws DAOException, ServiceException{
		News news = new News();	
		newsServiceImpl.updateNews(news, 1L, new ArrayList<Long>());
		verify(newsDao, times(1)).update(news);
	}
	
	@Test
	public void deleteNewsTest() throws DAOException, ServiceException{
		Long newsId = new Long(1L);
		newsServiceImpl.deleteNews(newsId);
		verify(newsDao, times(1)).delete(newsId);
	}
	
	@Test
	public void viewListOfNewsTest() throws DAOException, ServiceException{
		List<News> news = new ArrayList<News>();
		Long startPosition = new Long(1L);
		Long endPosition = new Long(2L);
		when(newsDao.read(startPosition, endPosition)).thenReturn(news);
		assertEquals(news, newsServiceImpl.viewListOfNews(startPosition, endPosition));
		verify(newsDao, times(1)).read(startPosition, endPosition);
	}
	
	@Test
	public void viewSingleNewsTest() throws DAOException, ServiceException{
		News news = new News();
		Long newsId = new Long(1L);
		when(newsDao.readById(newsId)).thenReturn(news);
		assertEquals(news, newsServiceImpl.viewSingleNews(newsId));
		verify(newsDao, times(1)).readById(newsId);
	}
	
	@Test
	public void searchNewsTest() throws DAOException, ServiceException{
		List<News> news = new ArrayList<News>();
		Long startPosition = new Long(1L);
		Long endPosition = new Long(2L);
		SearchCriteria searchCriteria = new SearchCriteria();
		when(newsDao.searchNews(searchCriteria, startPosition, endPosition)).thenReturn(news);
		assertEquals(news, newsServiceImpl.searchNews(searchCriteria, startPosition, endPosition));
		verify(newsDao, times(1)).searchNews(searchCriteria, startPosition, endPosition);
	}
	
	@Test
	public void countAllNews() throws DAOException, ServiceException{
		SearchCriteria searchCriteria = new SearchCriteria();
		Long countNews = new Long(1L);
		when(newsDao.countAllNews(searchCriteria)).thenReturn(countNews);
		assertEquals(countNews, newsServiceImpl.countAllNews(searchCriteria));
		verify(newsDao, times(1)).countAllNews(searchCriteria);
	}
}
