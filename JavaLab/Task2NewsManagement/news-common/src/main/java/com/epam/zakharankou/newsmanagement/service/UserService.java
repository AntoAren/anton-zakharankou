package com.epam.zakharankou.newsmanagement.service;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.User;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

/**
 * UserService class is service which work with {@link com.epam.zakharankou.newsmanagement.entity.User} action.
 * @author Anton_Zakharankou
 *
 */
public interface UserService {
	
	/**
	 * Checks there are user or not.
	 * @param user - checking user.
	 * @return user if has, null if doesn't have.
	 * @throws ServiceException if {@link DAOException} was thrown.
	 */
	public User hasUser(User user) throws ServiceException; 
}
