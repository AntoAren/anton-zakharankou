package com.epam.zakharankou.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.dao.GenericDao;
import com.epam.zakharankou.newsmanagement.dao.INewsDao;
import com.epam.zakharankou.newsmanagement.dao.impl.util.CloseUtility;
import com.epam.zakharankou.newsmanagement.entity.News;
import com.epam.zakharankou.newsmanagement.entity.SearchCriteria;


/**
 * NewsDao class implements {@link com.epam.zakharankou.newsmanagement.dao.INewsDao}.
 * @author Anton_Zakharankou
 *
 */
public class NewsDao implements INewsDao {
	
	private DataSource dataSource;
	
	private static final String NEWS_CREATE = "INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (NS_NEWS_ID_SEQ.nextval, ?, ?, ?, sysdate, sysdate)";
	private static final String NEWS_READ = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM (SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, ROW_NUMBER() over (ORDER BY NEWS.MODIFICATION_DATE DESC, COUNT(COMMENTS.NEWS_ID) DESC) R FROM NEWS LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) GROUP BY NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, COMMENTS.NEWS_ID ) WHERE R BETWEEN ? AND ?  ";
	private static final String NEWS_READ_ALL = "SELECT DISTINCT nw_id, nw_title, nw_short, nw_full, nw_cre_date, nw_mod_date, COUNT_COUNT, r FROM ( SELECT DISTINCT NEWS.NEWS_ID as nw_id, NEWS.TITLE as nw_title, NEWS.SHORT_TEXT as nw_short, NEWS.FULL_TEXT as nw_full, NEWS.CREATION_DATE as nw_cre_date, NEWS.MODIFICATION_DATE as nw_mod_date, COUNT(COMMENTS.NEWS_ID) AS COUNT_COUNT, ROW_NUMBER() OVER (ORDER BY NEWS.MODIFICATION_DATE DESC, COUNT(COMMENTS.NEWS_ID) DESC) R FROM NEWS LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) group by NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE ) ";												
	private static final String NEWS_READ_BY_ID = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE (NEWS_ID = ?)";
	private static final String NEWS_UPDATE = "UPDATE NEWS SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, MODIFICATION_DATE = sysdate WHERE (NEWS_ID = ?)";
	private static final String NEWS_DELETE = "DELETE FROM NEWS WHERE (NEWS_ID = ?)";
	
	private static final String SEARCH_START = "SELECT DISTINCT nw_id, nw_title, nw_short, nw_full, nw_cre_date, nw_mod_date, count_count FROM ( SELECT DISTINCT nw_id, nw_title, nw_short, nw_full, nw_cre_date, nw_mod_date, COUNT_COUNT, R, Q FROM ( SELECT DISTINCT nw_id, nw_title, nw_short, nw_full, nw_cre_date, nw_mod_date, COUNT_COUNT, R, ROW_NUMBER() OVER (ORDER BY R ) Q FROM (";
	private static final String SEARCH_JOIN_TAGS = " INNER JOIN NEWS_TAGS ON (NEWS_TAGS.NEWS_ID = nw_id) ";
	private static final String SEARCH_JOIN_AUTHORS = " INNER JOIN NEWS_AUTHORS ON (NEWS_AUTHORS.NEWS_ID = nw_id) ";
	private static final String SEARCH_WHERE = " WHERE ";
	private static final String SEARCH_OPTION_TAGS_START = " (NEWS_TAGS.TAG_ID IN (";
	private static final String SEARCH_OPTION_TAGS_END = ")) AND ";
	private static final String SEARCH_OPTION_AUTHORS = " (NEWS_AUTHORS.AUTHOR_ID = ?) AND ";
	
	private static final String COUNT_START = "SELECT COUNT(nw_id) as COUNT_NEWS FROM ( ";
	private static final String COUNT_END = ")))";
	
	private static final String GET_NEXT_NEWS_ID_START = "SELECT nw_id, nw_title, next_news_id FROM( SELECT nw_id, nw_title, LEAD (nw_id,1) OVER (ORDER BY r) AS next_news_id FROM (";
	private static final String GET_PREV_NEWS_ID_START = "SELECT nw_id, nw_title, next_news_id FROM( SELECT nw_id, nw_title, LAG (nw_id,1) OVER (ORDER BY r) AS next_news_id FROM (";
	private static final String GET_NEWS_ID_END = " )) WHERE nw_id = ? ";
	
	private static final String NS_NEWS_ID = "NEWS_ID";
	private static final String NS_TITLE = "TITLE";
	private static final String NS_SHORT_TEXT = "SHORT_TEXT";
	private static final String NS_FULL_TEXT = "FULL_TEXT";
	private static final String NS_CREATION_DATE = "CREATION_DATE";
	private static final String NS_MODIFICATION_DATE = "MODIFICATION_DATE";  
	private static final String NEWS_ID = "nw_id";
	private static final String TITLE = "nw_title";
	private static final String SHORT_TEXT = "nw_short";
	private static final String FULL_TEXT = "nw_full";
	private static final String CREATION_DATE = "nw_cre_date";
	private static final String MODIFICATION_DATE = "nw_mod_date";
	private static final String COUNT = "COUNT_NEWS";
	private static final String NEXT_NEWS_ID = "next_news_id";
	private static final String CURRENT_PAGE = "R";
	
	private static final String NEW_SEARCH_PART_1 = " SELECT DISTINCT nw_id, nw_title, nw_short, nw_full, nw_cre_date, nw_mod_date, R FROM ( SELECT DISTINCT nw_id, nw_title, nw_short, nw_full, nw_cre_date, nw_mod_date, COUNT_COUNT, ROW_NUMBER() OVER (ORDER BY nw_mod_date DESC, COUNT_COUNT DESC) R FROM ( SELECT DISTINCT nw_id, nw_title, nw_short, nw_full, nw_cre_date, nw_mod_date, COUNT_COUNT FROM ( ";
	private static final String NEW_SEARCH_PART_2 = " SELECT DISTINCT NEWS.NEWS_ID as nw_id, NEWS.TITLE as nw_title, NEWS.SHORT_TEXT as nw_short, NEWS.FULL_TEXT as nw_full, NEWS.CREATION_DATE as nw_cre_date, NEWS.MODIFICATION_DATE as nw_mod_date, COUNT(COMMENTS.NEWS_ID) AS COUNT_COUNT, ROW_NUMBER() OVER (ORDER BY NEWS.MODIFICATION_DATE DESC, COUNT(COMMENTS.NEWS_ID) DESC) R FROM NEWS LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) group by NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE";
	private static final String NEW_SEARCH_PART_3 = " order by nw_mod_date desc, count_count DESC) ";
	private static final String NEW_SEARCH_PART_4 = " WHERE R BETWEEN ? AND ?";
	private static final String NEW_SEARCH_PART_5 = " order by R";
	
	private static final String GET_CURRENT_PAGE = "WHERE nw_id = ? ";
	
	
	/**
	 * @see GenericDao#create(Object)
	 */
	public Long create(News news) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Long newsId = null;
		String[] column = {"NEWS_ID"};
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(NEWS_CREATE, column);
			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getShortText());
			preparedStatement.setString(3, news.getFullText());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if ((resultSet != null) && (resultSet.next())){
				newsId = resultSet.getLong(1);
			}
		} catch (SQLException exception){
			throw new DAOException("Error creating news entity.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return newsId;
	}
	
	/**
	 * @see INewsDao#read(Long, Long)
	 */
	public List<News> read(Long startPosition, Long endPosition) throws DAOException {
		List<News> listNews = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			News news;
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(NEWS_READ);
			preparedStatement.setLong(1, startPosition);
			preparedStatement.setLong(2, endPosition);
			resultSet = preparedStatement.executeQuery();
			listNews = new ArrayList<News>();
			while (resultSet.next()){
				news = parseResultSet(resultSet);
				listNews.add(news);
			}		
		} catch (SQLException exception){
			throw new DAOException(String.format("Error reading news entities between %d and %d positions.", startPosition, endPosition), exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return listNews;
	}
	
	/**
	 * @see GenericDao#readById(Long)
	 */
	public News readById(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		News news = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(NEWS_READ_BY_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()){
				news = parseResultSet(resultSet);
			}
		} catch (SQLException exception){
			throw new DAOException("Error reading news entity by NEWS_ID.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return news;
	}
	
	/**
	 * @see GenericDao#update(Object)
	 */
	public void update(News news) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(NEWS_UPDATE);
			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getShortText());
			preparedStatement.setString(3, news.getFullText());
			preparedStatement.setLong(4, news.getNewsId());
			preparedStatement.executeUpdate();
		} catch (SQLException exception){
			throw new DAOException("Error updating news entity.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, dataSource);
		}
	}
	
	/**
	 * @see GenericDao#delete(Long)
	 */
	public void delete(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(NEWS_DELETE);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException exception){
			throw new DAOException("Error deleting news entity.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, dataSource);
		}
	}

	/**
	 * @see INewsDao#searchNews(SearchCriteria, Long, Long)
	 */
	public List<News> searchNews(SearchCriteria searchCriteria, Long startPosition, Long endPosition) throws DAOException {
		if ((searchCriteria.getAuthorId() == null) && (searchCriteria.getTagsId() == null)){
			return read(startPosition, endPosition);
		}
		List<News> news = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;		
		try{
			News itemNews;
			connection = DataSourceUtils.getConnection(dataSource);	
			String query = buildQueryForSearch(searchCriteria);
			preparedStatement = connection.prepareStatement(query);
			int i = 1;
			if (searchCriteria.getTagsId() != null){
				for (Long tagId: searchCriteria.getTagsId()){
					preparedStatement.setLong(i, tagId);
					i++;
				}
			}
			if (searchCriteria.getAuthorId() != null){
				preparedStatement.setLong(i, searchCriteria.getAuthorId());
				i++;
			}
			preparedStatement.setLong(i, startPosition);
			preparedStatement.setLong(i + 1, endPosition);
			resultSet = preparedStatement.executeQuery();
			news = new ArrayList<News>();
			while (resultSet.next()){
				itemNews = parseResultSetForSearch(resultSet);
				news.add(itemNews);
			}
		} catch (SQLException exception) {
			throw new DAOException(String.format("Error searching news entities between %d and %d positions.", startPosition, endPosition), exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);		
		}
		return news;
	}

	/**
	 * @see INewsDao#countAllNews(SearchCriteria)
	 */
	public Long countAllNews(SearchCriteria searchCriteria) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Long countNews = null;
		try{
			
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(buildQueryForCount(searchCriteria));
			int i = 1;
			if (searchCriteria.getTagsId() != null){
				for (Long tagId: searchCriteria.getTagsId()){
					preparedStatement.setLong(i, tagId);
					i++;
				}
			}
			if (searchCriteria.getAuthorId() != null){
				preparedStatement.setLong(i, searchCriteria.getAuthorId());
			}
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				countNews = resultSet.getLong(COUNT);
			}
		} catch (SQLException exception){
			throw new DAOException("Error counting news entity by searchCriteria.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return countNews;
	}
	
	/**
	 * @see INewsDao#readAll()
	 */
	public List<News> readAll() throws DAOException {

		List<News> listNews = null;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try{
			News news;
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(NEWS_READ_ALL);
			listNews = new ArrayList<News>();
			while (resultSet.next()){
				news = parseResultSetForSearch(resultSet);
				listNews.add(news);
			}		
		} catch (SQLException exception){
			throw new DAOException("Error reading all news entities.", exception);
		} finally {
			CloseUtility.close(connection, statement, resultSet, dataSource);
		}
		return listNews;
	}
	
	/**
	 * @see INewsDao#getNextNewsId(SearchCriteria, Long)
	 */
	public Long getNextNewsId(SearchCriteria searchCriteria, Long newsId) throws DAOException {
		Long nextNewsId = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(buildQueryForNextNewsId(searchCriteria));
			int i = 1;
			if (searchCriteria.getTagsId() != null){
				for (Long tagId: searchCriteria.getTagsId()){
					preparedStatement.setLong(i, tagId);
					i++;
				}
			}
			if (searchCriteria.getAuthorId() != null){
				preparedStatement.setLong(i, searchCriteria.getAuthorId());
				i++;
			}
			preparedStatement.setLong(i, newsId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				nextNewsId = resultSet.getLong(NEXT_NEWS_ID);
			}
		} catch (SQLException exception){
			throw new DAOException("Error reading next newsId.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return nextNewsId;
	}

	/**
	 * @see INewsDao#getPrevNewsId(SearchCriteria, Long)
	 */
	public Long getPrevNewsId(SearchCriteria searchCriteria, Long newsId) throws DAOException {
		Long prevNewsId = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(buildQueryForPrevNewsId(searchCriteria));
			int i = 1;
			if (searchCriteria.getTagsId() != null){
				for (Long tagId: searchCriteria.getTagsId()){
					preparedStatement.setLong(i, tagId);
					i++;
				}
			}
			if (searchCriteria.getAuthorId() != null){
				preparedStatement.setLong(i, searchCriteria.getAuthorId());
				i++;
			}
			preparedStatement.setLong(i, newsId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				prevNewsId = resultSet.getLong(NEXT_NEWS_ID);
			}
		} catch (SQLException exception){
			throw new DAOException("Error reading next newsId.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return prevNewsId;
	}
	
	/**
	 * @see INewsDao#getCurrentPageForNews(SearchCriteria, Long)
	 */
	public Long getCurrentPageForNews(SearchCriteria searchCriteria, Long newsId) throws DAOException {
		Long currentPage = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(buildQueryForCurrentPage(searchCriteria));
			int i = 1;
			if (searchCriteria.getTagsId() != null){
				for (Long tagId: searchCriteria.getTagsId()){
					preparedStatement.setLong(i, tagId);
					i++;
				}
			}
			if (searchCriteria.getAuthorId() != null){
				preparedStatement.setLong(i, searchCriteria.getAuthorId());
				i++;
			}
			preparedStatement.setLong(i, newsId);
			
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()){
				currentPage = resultSet.getLong(CURRENT_PAGE);
			}
		} catch (SQLException exception){
			throw new DAOException("Error reading current page for news.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return currentPage;
	}	
	
	private News parseResultSetForSearch(ResultSet resultSet) throws SQLException{
		News itemNews = new News();
		itemNews.setNewsId(resultSet.getLong(NEWS_ID));
		itemNews.setTitle(resultSet.getString(TITLE));
		itemNews.setShortText(resultSet.getString(SHORT_TEXT));
		itemNews.setFullText(resultSet.getString(FULL_TEXT));
		itemNews.setCreationDate(resultSet.getTimestamp(CREATION_DATE));
		itemNews.setModificationDate(resultSet.getDate(MODIFICATION_DATE));
		return itemNews;
	}
	private News parseResultSet(ResultSet resultSet) throws SQLException{
		News news = new News();
		news.setNewsId(resultSet.getLong(NS_NEWS_ID));
		news.setTitle(resultSet.getString(NS_TITLE));
		news.setShortText(resultSet.getString(NS_SHORT_TEXT));
		news.setFullText(resultSet.getString(NS_FULL_TEXT));
		news.setCreationDate(resultSet.getTimestamp(NS_CREATION_DATE));
		news.setModificationDate(resultSet.getDate(NS_MODIFICATION_DATE));
		return news;
	}	
	
	private String buildQueryForCount(SearchCriteria searchCriteria){
		
		StringBuilder stringQuery = new StringBuilder(COUNT_START);
		stringQuery.append(SEARCH_START);
		
		stringQuery.append(NEWS_READ_ALL);
		stringQuery.append(") ");
		
		
		if (searchCriteria.getAuthorId() != null){
			stringQuery.append(SEARCH_JOIN_AUTHORS);
		}
		if (searchCriteria.getTagsId() != null){
			stringQuery.append(SEARCH_JOIN_TAGS);
		}
		if ((searchCriteria.getAuthorId() != null) || (searchCriteria.getTagsId() != null)){
			stringQuery.append(SEARCH_WHERE);
		}
		if ((searchCriteria.getTagsId() != null) && (searchCriteria.getTagsId().size() != 0)) {
			stringQuery.append(SEARCH_OPTION_TAGS_START);
			for (int i = 1; i <= searchCriteria.getTagsId().size(); i++){
				stringQuery.append(" ? , ");
			}
			stringQuery.delete(stringQuery.length() - 3, stringQuery.length());
			stringQuery.append(SEARCH_OPTION_TAGS_END);
		}
		if (searchCriteria.getAuthorId() != null){
			stringQuery.append(SEARCH_OPTION_AUTHORS);
		}
		if ((searchCriteria.getAuthorId() != null) || (searchCriteria.getTagsId() != null)){
			stringQuery.delete(stringQuery.length() - 4, stringQuery.length());
		}
		
		stringQuery.append(COUNT_END);
		return stringQuery.toString();
	}
	
	private String buildQueryForNextNewsId(SearchCriteria searchCriteria){
		StringBuilder stringQuery = new StringBuilder(GET_NEXT_NEWS_ID_START);
		
		stringQuery.append(NEWS_READ_ALL);
		
		if (searchCriteria.getAuthorId() != null){
			stringQuery.append(SEARCH_JOIN_AUTHORS);
		}
		if (searchCriteria.getTagsId() != null){
			stringQuery.append(SEARCH_JOIN_TAGS);
		}
		if ((searchCriteria.getAuthorId() != null) || (searchCriteria.getTagsId() != null)){
			stringQuery.append(SEARCH_WHERE);
		}
		if ((searchCriteria.getTagsId() != null) && (searchCriteria.getTagsId().size() != 0)) {
			stringQuery.append(SEARCH_OPTION_TAGS_START);
			for (int i = 1; i <= searchCriteria.getTagsId().size(); i++){
				stringQuery.append(" ? , ");
			}
			stringQuery.delete(stringQuery.length() - 3, stringQuery.length());
			stringQuery.append(SEARCH_OPTION_TAGS_END);
		}
		if (searchCriteria.getAuthorId() != null){
			stringQuery.append(SEARCH_OPTION_AUTHORS);
		}
		if ((searchCriteria.getAuthorId() != null) || (searchCriteria.getTagsId() != null)){
			stringQuery.delete(stringQuery.length() - 4, stringQuery.length());
		}
		stringQuery.append(GET_NEWS_ID_END);
		
		return stringQuery.toString();
	}
	
	private String buildQueryForPrevNewsId(SearchCriteria searchCriteria){
		StringBuilder stringQuery = new StringBuilder(GET_PREV_NEWS_ID_START);
		
		stringQuery.append(NEWS_READ_ALL);
		
		if (searchCriteria.getAuthorId() != null){
			stringQuery.append(SEARCH_JOIN_AUTHORS);
		}
		if (searchCriteria.getTagsId() != null){
			stringQuery.append(SEARCH_JOIN_TAGS);
		}
		if ((searchCriteria.getAuthorId() != null) || (searchCriteria.getTagsId() != null)){
			stringQuery.append(SEARCH_WHERE);
		}
		if ((searchCriteria.getTagsId() != null) && (searchCriteria.getTagsId().size() != 0)) {
			stringQuery.append(SEARCH_OPTION_TAGS_START);
			for (int i = 1; i <= searchCriteria.getTagsId().size(); i++){
				stringQuery.append(" ? , ");
			}
			stringQuery.delete(stringQuery.length() - 3, stringQuery.length());
			stringQuery.append(SEARCH_OPTION_TAGS_END);
		}
		if (searchCriteria.getAuthorId() != null){
			stringQuery.append(SEARCH_OPTION_AUTHORS);
		}
		if ((searchCriteria.getAuthorId() != null) || (searchCriteria.getTagsId() != null)){
			stringQuery.delete(stringQuery.length() - 4, stringQuery.length());
		}
		stringQuery.append(GET_NEWS_ID_END);
		
		return stringQuery.toString();
	}
	
	private String buildQueryForCurrentPage(SearchCriteria searchCriteria){
		StringBuilder stringBuilder = new StringBuilder();
		
		stringBuilder.append(NEW_SEARCH_PART_1);
		stringBuilder.append(NEW_SEARCH_PART_2);
		stringBuilder.append(")");
		
		if (searchCriteria.getAuthorId() != null){
			stringBuilder.append(SEARCH_JOIN_AUTHORS);
		}
		if (searchCriteria.getTagsId() != null){
			stringBuilder.append(SEARCH_JOIN_TAGS);
		}
		if ((searchCriteria.getAuthorId() != null) || (searchCriteria.getTagsId() != null)){
			stringBuilder.append(SEARCH_WHERE);
		}
		if ((searchCriteria.getTagsId() != null) && (searchCriteria.getTagsId().size() != 0)) {
			stringBuilder.append(SEARCH_OPTION_TAGS_START);
			for (int i = 1; i <= searchCriteria.getTagsId().size(); i++){
				stringBuilder.append(" ? , ");
			}
			stringBuilder.delete(stringBuilder.length() - 3, stringBuilder.length());
			stringBuilder.append(SEARCH_OPTION_TAGS_END);
		}
		if (searchCriteria.getAuthorId() != null){
			stringBuilder.append(SEARCH_OPTION_AUTHORS);
		}
		if ((searchCriteria.getAuthorId() != null) || (searchCriteria.getTagsId() != null)){
			stringBuilder.delete(stringBuilder.length() - 4, stringBuilder.length());
		}
		
		stringBuilder.append(")");
		stringBuilder.append(NEW_SEARCH_PART_3);
		stringBuilder.append(GET_CURRENT_PAGE);
		stringBuilder.append(NEW_SEARCH_PART_5);
		
		return stringBuilder.toString();
	}
	
	private String buildQueryForSearch(SearchCriteria searchCriteria){
		StringBuilder stringBuilder = new StringBuilder();
		
		stringBuilder.append(NEW_SEARCH_PART_1);
		stringBuilder.append(NEW_SEARCH_PART_2);
		stringBuilder.append(")");
		
		if (searchCriteria.getAuthorId() != null){
			stringBuilder.append(SEARCH_JOIN_AUTHORS);
		}
		if (searchCriteria.getTagsId() != null){
			stringBuilder.append(SEARCH_JOIN_TAGS);
		}
		if ((searchCriteria.getAuthorId() != null) || (searchCriteria.getTagsId() != null)){
			stringBuilder.append(SEARCH_WHERE);
		}
		if ((searchCriteria.getTagsId() != null) && (searchCriteria.getTagsId().size() != 0)) {
			stringBuilder.append(SEARCH_OPTION_TAGS_START);
			for (int i = 1; i <= searchCriteria.getTagsId().size(); i++){
				stringBuilder.append(" ? , ");
			}
			stringBuilder.delete(stringBuilder.length() - 3, stringBuilder.length());
			stringBuilder.append(SEARCH_OPTION_TAGS_END);
		}
		if (searchCriteria.getAuthorId() != null){
			stringBuilder.append(SEARCH_OPTION_AUTHORS);
		}
		if ((searchCriteria.getAuthorId() != null) || (searchCriteria.getTagsId() != null)){
			stringBuilder.delete(stringBuilder.length() - 4, stringBuilder.length());
		}
		
		stringBuilder.append(")");
		stringBuilder.append(NEW_SEARCH_PART_3);
		stringBuilder.append(NEW_SEARCH_PART_4);
		stringBuilder.append(NEW_SEARCH_PART_5);
		
		return stringBuilder.toString();
	}
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}	
}
