package com.epam.zakharankou.newsmanagement.dao.implJDBC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.dao.GenericDao;
import com.epam.zakharankou.newsmanagement.dao.ITagDao;
import com.epam.zakharankou.newsmanagement.dao.implJDBC.util.CloseUtility;
import com.epam.zakharankou.newsmanagement.entity.Tag;

/**
 * TagDao class implements {@link com.epam.zakharankou.newsmanagement.dao.ITagDao}.
 * @author Anton_Zakharankou
 *
 */
public class TagDao implements ITagDao {

	private DataSource dataSource;
	
	private static final String TAG_CREATE = "INSERT INTO TAGS (TAG_ID, TAG_NAME) VALUES (TG_TAG_ID_SEQ.nextval, ?)";
	private static final String TAG_READ = "SELECT TAG_ID, TAG_NAME FROM (SELECT TAGS.TAG_ID, TAGS.TAG_NAME, ROW_NUMBER() over (ORDER BY TAGS.TAG_ID) R FROM TAGS ) WHERE R BETWEEN ? and ?";
	private static final String TAG_READ_ALL = "SELECT TAG_ID, TAG_NAME FROM TAGS";
	private static final String TAG_READ_BY_ID = "SELECT TAG_ID, TAG_NAME FROM TAGS WHERE (TAG_ID = ?)";
	private static final String TAG_READ_BY_NEWS_ID = "SELECT TAGS.TAG_ID, TAGS.TAG_NAME FROM TAGS INNER JOIN NEWS_TAGS ON (NEWS_TAGS.TAG_ID = TAGS.TAG_ID) AND (NEWS_TAGS.NEWS_ID = ?)";
	private static final String TAG_UPDATE = "UPDATE TAGS SET TAG_NAME = ? WHERE (TAG_ID = ? )";
	private static final String TAG_DELETE = "DELETE FROM TAGS WHERE (TAG_ID = ? )";
	private static final String TAG_CREATE_LINK = "INSERT INTO NEWS_TAGS (NEWS_ID, TAG_ID) VALUES ( ? , ? )";
	private static final String TAG_DELETE_LINK = "DELETE FROM NEWS_TAGS WHERE ( NEWS_ID = ? ) AND ( TAG_ID = ? )";
	private static final String TAG_DELETE_LINKS_BY_NEWS_ID = "DELETE FROM NEWS_TAGS WHERE (NEWS_ID = ? )";
	private static final String TAG_DELETE_LINKS_BY_TAG_ID = "DELETE FROM NEWS_TAGS WHERE (TAG_ID = ? )";
	
	private static final String TG_TAG_ID = "TAG_ID";
	private static final String TG_TAG_NAME = "TAG_NAME";
	
	/**
	 * @see GenericDao#create(Object)
	 */
	public Long create(Tag tag) throws DAOException{
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Long tagId = null;
		String[] column = {"TAG_ID"};
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(TAG_CREATE, column);
			preparedStatement.setString(1, tag.getTagName());
			preparedStatement.executeUpdate();
			resultSet = preparedStatement.getGeneratedKeys();
			if ((resultSet != null) && (resultSet.next())){
				tagId = resultSet.getLong(1);
			}
		} catch (SQLException exception) {
			throw new DAOException("Error creating tag entity.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return tagId;
	}

	/**
	 * @see GenericDao#delete(Long)
	 */
	public void delete(Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(TAG_DELETE);
			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException exception){
			throw new DAOException("Error deleting tag entity.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, dataSource);
		}
	}
	
	/**
	 * @see ITagDao#readAll()
	 */
	public List<Tag> readAll() throws DAOException{
		List<Tag> listTags = null;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try{
			Tag tag;
			connection = DataSourceUtils.getConnection(dataSource);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(TAG_READ_ALL);
			listTags = new ArrayList<Tag>();
			while (resultSet.next()){
				tag = parseResultSet(resultSet);
				listTags.add(tag);
			}		
		} catch (SQLException exception){
			throw new DAOException("Error reading all tags entities.", exception);
		} finally {
			CloseUtility.close(connection, statement, resultSet, dataSource);
		}
		return listTags;
	}

	/**
	 * @see ITagDao#read(Long, Long)
	 */
	public List<Tag> read(Long startPosition, Long endPosition) throws DAOException {
		List<Tag> listTags = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			Tag tag;
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(TAG_READ);
			preparedStatement.setLong(1, startPosition);
			preparedStatement.setLong(2, endPosition);
			resultSet = preparedStatement.executeQuery();
			listTags = new ArrayList<Tag>();
			while (resultSet.next()){
				tag = parseResultSet(resultSet);
				listTags.add(tag);
			}		
		} catch (SQLException exception){
			throw new DAOException(String.format("Error reading tags entities between %d and %d positions.", startPosition, endPosition), exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return listTags;
	}
	
	/**
	 * @see GenericDao#readById(Long)
	 */
	public Tag readById(Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Tag tag = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(TAG_READ_BY_ID);
			preparedStatement.setLong(1, tagId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()){
				tag = parseResultSet(resultSet);
			}
		} catch (SQLException exception){
			throw new DAOException("Error reading tag entity by TAG_ID.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return tag;
	}
	
	/**
	 * @see GenericDao#update(Object)
	 */
	public boolean update(Tag tag) throws DAOException{
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(TAG_UPDATE);
			preparedStatement.setString(1, tag.getTagName());
			preparedStatement.setLong(2, tag.getTagId());
			preparedStatement.executeUpdate();
		} catch (SQLException exception){
			throw new DAOException("Error updating tag entity.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, dataSource);
		}
		return true;
	}

	/**
	 * @see ITagDao#createLink(Long, Long)
	 */
	public void createLink(Long newsId, Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(TAG_CREATE_LINK);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException exception){
			throw new DAOException("Error creating tag-news link.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, dataSource);
		}
	}
	
	/**
	 * @see ITagDao#deleteLink(Long, Long)
	 */
	public void deleteLink(Long newsId, Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(TAG_DELETE_LINK);
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException exception){
			throw new DAOException("Error deleting tag-news link.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, dataSource);
		}
	}
	
	/**
	 * @see ITagDao#createLinks(List, Long)
	 */
	public void createLinks(List<Long> tagIds, Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(TAG_CREATE_LINK);
			for (Long tagId: tagIds){
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tagId);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException exception){
			throw new DAOException("Error creating few tag-news links.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, dataSource);
		}
	}

	/**
	 * @see ITagDao#deleteLinksByNewsId(Long)
	 */
	public void deleteLinksByNewsId(Long newsId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(TAG_DELETE_LINKS_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
		} catch (SQLException exception){
			throw new DAOException("Error deleting few tag-news links by NEWS_ID.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, dataSource);
		}
	}

	/**
	 * @see ITagDao#readTagsByNewsId(Long)
	 */
	public List<Tag> readTagsByNewsId(Long newsId) throws DAOException {
		List<Tag> tags = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(TAG_READ_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			resultSet = preparedStatement.executeQuery();
			Tag tag = null;
			tags = new ArrayList<Tag>();
			while (resultSet.next()){
				tag = parseResultSet(resultSet);
				tags.add(tag);
			}
		} catch (SQLException exception){
			throw new DAOException("Error reading tags entities by NEWS_ID.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, resultSet, dataSource);
		}
		return tags;
	}

	/**
	 * @see ITagDao#deleteLinksByTagId(Long)
	 */
	public void deleteLinksByTagId(Long tagId) throws DAOException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try{
			connection = DataSourceUtils.getConnection(dataSource);
			preparedStatement = connection.prepareStatement(TAG_DELETE_LINKS_BY_TAG_ID);
			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException exception){
			throw new DAOException("Error deleting few tag-news links by TAG_ID.", exception);
		} finally {
			CloseUtility.close(connection, preparedStatement, dataSource);
		}
	}
	
	private Tag parseResultSet(ResultSet resultSet) throws SQLException{
		Tag tag = new Tag();
		tag.setTagId(resultSet.getLong(TG_TAG_ID));
		tag.setTagName(resultSet.getString(TG_TAG_NAME));
		return tag;
	}
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
