package com.epam.zakharankou.newsmanagement.dao.implJDBC;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import java.util.List;

import com.epam.zakharankou.newsmanagement.dao.implJDBC.TagDao;
import com.epam.zakharankou.newsmanagement.entity.Tag;

@SpringApplicationContext({"configTest.xml"})
@DataSet(value = "DataSetTest.xml", loadStrategy = CleanInsertLoadStrategy.class)
public class TagDaoTest extends UnitilsJUnit4{
	
	@SpringBean("tagDaoImpl")
	private TagDao tagDao;
	
	@Test
	public void createTest() throws Exception{
		Tag expectedTag = new Tag();
		expectedTag.setTagName("Tag5");
		
		Long actualId = tagDao.create(expectedTag);
		Tag actualTag = tagDao.readById(actualId);
		
		assertEquals(expectedTag.getTagName(), actualTag.getTagName());
	}
	
	@Test
	public void deleteTest() throws Exception{
		Long tagId = new Long(1L);
		
		tagDao.deleteLinksByTagId(tagId);
		tagDao.delete(tagId);
		Tag actualTag = tagDao.readById(tagId);
		
		assertNull(actualTag);
	}
	
	@Test
	public void readAllTest() throws Exception{
		Tag firstTag = new Tag();
		Tag secondTag = new Tag();
		Tag thirdTag = new Tag();
		Tag fourthTag = new Tag();
		firstTag.setTagId(new Long(1L));
		firstTag.setTagName("Tag1");
		secondTag.setTagId(new Long(2L));
		secondTag.setTagName("Tag2");
		thirdTag.setTagId(new Long(3L));
		thirdTag.setTagName("Tag3");
		fourthTag.setTagId(new Long(4L));
		fourthTag.setTagName("Tag4");
		List<Tag> expectedTagList = new ArrayList<Tag>();
		expectedTagList.add(firstTag);
		expectedTagList.add(secondTag);
		expectedTagList.add(thirdTag);
		expectedTagList.add(fourthTag);
		
		List<Tag> actualTagList = tagDao.readAll();
		
		assertEquals(expectedTagList, actualTagList);		
	}
	
	@Test
	public void readTest() throws Exception{
		Long startPosition = new Long(3L);
		Long endPosition = new Long(4L);
		Tag thirdTag = new Tag();
		Tag fourthTag = new Tag();
		thirdTag.setTagId(new Long(3L));
		thirdTag.setTagName("Tag3");
		fourthTag.setTagId(new Long(4L));
		fourthTag.setTagName("Tag4");
		List<Tag> expectedTagList = new ArrayList<Tag>();
		expectedTagList.add(thirdTag);
		expectedTagList.add(fourthTag);
		
		List<Tag> actualTagList = tagDao.read(startPosition, endPosition);
		
		assertEquals(expectedTagList, actualTagList);
	}
	
	@Test
	public void readById() throws Exception{
		Long tagId = new Long(3L);
		Tag expectedTag = new Tag();
		expectedTag.setTagId(new Long(3L));
		expectedTag.setTagName("Tag3");
		
		Tag actualTag = tagDao.readById(tagId);
		
		assertEquals(expectedTag, actualTag);
	}
	
	@Test
	public void updateTest() throws Exception{
		Tag expectedTag = new Tag();
		expectedTag.setTagId(new Long(2L));
		expectedTag.setTagName("Tag22");
		
		tagDao.update(expectedTag);
		Tag actualTag = tagDao.readById(expectedTag.getTagId());
		
		assertEquals(expectedTag, actualTag);
	}
	
	@Test
	public void createLinkTest() throws Exception{
		Long newsId = new Long(3L);
		Long tagId = new Long(2L);
		Tag expectedTag = new Tag();
		expectedTag.setTagId(tagId);
		expectedTag.setTagName("Tag2");
		
		tagDao.createLink(newsId, tagId);
		List<Tag> actualTagList = tagDao.readTagsByNewsId(newsId);
		
		assertNotNull(actualTagList);
		assertTrue(actualTagList.contains(expectedTag));
	}
	
	@Test
	public void deleteLinkTest() throws Exception{
		Long newsId = new Long(2L);
		Long tagId = new Long(3L);
		Tag expectedTag = new Tag();
		expectedTag.setTagId(tagId);
		expectedTag.setTagName("Tag3");
		
		tagDao.deleteLink(newsId, tagId);
		List<Tag> actualTagList = tagDao.readTagsByNewsId(newsId);
		
		assertNotNull(actualTagList);
		assertFalse(actualTagList.contains(expectedTag));
	}
	
	@Test
	public void createLinksTest() throws Exception{
		Long newsId = new Long(4L);	
		List<Long> expectedTagIdList = new ArrayList<Long>();
		expectedTagIdList.add(1L);
		expectedTagIdList.add(2L);
		
		tagDao.createLinks(expectedTagIdList, newsId);
		List<Tag> tagList = tagDao.readTagsByNewsId(newsId);
		
		List<Long> actualTagList = new ArrayList<Long>();
		for (Tag tag: tagList){
			actualTagList.add(tag.getTagId());
		}
		expectedTagIdList.add(3L);
		assertTrue(actualTagList.containsAll(expectedTagIdList));				
	}
	
	@Test
	public void deleteLinksByNewsIdTest() throws Exception{
		Long newsId = new Long(1L);
		List<Tag> expectedTagList = new ArrayList<Tag>();
		tagDao.deleteLinksByNewsId(newsId);
		List<Tag> actualTagList = tagDao.readTagsByNewsId(newsId);		
		
		assertEquals(expectedTagList, actualTagList);
	}
	
	@Test
	public void readTagsByNewsIdTest() throws Exception{
		Long newsId = new Long(2L);
		Tag firstTag = new Tag();
		Tag secondTag = new Tag();
		firstTag.setTagId(new Long(3L));
		firstTag.setTagName("Tag3");
		secondTag.setTagId(new Long(4L));
		secondTag.setTagName("Tag4");
		List<Tag> expectedTagList = new ArrayList<Tag>();
		expectedTagList.add(firstTag);
		expectedTagList.add(secondTag);
		
		List<Tag> actualTagList = tagDao.readTagsByNewsId(newsId);
		
		assertEquals(expectedTagList, actualTagList);
	}
	
	@Test
	public void deleteLinksByTagIdTest() throws Exception{
		Long tagId = new Long(3L);
		
		tagDao.deleteLinksByTagId(tagId);
		tagDao.delete(tagId);		
	}
}
