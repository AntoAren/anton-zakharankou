<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="divNewsList">
	<c:choose>
		<c:when test="${complexNews.news != null }">
			<div class="divBack">
				<a href="/news-admin/news/back/${complexNews.news.newsId}"><spring:message code="local.back"/></a>				
			</div>
			<table class="tableSingleNews">
				<tr class="trSingleNews">
					<td class="textSingleNewsTitle">
						<b><c:out value="${complexNews.news.shortText}"/></b> (<spring:message code="local.byAuthor"/> <c:out value="${complexNews.author.authorName}"/>)
					</td>
					<td class="textDate">
						<c:out value="${complexNews.news.modificationDate}"/>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="textSingleNewsFullText">
						<c:out value="${complexNews.news.fullText}"/>
					</td>
				</tr>
			</table>
			<table class="tableComments">
				<c:forEach var="comment" items="${complexNews.comments}">
					<tr class="trComment">
						<td class="tdComment">
							<table>
								<tr class="trCommentEdit">
									<td	colspan="2" class="textDateComment">
										<c:out value="${comment.getDate()}"/>
									</td>
								</tr>
								<tr>
									<td class="textComment">
										<c:out value="${comment.commentText}"/>
									</td>
									<td align="right">
										<sf:form action="/news-admin/news/deleteComment" method="post" commandName="comment">									
											<sf:hidden path="newsId" value="${complexNews.news.newsId}"/>
											<sf:hidden path="commentId" value="${comment.commentId}"/>
											<input type="submit" value="&#10006" class="buttonDeleteComment"/>
										</sf:form> 
									</td>
								</tr>						
							</table>
						</td>
					</tr>
				</c:forEach>		
				<sf:form action="/news-admin/news/postComment" modelAttribute="comment">
					<tr class="trPostComment">
						<td>					
							<sf:textarea path="commentText" rows="3" cols="44"/>
							<sf:errors path="commentText" cssClass="error"></sf:errors>
						</td>			
					</tr>
					<tr class="trPostCommentButton">
						<td align="right">
							<sf:hidden path="newsId" value="${complexNews.news.newsId}"/>
							<input type="submit" value="<spring:message code="local.postComment"/>"/>
						</td>
					</tr>						
				</sf:form>		
			</table>
			<table class="tablePrevNext">
				<tr>
					<td align="left">
						<c:if test="${prevNewsId != 0}">
							<a href="/news-admin/news/showNews/${prevNewsId}"><spring:message code="local.prev"/></a>					
						</c:if>
					</td>
					<td align="right">
						<c:if test="${nextNewsId != 0}">
							<a href="/news-admin/news/showNews/${nextNewsId}"><spring:message code="local.next"/></a>					
						</c:if>
					</td>
				</tr>
			</table>
		</c:when>
		<c:otherwise>
			<table>
				<tr valign="middle">
					<td align="center">
						<h3><spring:message code="local.noSuchNewsId"/></h3>
					</td>
				</tr>
			</table>
		</c:otherwise>
	</c:choose>
</div>