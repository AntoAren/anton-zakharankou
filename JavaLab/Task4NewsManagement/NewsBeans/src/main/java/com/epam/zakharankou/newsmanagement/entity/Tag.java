package com.epam.zakharankou.newsmanagement.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Tag class provides entity tag.
 * @author Anton_Zakharankou
 *
 */

@Entity
@Table(name = "TAGS")
@SequenceGenerator(name = "TG_TAG_ID_SEQ", sequenceName = "TG_TAG_ID_SEQ", allocationSize = 1)
public class Tag{
		
	@Id	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TG_TAG_ID_SEQ")
	@Column(name = "TG_TAG_ID", nullable = false, precision = 20)
	@PrimaryKeyJoinColumn
	private Long tagId;
	
	@Column(name = "TG_TAG_NAME", nullable = false, precision = 30)
	@NotNull(message = "{error.tag.tagName.notNull}")
	@Size(min = 1, max = 30, message = "{error.tag.tagName.size}")
	private String tagName;
		
	@ManyToMany
	@JoinTable(name = "NEWS_TAGS", inverseJoinColumns = {@JoinColumn(name = "NT_NEWS_ID", 
			referencedColumnName = "NW_NEWS_ID")}, joinColumns = {@JoinColumn(name = "NT_TAG_ID",
			referencedColumnName = "TG_TAG_ID")})
	private List<News> newsList;
	
	public Tag(){}
	
	public Long getTagId() {
		return tagId;
	}
	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tagId == null) ? 0 : tagId.intValue());
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (tagId.longValue() != other.getTagId().longValue())
			return false;
		if (tagName == null) {
			if (other.getTagName() != null)
				return false;
		} else if (!tagName.equals(other.getTagName()))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Tag [tagId=" + tagId + ", tagName=" + tagName + "]";
	}
	
}
