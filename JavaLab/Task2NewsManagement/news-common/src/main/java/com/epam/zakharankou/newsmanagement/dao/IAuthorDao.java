package com.epam.zakharankou.newsmanagement.dao;

import java.util.List;

import com.epam.zakharankou.newsmanagement.dao.GenericDao;
import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.Author;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

/**
 * IAuthorDao is interface which works with {@link com.epam.zakharankou.newsmanagement.entity.Author} entity.
 * @author Anton_Zakharankou
 *
 */
public interface IAuthorDao extends GenericDao<Author> {	
	
	/**
	 * Reads a certain number of records {@link com.epam.zakharankou.newsmanagement.entity.Author} entities from database.
	 * @return list of {@link com.epam.zakharankou.newsmanagement.entity.Author} entities.
	 * @throws DAOException if SQLException was thrown.
	 */
	public List<Author> read(Long startPosition, Long endPosition) throws DAOException;
	
	/**
	 * Reads all {@link com.epam.zakharankou.newsmanagement.entity.Author} entities from database.
	 * @return list of {@link com.epam.zakharankou.newsmanagement.entity.Author} entities.
	 * @throws DAOException if SQLException was thrown.
	 */
	public List<Author> readAll() throws DAOException;
	
	/**
	 * Writes record in NEWS_AUTHORS table in database. Creates link between {@link com.epam.zakharankou.newsmanagement.entity.Author} entity and 
	 * {@link com.epam.zakharankou.newsmanagement.entity.News} entity.
	 * @throws DAOException if SQLException was thrown.
	 */
	public void createLink(Long authorId, Long newsId) throws DAOException;
	
	/**
	 * Delete record from NEWS_AUTHORS table from database. Delete link between {@link com.epam.zakharankou.newsmanagement.entity.Author} entity and 
	 * {@link com.epam.zakharankou.newsmanagement.entity.News} entity.
	 * @throws DAOException if SQLException was thrown.
	 */
	public void deleteLink(Long newsId) throws DAOException;
	
	/**
	 * Reads {@link com.epam.zakharankou.newsmanagement.entity.Author} entity by received parameter.
	 * @return {@link com.epam.zakharankou.newsmanagement.entity.Author} entity
	 * @throws DAOException if SQLException was thrown.
	 */
	public Author readAuthorByNewsId(Long newsId) throws DAOException;
	
	/**
	 * Reads all not expired authors.
	 * @return list of authors.
	 * @throws ServiceException if SQLException was thrown.
	 */
	public List<Author> readAllNotExpired() throws DAOException;
}