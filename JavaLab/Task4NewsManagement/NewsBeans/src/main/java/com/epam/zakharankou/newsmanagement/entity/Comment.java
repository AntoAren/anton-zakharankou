package com.epam.zakharankou.newsmanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * Comment class provides comment entity.
 * @author Anton_Zakharankou
 *
 */

@Entity
@Table(name = "COMMENTS")
@SequenceGenerator(name = "CM_COMMENT_ID_SEQ", sequenceName = "CM_COMMENT_ID_SEQ", allocationSize = 1)
public class Comment{
		
	@Id	
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_COMMENT_ID_SEQ")
	@Column(name = "CM_COMMENT_ID", nullable = false, precision = 20)
	private Long commentId;	
	
	@PrimaryKeyJoinColumn
	@Column(name = "CM_NEWS_ID", nullable = false, precision = 20)
	@NotNull
	@Min(1)
	private Long newsId;
	
	@Column(name = "CM_COMMENT_TEXT", nullable = false, precision = 100)
	@NotNull(message = "{error.comment.commentText.notNull}")
	@Size(min = 1, max = 100, message = "{error.comment.commentText.size}")
	private String commentText;
	
	@Column(name = "CM_CREATION_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;	
	
	@ManyToOne
	@JoinColumn(name="CM_NEWS_ID", nullable=false, insertable=false, updatable=false)
	private News news;
	
	public Comment(){}
	
	public Long getCommentId() {
		return commentId;
	}
	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}
	public Long getNewsId() {
		return newsId;
	}	
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}
	public String getCommentText() {
		return commentText;
	}
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	public Date getCreationDate() {		
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
		
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((commentId == null) ? 0: commentId.intValue());
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0: newsId.intValue());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentId != other.getCommentId())
			return false;
		if (commentText == null) {
			if (other.getCommentText() != null)
				return false;
		} else if (!commentText.equals(other.getCommentText()))
			return false;
		if (creationDate == null) {
			if (other.getCreationDate() != null)
				return false;
		} else if (!creationDate.equals(other.getCreationDate()))
			return false;
		if (newsId != other.getNewsId())
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Comment [commentId=" + commentId + ", newsId=" + newsId
				+ ", commentText=" + commentText + ", creationDate="
				+ creationDate + "]";
	}	
}
