package com.epam.zakharankou.newsmanagement.service.impl;

import org.apache.log4j.Logger;

import com.epam.zakharankou.newsmanagement.dao.IUserDao;
import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.User;
import com.epam.zakharankou.newsmanagement.service.UserService;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

/**
 * UserServiceImpl class implements {@link com.epam.zakharankou.newsmanagement.service.UserService} interface.
 * @author Anton_Zakharankou
 *
 */
public class UserServiceImpl implements UserService {

	private static final Logger logger = Logger.getLogger("RollingFileAppender");
	
	private IUserDao userDao;	

	/**
	 * @see UserService#hasUser(User)
	 */
	public User hasUser(User user) throws ServiceException {
		User readUser = null;
		try{
			readUser = userDao.hasUser(user);
		} catch (DAOException exception){
			logger.error("Error reading user entity.", exception);
			throw new ServiceException("Error reading user entity.", exception);
		}
		return readUser;
	}

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}
}
