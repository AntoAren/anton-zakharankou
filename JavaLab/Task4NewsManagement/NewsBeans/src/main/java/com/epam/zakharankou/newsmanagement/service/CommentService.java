package com.epam.zakharankou.newsmanagement.service;

import com.epam.zakharankou.newsmanagement.entity.Comment;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

/**
 * CommentService class is service which work with {@link com.epam.zakharankou.newsmanagement.entity.Comment} action.
 * @author Anton_Zakharankou
 *
 */
public interface CommentService {	
		
	/**
	 * @see MainNewsManagementService#addComment(Comment)
	 */
	public Long addComment(Comment comment) throws ServiceException;
	
	/**
	 * @see MainNewsManagementService#deleteComment(Long)
	 */
	public void deleteComment(Long commentId) throws ServiceException;
}
