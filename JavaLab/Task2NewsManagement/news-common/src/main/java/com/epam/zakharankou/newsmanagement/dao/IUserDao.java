package com.epam.zakharankou.newsmanagement.dao;

import com.epam.zakharankou.newsmanagement.dao.exception.DAOException;
import com.epam.zakharankou.newsmanagement.entity.User;
import com.epam.zakharankou.newsmanagement.service.exception.ServiceException;

/**
 * IUserDao is interface which works with {@link com.epam.zakharankou.newsmanagement.entity.User} entity.
 * @author Anton_Zakharankou
 *
 */
public interface IUserDao {

	/**
	 * Checks there are user or not.
	 * @param user - checking user.
	 * @return user if has, null if doesn't have.
	 * @throws ServiceException if {@link DAOException} was thrown.
	 */
	public User hasUser(User user) throws DAOException;
}
